public class 值传递与引用传递
{
	public static void main(String[] args) {
		int x=10;
		fun(x);
		System.out.println("x="+x);
		System.out.println("----------------------");



		duck d=new duck();
		fun1(d);
		System.out.println(d.age);
		System.out.println("----------------------");
		fun11(d);
		System.out.println(d.age);


		String name="小马";
		fun2(name);
		System.out.println(name);
		System.out.println("----------------------");

		person per=new person();
		fun3(per);
		System.out.println(per.name);



	}

	public static void fun(int mx)//值传递
	{
		mx=20;
	}

	public static void fun1(duck d)//引用传递
	{
		d.age=10;
	}

	public static void fun11(duck d)
	{
		d=new duck();
		d.age=20;
	}
	public static void fun2(String name)//String引用数据类型
	{
		name="小小";
	}

	public static void fun3(person p)
	{
		//p.name="爸爸";
		//p=new person();
		p.name="dada";
	}
}

class duck
{
	int age=1;
}

class person
{
	String name="嘻嘻";
}
