public class StringBuilderDemo
{
	public static void main(String[] args) {
		/*
		StringBuilder适合在单线程中使用，是线程不安全的，性能高
		是StringBuffer的兄弟
		 */
		StringBuilder sb=new StringBuilder();
		sb.append("aaa").append("bbb");
		System.out.println(sb);
	}
}