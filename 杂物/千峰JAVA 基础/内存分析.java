public class 内存分析
{
	public static void main(String[] args) {
		/*
		当对象单元没有指向其的对象引用时，会被JVM垃圾回收
		 */
		Horse horse1=new Horse();
		horse1.name="小白";
		horse1.age=20;
		horse1.run();
		Horse horse2=new Horse();
		horse2.name="小花";
		horse2.age=10;
		horse2.run();
		horse2=horse1;
		System.out.println("---------------------------------");
		horse1.run();
		horse2.run();
		horse2.name="haha";
		horse1.run();
	}
}

class Horse
{
	String name;
	int age;
	public Horse()
	{

	}
	public void run()
	{
		System.out.println(this.name+"在跑,它"+this.age+"岁了");
		
	}
}