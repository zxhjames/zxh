//许多具有相同的特征和行为的对象可以成为抽象类
/*abstract class Animal 
{
	public int height;
	public int width;
	public String name;
	//抽象方法只有声明，没有实现
	public Animal(String s,int h,int w)
	{
		this.name=s;
		this.height=h;
		this.width=w;
	}
	public abstract void move();
}
*/
abstract class Person //extends Animal 
{
	public String name;
	public int height;
	public int width;
	public Person(String s,int h,int w)
	{
		this.name=s;
		this.height=h;
		this.width=w;
	}
	public abstract void eat();
}

class Man extends Person 
{
	public int age;
	public Man()
	{
		super("科比",180,60);
	}
	public void move()
	{
		System.out.println("我爱跑步");
	}
	
	public default void eat()
	{
		System.out.println("我爱吃肉");
	}
}

public class 抽象类 
{
	//抽象类无法实例化
	public static void main(String[] args) {
		Man man=new Man();
		man.age=100;
		man.move();
		man.eat();
		System.out.println(man.name+" "+man.height+" "+man.width);
	}
}