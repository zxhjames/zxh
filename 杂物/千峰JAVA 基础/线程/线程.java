class MyThread extends Thread//一个类只要继承了Thread，就是多线程操作类
{
	private String name;
	public MyThread(String name)//属性内容
	{
		this.name=name;
	}

	public void run()
	{
		for(int i=0;i<10;i++)
		{
			System.out.println(name+"运行"+i);
		}
	}

}

class MyThreadRun implements Runnable//注意，这是接口
{
	private String name;
	public MyThreadRun(String name)
	{
		this.name=name;
	}

	public void run()
	{
		for(int i=0;i<10;i++)
		{
			System.out.println(name+i+"*");
		}
	}
}


//继承thread不能共享
class MyThreaddemo extends Thread{
	private int t=5;
	public void run()
	{
		for(int i=0;i<100;i++)
		{
			if(t>0)
			{
				System.out.println("t="+t--);
			}
		}
	}
}


class MyThreaddemo_1 implements Runnable{
	private int t=5;
	public void run()
	{
		for(int i=0;i<100;i++)
		{
			if(t>0)
			{
				System.out.println("t="+t--);
			}
		}
	}
}
	public class 线程
	{
		public static void main(String[] args) {
			/*MyThread mt1=new MyThread("线程A");
			MyThread mt2=new MyThread("线程B");
			//此时并没有启动
			mt1.run();
			mt2.run();
			//此时才是启动线程，谁抢占了cpu资源，就会射出，所以每次结果会不相同
			mt1.start();
			mt2.start();
			//重复调用，会出现异常
			mt1.start();
*/
			//使用runnable
/*			MyThreadRun mt3=new MyThreadRun("线程C");
			MyThreadRun mt4=new MyThreadRun("线程D");
			
			Thread mtt3=new Thread(mt3);
			Thread mtt4=new Thread(mt4);

			mtt3.start();
			mtt4.start();
*/

			//使用THread不能进行资源共享
			/*MyThreaddemo mt5=new MyThreaddemo();
			MyThreaddemo mt6=new MyThreaddemo();
			MyThreaddemo mt7=new MyThreaddemo();

			mt5.start();
			mt6.start();
			mt7.start();*/


			//下面是runnable测试
			MyThreaddemo_1 my=new MyThreaddemo_1();
			new Thread(my).start();
			System.out.println(new Thread(my).getName());
			new Thread(my).start();
			System.out.println(new Thread(my).getName());
			new Thread(my).start();
System.out.println(new Thread(my).getName());
		}
	}