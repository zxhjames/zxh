class MyThread implements Runnable
{
	public void run()
	{
		for(int i=0;i<10;i++)
		{
			System.out.println(Thread.currentThread().getName()+"运行-->"+i);
		}
	}
}


public class 判断线程是否启动
{
	public static void main(String[] args) {
		MyThread mt=new MyThread();//实例化对象
		Thread t=new Thread(mt,"线程");//实例化thread对象
		System.out.println("线程开始执行之前-->"+t.isAlive());//判断线程是否启动
		t.start();
		System.out.println("线程开始执行之后-->"+t.isAlive());

		for(int i=0;i<3;i++)
		{
			System.out.println("main运行"+i);
		}
		System.out.println("代码执行之后"+t.isAlive());
		//主线程有可能最先执行完，那么其他线程将不受任何影响
	}
}