class MyThread implements Runnable
{
	public void run()
	{
		for(int i=0;i<50;i++)
		{
			System.out.println(Thread.currentThread().getName()+"运行-->"+i);
		}
	}
}


public class 线程的强制运行
{
	public static void main(String[] args) {
		MyThread mt=new MyThread();
		Thread m=new Thread(mt,"线程");
		m.start();
		for(int i=0;i<50;i++)
		{
			if(i>10){
				try{
				m.join();}//让m线程进入，阻塞main线程
				catch(Exception e){}
			}
			System.out.println("Main线程运行-->"+i);
		}
	}
}