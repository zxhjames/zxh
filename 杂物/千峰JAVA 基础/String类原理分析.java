public class String类原理分析
{
	//String表示一个字符，值不可变，不能被继承
	public static void main(String[] args) {
		//第一种赋值方式，直接赋值
		String s1="mdzz";
		//第二种赋值方式，使用new关键字开辟一个新的空间
		//首先在常量池里找到"mdzz",之后再开辟一块空间
		String s2=new String("hello");
		//不可能相等
		System.out.println(s1.equals(s2));
		String s3="mdzz";
		System.out.println(s1==s3);

		String a="a";
		String a1=a+1;
		String a2="a1";
		System.out.println(a1==a2);
	}
}