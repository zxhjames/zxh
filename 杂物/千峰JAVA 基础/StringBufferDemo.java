public class StringBufferDemo
{
	public static void main(String[] args) {
		String a="a";
		String b="b";
		String c=a+b+1;
		System.out.println(c);
		//这种方法可能会产生多个对象
		
		String d="a"+1+2+4+"b";
		System.out.println(d);
		//这种只会有一个变量，因为后面的都是常量
		
		/*
		StringBuffer目的是来解决字符串相加带来的性能问题
		StringBuffer内部实现采用字符数组，默认的长度为16，超过数组大小时
		长度变为原来的两倍加2,而且它是线程安全的
		适合多线程编程
		 */
		StringBuffer sb=new StringBuffer(2);
		sb.append("a").append("b").append("d");
		sb.append("hahahahaha",0,4);//添加指定索引
		sb.delete(0,3);//删除指定索引
		System.out.println(sb.toString());
		System.out.println(sb.capacity());
	}
}