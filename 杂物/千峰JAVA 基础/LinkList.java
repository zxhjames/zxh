public class LinkList
{
	public static void main(String[] args) {
		NodeManager nm=new NodeManager();
		nm.add(5);
		nm.add(4);
		nm.add(3);
		nm.add(2);
		nm.add(1);
		nm.print();
	}
}


class NodeManager
{
	private Node root;//定义一个根节点

	//添加结点
	public void add(int data)
	{
		if(root==null)
		{
			root=new Node(data);
		}
		else
		{
			root.addData(data);
		}
	}

	//删除节点
	public void del(int data)
	{
		if(root.getData()==data)
		{
			root=root.next;
		}
		else
		{
			root.delData(data);
		}
	}

	//打印所有节点
	public void print()
	{
		if(root!=null)
		{
			System.out.print(root.getData()+"->");
			root.printNode();
			System.out.println();
		}
	}

	//判断是否找到结点数据
	public boolean find(int data)
	{
		if(root==null)
			return false;
		if(root.getData()==data)
			return true;
		else
			return root.findNode(data);
	}

	//更新结点
	public void update(int oldData,int newData)
	{

	}

	//插入节点
	public void insert(int index,int data)
	{

	}


	private class Node//不想类外面的类来访问
	{
		private int data;
		private Node next;//用当前的类型来定义自己的属性
		public Node(int data)
		{
			this.data=data;
		}
		public void setData(int data)
		{
			this.data=data;
		}
		public int getData()
		{
			return this.data;
		}
		public void addData(int data)
		{
			if(this.next==null)
			{
				this.next=new Node(data);
			}
			else
			{
				this.next.addData(data);
			}
			
		}
		public void delData(int data)
		{
			if(this.next!=null)
			{
				if(this.next.data==data)
				{
					this.next=this.next.next;
				}
				else
				{
					this.next.delData(data);
				}
			}
		}	
		public void printNode()
		{
			if(this.next!=null)
			{
				System.out.print(this.next.data+"->");
				this.next.printNode();
			}
		}

		//查找是否存在结点
		public boolean findNode(int data)
		{
			if(this.next!=null)
			{
				if(this.next.data==data)
				{
					return true;
				}
				else
				{
					this.next.findNode(data);
				}
			}
			return false;
			
		}
		public void updateNode(int oldData,int newData)
		{

		}
		public void insert(int index,int data)
		{

		}
	}

}