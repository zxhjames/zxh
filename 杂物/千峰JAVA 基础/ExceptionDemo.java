public class ExceptionDemo
{
	public static void main(String[] args) {
		//div(10,1);
		//method();
		div2(1,0);
	}

	private static void div(int num1,int num2)
	{
		int[] arr={1,2,3,4,5};
		try
		{
			System.out.println(arr[4]);
			//arr=null;
			int result=num1/num2;
			System.out.println(result);
			System.out.println(arr.length);
		}
		catch(ArithmeticException e)//数学算术异常
		{
			System.out.println("除数不能为0！");
		}
		catch(ArrayIndexOutOfBoundsException e)//数组越界异常
		{
			System.out.println("数组越界了！");
		}
		catch(NullPointerException e)//空指针异常
		{
			System.out.println("空指针异常!");
		}
		catch(Exception e)
		{
			System.out.println("出错了!");
			//这个异常是父类，注意要把小的异常放在后面，把小异常放前面
		}
		finally//不管怎样，最终都会有finally执行
		{
			System.out.println("程序执行完毕!");
		}
	}

	private static int method()
	{
			int a=10;
		int b=5;
		try{
			System.out.println("a="+a);
			System.out.println("b="+b);
			int c=a/b;
			System.out.println("a/b="+c);
			return c;
		}catch(Exception e){
			e.printStackTrace();//打印内存栈的信息
			//用于代码测试使用，没有错误就删除
		}finally{
			System.out.println("finally");
			//注意，这一步是先执行return语句，再执行finally		}
		return -1;
		}
	}

	private static int div2(int a,int b)//throws ArithmeticException
	{
		try
		{
			int c=a/b;
			return c;
		}catch(ArithmeticException e){
			throw new ArithmeticException("除数不能为0");
			//抛异常不用写return语句
		}
	}
		
}