public class SelectSort
{
	static final int SIZE=10;

	public static void  selectSort(int[] a,int m,int n)
	{
		int index,temp;
		
		for(int i=0;i<a.length-1;i++)
	 {
			index=i;
			for(int j=i+1;j<a.length;j++)
			{
				if(a[j]<a[index])
				{
					index=j;
					m=m+1;
				}
			}
		
		//交换两个数
		if(index!=i)
		{
			temp=a[i];
			a[i]=a[index];
			a[index]=temp;
			++n;
		}

		System.out.println("第"+(i+1)+"步排序结果是:");
		for(int h=0;h<a.length;h++)
		{
			System.out.print(" "+a[h]);
		}
		System.out.println("\n");

	}
	System.out.println("一共进行了"+m+"次比较,"+n+"次移动");
   }



public static void main(String[] args) {
	int[] shuzu=new int[SIZE];

	for(int i=0;i<SIZE;i++)
	{
		shuzu[i]=(int)(100+Math.random()*(100+1));
	}

	System.out.print("排序前数组为: \n");
	for(int i=0;i<SIZE;i++)
	{
		System.out.print(shuzu[i]+" ");
	}
	System.out.print("\n");
	selectSort(shuzu,0,0);
	System.out.print("排序后的数组为:\n");
	for(int i=0;i<SIZE;i++)
	{
		System.out.print(shuzu[i]+" ");
	}
	System.out.print("\n");
}


}