public class InsertSort
{
	static final int size=10;
	static void insertionsort(int[] a)
	{
		int i,j,t,h;
		for(i=1;i<a.length;i++)
		{
			t=a[i];
			j=i-1;
			while(j>=0&&t<a[j])
			{
				a[j+1]=a[j];
				j--;
			}
			a[j+1]=t;
			System.out.print("第"+i+"步结果:");
			for(h=0;h<a.length;h++)
			{
				System.out.print(" "+a[h]);
			}
			System.out.print("\n");
		}
	}

	public static void main(String[] args)
	{
		int[] shuzu=new int[size];
		int i;
		for(i=0;i<size;i++)
		{
			shuzu[i]=(int)(100+Math.random()*(100+1));
		}
		System.out.print("排序前: \n");
		for(i=0;i<size;i++)
		{
			System.out.print(shuzu[i]+" ");
		}
		System.out.print("\n");
		insertionsort(shuzu);
		System.out.print("排序后: \n");
		for(i=0;i<size;i++)
		{
			System.out.print(shuzu[i]+" ");
		}
		System.out.print("\n");
		
	}
}
