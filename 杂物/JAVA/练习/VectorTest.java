import java.util.Vector;


class VectorTest
{
	public static void main(String[] args) {
		Vector vec=new Vector(3);
		System.out.println("The old capacity is:"+vec.capacity());
		vec.addElement(new Integer(1));
		vec.addElement(new Integer(2));
		vec.addElement(new Integer(3));
		vec.addElement(new Integer(4));
		vec.addElement(new Integer(5));
		vec.addElement(new Integer(6));
		System.out.println("the new capacity is:"+vec.capacity());

		for(int i=0;i<vec.size();i++)
		{
			int x=((Integer)(vec.get(i))).intValue();
			System.out.println(x);
		}

		System.out.println("组件数为:"+vec.size());
		System.out.println("第一个组件为:"+vec.firstElement());
		System.out.println("第后一个组件为:"+vec.lastElement());
		if(vec.contains(new Integer(2)))
		{
			System.out.println("找到值为2的组件");
		}
		vec.removeElement(1);
		if(vec.contains(new Integer(1)))
		{
			System.out.println("找到值为1的组件");
		}
		else
		{
			System.out.println("删除了值等于1的组件");
		}
	}
}