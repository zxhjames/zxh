#include<iostream>
#include<stdio.h>
using namespace std;
#define MAX_TREE_SIZE 100
typedef int TELEMTYPE;//树节点的类型，目前暂定为整型

//树节点结构
typedef struct PTNode
{
	TELEMTYPE data;//结点数据
	int parent;//双亲位置
}PTNode;

//树结构
typedef struct 
{
	PTNode nodes[MAX_TREE_SIZE];//结点数组
	int r,n;//根的位置和节点数
}PTree;