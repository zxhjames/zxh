import java.util.*;


class CBTType//定义二叉树的类型
{
	String data;//元素数据
	CBTType left;//左子数结点的引用
	CBTType right;//右子树结点的引用
}

public class TwoDivideTree
{
	
	final int MAXLEN=20;//最大长度
	static Scanner input = new Scanner(System.in);
	//初始化二叉树的根
	CBTType InitTree()
	{
		CBTType node;

		if((node=new CBTType())!=null)
		{
			System.out.println("请先输入一个根结点数据:\n");
			node.data=input.next();
			node.left=null;
			node.right=null;
			if(node!=null)
			{
				return node;
			}
			else
			{
				return null;
			}
		}
		return null;
	}


	//添加结点
	void AddTreeNode(CBTType treeNode)
	{
		CBTType pnode,parent;
		String data;
		int menusel;

		if((pnode=new CBTType())!=null)
		{
			System.out.println("输入二叉树节点数据:\n");
			pnode.data=input.next();
			pnode.left=null;	//设置左右子树为空
			pnode.right=null;

			System.out.println("输入该节点的父节点数据:");
			data=input.next();

			parent=TreeFindNode(treeNode,data);	//查找指定数据的顶点
			if(parent==null)
			{
				System.out.println("未找到该父亲顶点\n");
				pnode=null;
				return;
			}

			System.out.printf("1.添加该结点到左子树\n2.添加该结点到右子树\n");

			do
			{
				menusel=input.nextInt();//输入选择项

				if(menusel==1 || menusel==2)
				{
					if(parent==null)
					{
						System.out.println("不存在父节点，请先设置父节点!\n");
					}
					else
					{
						switch(menusel)
						{
							case 1:
								if(parent.left!=null)
								{
									System.out.println("左子树结点不为空!\n");
								}
								else
								{
									parent.left=pnode;
								}
								break;

							case 2:
								if(parent.right!=null)
								{
									System.out.println("右子树结点不为空!\n");
								}
								else
								{
									parent.right=pnode;
								}
								break;
							default:
							System.out.println("无效参数!\n");

						}
					}
				}
			}while(menusel!=1 && menusel!=2);
		}
	}


	//寻找结点
	CBTType TreeFindNode(CBTType treeNode,String data)
	{
		CBTType ptr;

		if(treeNode==null)
		{
			return null;
		}
		else
		{
			if(treeNode.data.equals(data))
			{
				return treeNode;
			}
			else
			{
				if(TreeFindNode(treeNode.left,data)!=null)
				{
					ptr=TreeFindNode(treeNode.left,data);
					return ptr;
				}
				else if(TreeFindNode(treeNode.right,data)!=null)
				{
					ptr=TreeFindNode(treeNode.right,data);
					return ptr;
				}
				else
				{
					return null;
				}
			}
		}
	}


	//获取左子树
	CBTType TreeLeftNode(CBTType treeNode)
	{
		if(treeNode!=null)
		{
			return treeNode.left;
		}
		else
		{
			return null;
		}
	}

	//获取右子树
	CBTType TreeRightNode(CBTType treeNode)
	{
		if(treeNode!=null)
		{
			return treeNode.right;
		}
		else
		{
			return null;
		}
	}


	//判断空树
	int TreeIsEmpty(CBTType treeNode)
	{
		if(treeNode!=null)
		{
			return 0;
		}

		else
		{
			return 1;
		}
	}


	//计算二叉树的深度
	int TreeDepth(CBTType treeNode)
	{
		int depleft,depright;
		if(treeNode==null)
		{
			return 0;
		}
		else
		{
			depleft = TreeDepth(treeNode.left);//左子树深度
			depright = TreeDepth(treeNode.right);
			if(depleft<depright)
			{
				return depleft+1;
			}

			else
			{
				return depright+1;
			}
		}
	}




	//清空二叉树
	void ClearTree(CBTType treeNode)
	{
		if(treeNode!=null)
		{
			ClearTree(treeNode.left);
			ClearTree(treeNode.right);
			treeNode=null;
		}
	}



	//显示结点数据
	void TreeNodeData(CBTType p)
	{
		System.out.printf("%s",p.data);
	}



	//遍历二叉树
	//1.按层遍历
	void LevelTree(CBTType treeNode)
	{
		CBTType p;
		CBTType[] q=new CBTType[MAXLEN];//定义一个顺序栈
		int head=0;
		int tail=0;

		if(treeNode!=null)	//如果队首引用不为空
		{
			tail=(tail+1)%MAXLEN;//计算循环队列队尾序号
			q[tail]=treeNode;
		}

		while(head!=tail)
		{
			head=(head+1)%MAXLEN;
			p=q[head];
			TreeNodeData(p);
			if(p.left!=null)
			{
				tail=(tail+1)%MAXLEN;
				q[tail]=p.left;
			}

			if(p.right!=null)
			{
				tail=(tail+1)%MAXLEN;
				q[tail]=p.right;
			}
		}
	}


	//先序遍历
	void DLRTree(CBTType treeNode)
	{
		if(treeNode!=null)
		{
			TreeNodeData(treeNode);//显示结点的数据
			DLRTree(treeNode.left);
			DLRTree(treeNode.right);
		}
	}

	//中序遍历
	void LDRTree(CBTType treeNode)
	{
		if(treeNode!=null)
		{
			LDRTree(treeNode.left);
			TreeNodeData(treeNode);
			LDRTree(treeNode.right);
		}
	}

	//后序遍历
	void LRDTree(CBTType treeNode)
	{
		if(treeNode!=null)
		{
			LRDTree(treeNode.left);
			LRDTree(treeNode.right);
			TreeNodeData(treeNode);
		}
	}


	public static void main(String[] args) {
		CBTType root=null;//root为指向二叉树根节点的引用
		int menusel;
		TwoDivideTree t=new TwoDivideTree();
		//设置根元素
		 
		root=t.InitTree();

		do
		{
			System.out.println("请选择菜单添加二叉树的结点\n");
			System.out.println("0 退出\t");
			System.out.println("1 添加二叉树的结点\n");
			menusel=input.nextInt();
			switch(menusel)
			{
				case 1:
				t.AddTreeNode(root);
				break;
				case 0:
				break;
				default:
				;	
			}
		}while(menusel!=0);


		do{
			System.out.printf("请选择菜单添加二叉树，输入0表示退出:\n");
			System.out.println("1.先序遍历DLR\t");
			System.out.println("2.中序遍历LDR\n");
			System.out.println("3.后序遍历LRD\t");
			System.out.println("4.按层遍历\n");
			menusel=input.nextInt();
			switch(menusel)
			{
				case 0:
				break;
				case 1:
				System.out.println("\n先序遍历DLR的结果:");
				t.DLRTree(root);
				System.out.println();
				break;
				case 2:
				System.out.println("\n中序遍历LDR的结果:");
				t.LDRTree(root);
				System.out.println();
				break;
				case 3:
				System.out.println("\n后序遍历LRD的结果:");
				t.LRDTree(root);
				System.out.println();
				break;
				case 4:
				System.out.println("\n按层遍历的结果");
				t.LevelTree(root);
				System.out.println();
				break;
				default:
				;
			}
		}while(menusel!=0);

		System.out.printf("二叉树的深度为:%d\n",t.TreeDepth(root));
		t.ClearTree(root);
		root=null;
	}
}



