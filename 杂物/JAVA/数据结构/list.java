import java.util.Scanner;

class data//数据元素的类
{
	String key;
	String name;
	int age;
}
class cltype//链表的类
{
	data nodedata=new data();//存放节点数据
	cltype nextnode;//指向下一个节点的指针

	//追加尾结点
	cltype add(cltype head,data nodedata)//头指针，数据域
	{
		cltype node,htemp;
		if((node=new cltype())==null)//开辟一个前驱节点的指针，如果无法开辟
		{
			System.out.println("申请内存失败!\n");
			return null;
		}

		else//如果可以开辟
		{
			node.nodedata=nodedata;//保存数据，传参
			node.nextnode=null;//设置后继结点为空
			if(head==null)//头引用，如果为空
			{
				head=node;
				return head;
			}
			htemp=head;//如果头结点不为空
			while(htemp.nextnode!=null)//寻找链表的末尾
			{
				htemp=htemp.nextnode;
			}
			htemp.nextnode=node;
			return head;
		}

	}

 
	//追加头结点
	cltype addf(cltype head,data nodedata)
	{
		cltype node;//下一个节点的指针
		if((node=new cltype())==null)
		{
			System.out.println("申请内存失败!");
			return null;
		}
		else
		{
			node.nodedata=nodedata;
			node.nextnode=head;
			head=node;
			return head;
		}
	}

	//寻找结点
	cltype findnode(cltype head,String key)
	{
		cltype htemp;
		htemp=head;
		while(htemp!=null)
		{
			if(htemp.nodedata.key.compareTo(key)==0)
			{
				return htemp;
			}
			htemp=htemp.nextnode;
		}
		return null;
	}


	//插入结点
	cltype insertnode(cltype head,String findkey,data nodedata)
	{
		cltype node,nodetemp;
		if((node=new cltype())==null)
		{
			System.out.println("申请内存失败!");
			return null;
		}

		node.nodedata=nodedata;
		nodetemp=findnode(head,findkey);

		if(nodetemp!=null)//node前一个节点的指针
		{
			node.nextnode=nodetemp.nextnode;
			nodetemp.nextnode=node;
		}
		else
		{
			System.out.println("未能找到正确的插入位置!\n");
			//free(node);
		}
		return head;
	}

	//删除结点
	int deletenode(cltype head,String key)
	{
		cltype node,htemp;
		htemp=head;
		node=head;
		while(htemp!=null)
		{
			if(htemp!=null)
			{
				node.nextnode=htemp.nextnode;
				free(htemp);
				return 1;
			}
			else
			{
				node=htemp;
				htemp=htemp.nextnode;
			}
		}
		return 0;
	}

	//计算链表长度
	int cllength(cltype head)
	{
		cltype htemp;
		int len=0;
		htemp=head;
		while(htemp!=null)
		{
			len++;
			htemp=htemp.nextnode;
		}
		return len;
	}

	//遍历链表
	void allnode(cltype head)
	{
		cltype htemp;
		data nodedata;
		htemp=head;
		System.out.printf("当前链表共有%d个结点，链表所有数据如下:\n",cllength(head));
		while(htemp!=null)
		{
			nodedata=htemp.nodedata;
			System.out.printf("结点(%s,%s,%d)\n",nodedata.key,nodedata.name,nodedata.age);
			htemp=htemp.nextnode;
		}	
	}
}

public class list 
{
	public static void main(String[] args) {
		
	}
}