 public class Student{

    private int age;
    private String name;

    public void Student() {
        this.age = 21;
        this.name = "someone";
    }

    public void setAge (int age) {
        this.age = age;
    }

    public int getAge() {
        return this.age;
    }

     public static void main(String[] args) {

        Student[] students = new Student [3]; 

        students[0].setAge(18);  

        System.out.println(students[0].getAge());
    }
}

