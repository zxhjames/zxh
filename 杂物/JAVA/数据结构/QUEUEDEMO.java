import java.util.Scanner;

//数据域
class DATA4 
{
	String name;
	int age;
}
//队列
class SQTYPE 
{
	static final int QUEUELEN=15;
	DATA4[] data=new DATA4[QUEUELEN];
	int head;
	int tail;


	//初始化队列
	SQTYPE INIT()
	{
		SQTYPE q;
		if((q=new SQTYPE())!=null)
		{
			q.head=0;//设置对头
			q.tail=0;//设置队尾
			return q;
		}
		else
		{
			return null;
		}
	}

	//判断队列是否为空
	int SQTISEMPTY(SQTYPE q)
	{
		int temp=0;
		if(q.head==q.tail)
		{
			temp=1;
			System.out.println("队列为空!");
			return (temp);
		}
		return 0;
	}
	//判断队列是否已满
	int SQTFULL(SQTYPE q)
	{
		int temp=0;
		if(q.tail==QUEUELEN)
		{
			temp=1;
			return temp;
		}
		return 0;
	}

	//清空队列
	void SQTCLEAR(SQTYPE q)
	{
		q.head=0;
		q.tail=0;
	}

	//释放队列
	void SQTYPEFREE(SQTYPE q)
	{
		if(q!=null)
		{
			q=null;
		}
	}

	//开始入队
	int INSQTYPE(SQTYPE q,DATA4 data)
	{
		if(q.tail==QUEUELEN)
		{
			System.out.println("队列已经满了！操作失败");
			return 0;
		}

		else
		{
			q.data[q.tail++]=data;
			return 1;
		}
	}

	//开始出队!
	//
	DATA4 OUTSQTYPE(SQTYPE q)
	{
		if(q.head==q.tail)
		{
			System.out.println("队列已经空了，操作失败");
			System.exit(0);
		}

		else
		{
			return q.data[q.head++];
		}

		return null;
	}

	//读结点数据
	DATA4 PEEKSQTYPE(SQTYPE q)
	{
		if(SQTISEMPTY(q)==1)
		{
			System.out.println("空队列!");
			return null;
		}

		else
		{
			return q.data[q.head];
		}
	}

	//计算队列长度
	int SQTYPELEN(SQTYPE q)
	{
		int temp;
		temp=q.tail-q.head;
		return temp;
	}
}


public class QUEUEDEMO 
{
	public static void main(String[] args) {
		SQTYPE st=new SQTYPE();
		DATA4 data1;

		Scanner input=new Scanner(System.in);
		SQTYPE stack=st.INIT();
		System.out.println("入队列操作");
		System.out.println("输入姓名，年龄进行入队列操作!");

		do 
		{
			DATA4 data=new DATA4();
			data.name=input.next();
			data.age=input.nextInt();
			if(data.name.equals("0"))
			{
				break;
			}
			else
			{
				st.INSQTYPE(stack,data);
			}
		}while(true);



		String temp="1";
		System.out.printf("出队操作，按任意非0键进行出栈操作");
		temp=input.next();
		while(!temp.equals("0"))
		{
			data1=st.OUTSQTYPE(stack);
			System.out.printf("出队列的顺序是(%s,%d)\n",data1.name,data1.age);
			temp=input.next();
		}
		System.out.println("测试结束!");
		st.SQTYPEFREE(stack);
	}
}


