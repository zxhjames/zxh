import java.util.Scanner;
import java.lang.String;

class DATA
{
	int key;
	String name;
}


class STACKDEMO 
{
	static final int maxlen=50;
	DATA[] data=new DATA[maxlen+1];
	int top;


	void init(STACKDEMO p)
	{
		//STACKDEMO p;
		
			p.top=0;
			System.out.println("栈初始化已完成!");
		
	}

	boolean isEmpty(STACKDEMO p)
	{
		if(p.top==0)
		{
			System.out.println("栈为空!");
			return true;
		}

		else
		{
			//System.out.println("栈不为空!");
			return false;
		}

	}

	boolean isFull(STACKDEMO p)
	{
		if(p.top==maxlen)
		{
			System.out.println("栈已经满!");
			return true;
		}

		else
		{
			System.out.println("栈未满!");
			return false;
		}
	}

	//清空栈
	void CLEAR(STACKDEMO p)
	{
		p.top=0;
		System.out.println("栈已清空!");
	}

	//释放空间
	void FREE(STACKDEMO p)
	{
		if(p!=null)
		{
			p=null;
		}
	}

	//开始入栈
	int PUSHSTACK(STACKDEMO p,DATA data)
	{
		if((p.top+1)>maxlen)
		{
			System.out.println("栈溢出！");
			return 0;
		}

		else
		{
			p.data[++p.top]=data;
			return 1;
		}
	}

	//开始出栈!
	DATA POPSTACK(STACKDEMO p)
	{
		if(p.top==0)
		{
			System.out.println("栈为空!");
		}

		return (p.data[p.top--]);
	}

	//读栈顶操作！
	DATA PEEKSTACK(STACKDEMO p)
	{
		if(p.top==0)
		{
			System.out.println("栈为空!");
		}
		
		return (p.data[p.top]);
		
	}

	//删除栈中元素
	void delete(STACKDEMO p)
	{
		p.data[p.top]=null;
		p.top--;
	}


}


public class stackdemo_
{
	public static void main(String[] args) {
		/*STACKDEMO st=new STACKDEMO();
		STACKDEMO stack=st.init();*/
		STACKDEMO p=new STACKDEMO();
		DATA data1=new DATA();
		p.init(p);
		p.isEmpty(p);
		p.isFull(p);
		Scanner input=new Scanner(System.in);
		System.out.println("入栈操作！");
		System.out.println("输入姓名&学号!开始入栈!");
		 //String data.name=null;
		do
		{
			DATA data=new DATA();
			data.name=input.next();

			if(data.name.equals("0"))
			{
				break;
			}
			else
			{
				data.key=input.nextInt();
				p.PUSHSTACK(p,data);
				//p.delete(p);
			}
		}while(true);
		
		String temp="1";
		System.out.println("出栈操作，按任意非0键进行出栈!");
		temp=input.next();
		while(!temp.equals("0"))
		{
			if(p.isEmpty(p))
			{
				System.out.println("已经全部出栈!");
				break;
			}
			else
			data1=p.POPSTACK(p);
			System.out.printf("出栈的数据是(%s,%d)\n",data1.name,data1.key);
			temp=input.next();
		}	
		System.out.println("测试结束！");
		p.FREE(p);
	} 
}