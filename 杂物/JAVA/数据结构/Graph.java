import java.util.Scanner;
class GraphMatrix
	{
		
		static final int MaxNum=20;
		//图的最大顶点数
		static final int MaxValue=65535;
		//最大值
		char[] Vertex=new char[MaxNum];
		//保存顶点信息
		int GType;	
		//图的类型
		int VertexNum;
		//顶点的数量
		int EdgeNum;
		//边的数量
		int[][] EdgeWeight=new int[MaxNum][MaxNum];		
		//保存边的权
		int[] isTrav=new int[MaxNum];//遍历标志
	}


	public class Graph 
	{
		static Scanner input=new Scanner(System.in);
		static void CreateGraph(GraphMatrix GM)
		{
			int i,j,k;
			int weight;//权
			char EstartV,EendV;//边的起始顶点
			System.out.printf("输入图中各顶点的信息\n");
			for(i=0;i<GM.VertexNum;i++)
			{
				System.out.printf("第%d个顶点:", i+1);
				//保存到各顶数组元素中
				GM.Vertex[i]=(input.next().toCharArray())[0];
			}
			System.out.println("输入构成各边的顶点和权值");
			for(k=0;k<GM.EdgeNum;k++)
			{
				System.out.printf("第%d条边",k+1);
				EstartV=input.next().charAt(0);
				EendV=input.next().charAt(0);
				weight=input.nextInt();
				for(i=0;EstartV!=GM.Vertex[i];i++)//在已有顶点中寻找开始点
				{
					for(j=0;EendV!=GM.Vertex[j];j++)//在已有顶点中寻找终点
					{
						GM.EdgeWeight[i][j]=weight;//对应位置保存权值，表示有一条边
						if(GM.GType==0)//若是无向图
						{
							GM.EdgeWeight[j][i]=weight;//在对角位置保存权值
						}
					}
				}
			}
		}


		//清空图
		static void ClearGraph(GraphMatrix GM)
		{
			int i,j;

			for(i=0;i<GM.VertexNum;i++)//清空矩阵
			{
				for(j=0;j<GM.VertexNum;j++)
				{
					//设置矩阵中各元素为MaxValue
					GM.EdgeWeight[i][j]=GraphMatrix.MaxValue;//不可达
				}
			}
		}


		//显示图
		static void OutGraph(GraphMatrix GM)
		{
			//输出领结矩阵
			int i,j;
			for(j=0;j<GM.VertexNum;j++)
			{
				System.out.printf("\t%c",GM.Vertex[j]);//输出顶点信息
			}

			System.out.println();

			for(i=0;i<GM.VertexNum;i++)
			{
				System.out.printf("%c",GM.Vertex[i]);

				for(j=0;j<GM.VertexNum;j++)
				{
					if(GM.EdgeWeight[i][j]==GraphMatrix.MaxValue)//若权值为最大值
					{
						System.out.printf("\tZ");
					}

					else
					{
						System.out.printf("\t%d",GM.EdgeWeight[i][j]);
					}
				}
				System.out.println();
			}
		}


		//遍历图
		static void DeepTraOne(GraphMatrix GM,int n)
		{
			int i;
			GM.isTrav[n]=1;
			System.out.printf("->%c",GM.Vertex[n]);//输出结点数据
			//添加处理节点的操作
			
			for(i=0;i<GM.VertexNum;i++)
			{
				if(GM.EdgeWeight[n][i]!=GraphMatrix.MaxValue && GM.isTrav[n]==0)
				{
					DeepTraOne(GM,i);//递归进行遍历
				}
			}
		}



		//深度优先遍历结点
	static void DeepTraGraph(GraphMatrix GM)
	{
		int i;

		for(i=0;i<GM.VertexNum;i++)//清除各顶点遍历标志
		{
			GM.isTrav[i]=0;
		}
		System.out.println("深度优先遍历结点:");

		for(i=0;i<GM.VertexNum;i++)
		{
			if(GM.isTrav[i]==0)//若改点未遍历
			{
				DeepTraOne(GM,i);//调用函数遍历
			}
		}
		System.out.println();
	}


		public static void main(String[] args) {
			GraphMatrix GM=new GraphMatrix();//定义保存领接表的图
			System.out.println("输入生成图的类型:");
			GM.GType=input.nextInt();
			System.out.println("输入图的顶点数量:");
			GM.VertexNum=input.nextInt();
			ClearGraph(GM);
			CreateGraph(GM);
			System.out.println("该图的领接矩阵数据如下:\n");
			OutGraph(GM);
			DeepTraGraph(GM);
		}

	}


	

	
	


	


	






