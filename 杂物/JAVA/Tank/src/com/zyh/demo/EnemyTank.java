package com.zyh.demo;

import java.util.Vector;

public class EnemyTank extends Tank implements Runnable{
	Vector<Shot> shots = new Vector<>();
	Vector<EnemyTank> ets = new Vector<>();
	
	public Vector<EnemyTank> getEts() {
		return ets;
	}

	public void setEts(Vector<EnemyTank> ets) {
		this.ets = ets;
	}

	public Vector<Shot> getShots() {
		return shots;
	}

	public void setShots(Vector<Shot> shots) {
		this.shots = shots;
	}

	public EnemyTank(int x, int y) {
		super(x, y);
	}

	public boolean isTouchOtherEnemyTank(){
		for (int i = 0; i < ets.size(); i++) {
			EnemyTank et = ets.get(i);
			if (et!= this) {
				switch (this.getDirect()) {
				//向上
				case 0:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//左边的点
						if (this.getX() - 11 >= et.getX()-15 && this.getX() - 11 <= et.getX() + 15 && //横坐标
						this.getY() - 15 >= et.getY() - 11 && this.getY() - 15 <= et.getY() + 11) {//纵坐标
							return true;
						}
						//右边的点
						if (this.getX() + 11 >= et.getX()-15 && this.getX() + 11 <= et.getX() + 15 && //横坐标
						this.getY() - 15 >= et.getY() - 11 && this.getY() - 15 <= et.getY() + 11) {//纵坐标
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//左边的点
						if (this.getX() - 11 >= et.getX()-11 && this.getX() - 11 <= et.getX() + 11 && //横坐标
						this.getY() - 15 >= et.getY() - 15 && this.getY() - 15 <= et.getY() + 15) {//纵坐标
							return true;
						}
						//右边的点
						if (this.getX() + 11 >= et.getX()-11 && this.getX() + 11 <= et.getX() + 11 && //横坐标
						this.getY() - 15 >= et.getY() - 15 && this.getY() - 15 <= et.getY() + 15) {//纵坐标
							return true;
						}
					}
					break;
				//向右
				case 1:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//上边的点
						if (this.getX() + 15 >= et.getX()-15 && this.getX() + 15 <= et.getX() + 15 && //横坐标
						this.getY() - 11 >= et.getY() - 11 && this.getY() - 11 <= et.getY() + 11) {//纵坐标
							return true;
						}
						//下边的点
						if (this.getX() + 15 >= et.getX()-15 && this.getX() + 15 <= et.getX() + 15 && //横坐标
						this.getY() + 11 >= et.getY() - 11 && this.getY() + 11 <= et.getY() + 11) {//纵坐标
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//上边的点
						if (this.getX() + 15 >= et.getX()-11 && this.getX() + 15 <= et.getX() + 11 && //横坐标
						this.getY() - 11 >= et.getY() - 15 && this.getY() - 11 <= et.getY() + 15) {//纵坐标
							return true;
						}
						//下边的点
						if (this.getX() + 15 >= et.getX()-11 && this.getX() + 15 <= et.getX() + 11 && //横坐标
						this.getY() + 11 >= et.getY() - 15 && this.getY() + 11 <= et.getY() + 15) {//纵坐标
							return true;
						}
					}
					break;
				//向下
				case 2:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//左边的点
						if (this.getX() - 11 >= et.getX()-15 && this.getX() - 11 <= et.getX() + 15 && //横坐标
						this.getY() + 15 >= et.getY() - 11 && this.getY() + 15 <= et.getY() + 11) {//纵坐标
							return true;
						}
						//右边的点
						if (this.getX() + 11 >= et.getX()-15 && this.getX() + 11 <= et.getX() + 15 && //横坐标
						this.getY() + 15 >= et.getY() - 11 && this.getY() + 15 <= et.getY() + 11) {//纵坐标
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//左边的点
						if (this.getX() - 11 >= et.getX()-11 && this.getX() - 11 <= et.getX() + 11 && //横坐标
						this.getY() + 15 >= et.getY() - 15 && this.getY() + 15 <= et.getY() + 15) {//纵坐标
							return true;
						}
						//右边的点
						if (this.getX() + 11 >= et.getX()-11 && this.getX() + 11 <= et.getX() + 11 && //横坐标
						this.getY() + 15 >= et.getY() - 15 && this.getY() + 15 <= et.getY() + 15) {//纵坐标
							return true;
						}
					}
					break;
				//向左
				case 3:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//上边的点
						if (this.getX() - 15 >= et.getX()-15 && this.getX() - 15 <= et.getX() + 15 && //横坐标
						this.getY() - 11 >= et.getY() - 11 && this.getY() - 11 <= et.getY() + 11) {//纵坐标
							return true;
						}
						//下边的点
						if (this.getX() - 15 >= et.getX()-15 && this.getX() - 15 <= et.getX() + 15 && //横坐标
						this.getY() + 11 >= et.getY() - 11 && this.getY() + 11 <= et.getY() + 11) {//纵坐标
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//上边的点
						if (this.getX() - 15 >= et.getX()-11 && this.getX() - 15 <= et.getX() + 11 && //横坐标
						this.getY() - 11 >= et.getY() - 15 && this.getY() - 11 <= et.getY() + 15) {//纵坐标
							return true;
						}
						//下边的点
						if (this.getX() - 15 >= et.getX()-11 && this.getX() - 15 <= et.getX() + 11 && //横坐标
						this.getY() + 11 >= et.getY() - 15 && this.getY() + 11 <= et.getY() + 15) {//纵坐标
							return true;
						}
					}
					break;
				}
			}
		}
		return false;
	}
	
	
	@Override
	public void run() {   
		while (true) {
			if (this.getSpeed()!= 0 ) {
				switch (this.getDirect()) {
				case 0:
					for (int i = 0; i < 30; i++) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						if (this.getY()-20>=0 && !isTouchOtherEnemyTank()) {
							if (!(this.getX()+11 >= 200 && this.getX()-11<=400 && this.getY()-20<=280 && this.getY()-20>=250)) {
								this.setY(this.getY()-this.getSpeed());
							}
						}
					}
					break;
				case 1:
					for (int i = 0; i < 30; i++) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (this.getX()+20<=600) {
							if ((!(this.getX()+20 >= 200 && this.getX()+20<=400 && this.getY()-11<=280 && this.getY()+11>=250)) &&
								(!(this.getX()+20 >= 260 && this.getX()+20<=330 && this.getY()+11<=400 && this.getY()-11>=350))) {
									this.setX(this.getX()+this.getSpeed());
							}
						}
					}
					break;
				case 2:
					for (int i = 0; i < 30; i++) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (this.getY()+20<=400) {
							if ((!(this.getX()+11 >= 200 && this.getX()-11 <= 400 && this.getY()+20 <= 280 && this.getY()+20 >= 250)) &&
								(!(this.getX()+11 >= 260 && this.getX()-11 <= 330 && this.getY()+20 <= 400 && this.getY()+20 >=350))) {
									this.setY(this.getY()+this.getSpeed());
							}
						}
					}
					break;
				case 3:
					for (int i = 0; i < 30; i++) {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						if (this.getX()-20>=0) {
							if ((!(this.getX()-20 >= 200 && this.getX()-20<=400 && this.getY()-11<=280 && this.getY()+11>=250)) &&
								(!(this.getX()-20 >= 260 && this.getX()-20<=330 && this.getY()+11<=400 && this.getY()-11>=350))) {
									this.setX(this.getX()-this.getSpeed());
							}
						}
						
					}
					break;
				}
				int direct = (int) (Math.random()*4);
				this.setDirect(direct);
				
				if (this.getShots().size()<3) {
					Shot shot = null;
					switch (this.getDirect()) {
						case 0:
							shot = new Shot(this.getX(), this.getY()-20);
						break;
						case 1:
							shot = new Shot(this.getX()+20, this.getY());
						break;
						case 2:
							shot = new Shot(this.getX(), this.getY()+20);
						break;
						case 3:
							shot = new Shot(this.getX()-20, this.getY());
						break;
					
					}
					shot.setDirect(this.getDirect());
					new Thread(shot).start();
					this.getShots().add(shot);
				}
			}
		}
	}

}
