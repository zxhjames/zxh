package com.zyh.demo;

public class Shot implements Runnable{
	private int x;
	private int y;
	private int direct;
	private int speed=6;
	private boolean isLive = true;
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}

	public int getDirect() {
		return direct;
	}
	public void setDirect(int direct) {
		this.direct = direct;
	}

	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public boolean isLive() {
		return isLive;
	}
	public void setLive(boolean isLive) {
		this.isLive = isLive;
	}


	public Shot(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	@Override
	public void run() {
		while(true){
			if (this.getSpeed()!= 0) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				switch (direct) {//根据坦克方向判断子弹运行方向
					case 0:
						y-=speed;
						break;
					case 1:
						x+=speed;
						break;
					case 2:
						y+=speed;
						break;
					case 3:
						x-=speed;
						break;
				}
				if((x<=0 || x>=600 || y<=0 || y>=400)||(x>=275 && x<=325 && y>=350 && y<=360)){//控制子弹范围
					isLive = false;
					break;
				}
			}
		}
	}
}
