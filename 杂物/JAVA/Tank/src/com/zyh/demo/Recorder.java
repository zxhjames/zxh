package com.zyh.demo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Vector;

public class Recorder {
	public static int enemyTankCount = 6;
	public static int myTankLife = 3;
	public static int score = 0;
	public static int myTankX = 250;
	public static int myTankY = 385;
	public static int myTankDirect = 0;
	public static int[] enemyTankX = new int [enemyTankCount];
	public static int[] enemyTankY = new int [enemyTankCount];
	public static int[] enemyTankDirect = new int [enemyTankCount];
	public static int[] enemyTankType = new int[enemyTankCount];
	public static String hasRecord;
	
	public static void saveRecord(MyTank mt,Vector<EnemyTank> ets){
		myTankX = mt.getX();
		myTankY = mt.getY();
		myTankDirect = mt.getDirect();
		for (int i = 0; i < enemyTankCount; i++) {
			EnemyTank et = ets.get(i);
			et.setEts(ets);
			ets.add(et);
			enemyTankX[i] = et.getX();
			enemyTankY[i] = et.getY();
			enemyTankDirect[i] = et.getDirect();
			enemyTankType[i] = et.getType();
		}
		
		File file = new File("src/record.txt");
		FileOutputStream fos = null;
		BufferedWriter bw = null;
		try {
			fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write("true---");
			bw.write(score+"---"+myTankX+"---"+myTankY+"---"+myTankDirect+"---"+enemyTankCount+"---"+myTankLife+"---");
			for (int i = 0; i < enemyTankCount; i++) {
				bw.write(enemyTankX[i]+"---"+enemyTankY[i]+"---"+enemyTankDirect[i]+"---"+enemyTankType[i]+"---");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (bw!= null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();  
				}
			}
		}
	}
	
	public static void readRecord(){
		File file = new File("src/record.txt");
		FileInputStream fis = null;
		BufferedReader br = null;
		try {
			fis = new FileInputStream(file);
			br = new BufferedReader(new InputStreamReader(fis));
			String readLine = br.readLine();
			if (readLine != null) {
				String[] strs = readLine.split("---");
				hasRecord = strs[0];
				score = Integer.parseInt(strs[1]);
				myTankX = Integer.parseInt(strs[2]);
				myTankY = Integer.parseInt(strs[3]);
				myTankDirect = Integer.parseInt(strs[4]);
				enemyTankCount = Integer.parseInt(strs[5]);
				myTankLife = Integer.parseInt(strs[6]);
				int j = 7;
				for (int i = 0; i < enemyTankCount; i++) {
					enemyTankX[i] = Integer.parseInt(strs[j]);
					j++;
					enemyTankY[i] = Integer.parseInt(strs[j]);
					j++;
					enemyTankDirect[i] = Integer.parseInt(strs[j]);
					j++;
					enemyTankType[i] = Integer.parseInt(strs[j]);
					j++;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (br!= null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void clear(){
		File file = new File("src/record.txt");
		FileOutputStream fos = null;
		BufferedWriter bw = null;
		try {
			fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write("");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bw!= null) {
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();  
				}
			}
		}
	}
}
