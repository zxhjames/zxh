package com.zyh.demo;

public class Bomb {
	private int x;
	private int y;
	private boolean isLive = true;
	private int live = 21;
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public boolean isLive() {
		return isLive;
	}
	public void setLive(boolean isLive) {
		this.isLive = isLive;
	}
	public int getLive() {
		return live;
	}
	public void setLive(int live) {
		this.live = live;
	}
	public Bomb(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public void liveDown(){
		if (live>0) {
			live--;
		}else {
			isLive = false;
		}
	}
}
