package com.zyh.demo;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

/*
 * 1、画出自身的坦克
 * 2、为坦克添加方向和速度，并且添加事件使坦克可以移动 
 * 3、添加敌人的坦克
 * 4、编写子弹类，使自己的坦克可以发射子弹
 * 5、发射多颗子弹
 * 6、自己的子弹消灭敌人的坦克并产生爆炸效果
 * 7、使敌人的坦克可以移动
 * 8、控制所有坦克的移动范围
 * 9、使敌人的坦克也可以发射子弹
 * 10、使敌人的坦克可以击中我
 * 11、使敌人的坦克不相互碰撞
 * 12、添加游戏记录
 * 13、添加障碍物

 */
public class demo extends JFrame{
	public static void main(String[] args) {
		new demo();
	}
	
	JPanel mp;
	
	JLabel title;
	
	public demo() {
		mp = new JPanel();
		this.add(mp);
		
		title = new JLabel();
		title.setFont(new Font("Times New Roman", Font.BOLD, 50));
		title.setLocation(50, 50);
		title.setText("AAAA");
		mp.add(title);
		
		this.setTitle("坦克大战");
		this.setSize(800, 600);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocation(100, 100);
	}

}
