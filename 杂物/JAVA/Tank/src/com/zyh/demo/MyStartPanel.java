package com.zyh.demo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyStartPanel extends JPanel implements Runnable{
	int times = 0;
	
	@Override
	public void paint(Graphics gra) {
		super.paint(gra);
		gra.fillRect(0, 0, 600, 400);
		gra.setColor(Color.YELLOW);
		gra.setFont(new Font("楷体", Font.BOLD, 50));
		if (times%2 != 0) {
			gra.drawString("Stage 1", 200, 190);
		}
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			times++;
			repaint();//进行重绘，反复调用方法
		}	
	}
}
