package com.zyh.demo;

import java.util.Vector;

public class MyTank extends Tank{
	private Shot shot;
	private Vector<Shot> shots = new Vector<>();
	
	public Vector<Shot> getShots() {
		return shots;
	}
	public void setShots(Vector<Shot> shots) {
		this.shots = shots;
	}
	public Shot getShot() {
		return shot;
	}
	public void setShot(Shot shot) {
		this.shot = shot;
	}
	
	public MyTank(int x, int y) {
		super(x, y);
	}
	
	//发射子弹
	public void shotEnemyTank(){
		if (this.isLive()) {
			switch (this.getDirect()) {
				case 0:
					shot = new Shot(this.getX(), this.getY()-20);
					shot.setDirect(this.getDirect());
					break;
				case 1:
					shot = new Shot(this.getX()+20, this.getY());
					shot.setDirect(this.getDirect());
					break;
				case 2:
					shot = new Shot(this.getX(), this.getY()+20);
					shot.setDirect(this.getDirect());
					break;
				case 3:
					shot = new Shot(this.getX()-20, this.getY());
					shot.setDirect(this.getDirect());
					break;
			}
			if (shots.size()<5) {
				shots.add(shot);
			}
			new Thread(shot).start();
		}
	}

	//坦克移动方向
	public void moveUp(){
		this.setY(this.getY()-this.getSpeed());
	}
	public void moveRight(){
		this.setX(this.getX()+this.getSpeed());
	}
	public void moveDown(){
		this.setY(this.getY()+this.getSpeed());
	}
	public void moveLeft(){
		this.setX(this.getX()-this.getSpeed());
	}
}
