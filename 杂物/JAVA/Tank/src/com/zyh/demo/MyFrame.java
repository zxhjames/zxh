package com.zyh.demo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/*
 * 1、画出自身的坦克
 * 2、为坦克添加方向和速度，并且添加事件使坦克可以移动 
 * 3、添加敌人的坦克
 * 4、编写子弹类，使自己的坦克可以发射子弹
 * 5、发射多颗子弹
 * 6、自己的子弹消灭敌人的坦克并产生爆炸效果
 * 7、使敌人的坦克可以移动
 * 8、控制所有坦克的移动范围
 * 9、使敌人的坦克也可以发射子弹
 * 10、使敌人的坦克可以击中我
 * 11、使敌人的坦克不相互碰撞
 * 12、添加游戏记录
 * 13、添加障碍物

 */
public class MyFrame extends JFrame implements ActionListener{
	public static void main(String[] args) {
		new MyFrame();
	}
	
	MyPanel mp;
	MyStartPanel msp;
	
	JMenuBar jmb;
	JMenu jm1,jm2,jm3;
	JMenuItem jmi1,jmi2,jmi3,jmi4,jmi5;
	
	public MyFrame() {
		msp = new MyStartPanel();
		this.add(msp);
		new Thread(msp).start();
		
		jmb = new JMenuBar();
		
		jm1 = new JMenu("游戏（G）");
		jm1.setMnemonic('G');
		jm2 = new JMenu("进度");
		jm3 = new JMenu("帮助");
		
		jmi1 = new JMenuItem("开始新游戏");
		jmi1.addActionListener(this);
		jmi1.setActionCommand("newGame");
		
		jmi2 = new JMenuItem("暂停游戏");
		jmi2.addActionListener(this);
		jmi2.setActionCommand("stop");
		
		jmi3 = new JMenuItem("继续游戏");
		jmi3.addActionListener(this);
		jmi3.setActionCommand("continue");
		
		jmi4 = new JMenuItem("保存游戏进度并退出");
		jmi4.addActionListener(this);
		jmi4.setActionCommand("exit");
		
		jmi5 = new JMenuItem("继续记录中的游戏");
		jmi5.addActionListener(this);
		jmi5.setActionCommand("read");
		
		jm1.add(jmi1);
		jm1.add(jmi5);
		jm1.add(jmi2);
		jm1.add(jmi3);
		jm1.add(jmi4);
		
		jmb.add(jm1);
		jmb.add(jm2);
		jmb.add(jm3);
		
		this.setJMenuBar(jmb);
		
		this.setTitle("坦克大战");
		this.setSize(800, 600);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("newGame".equals(e.getActionCommand())) {
			Recorder.clear();
			MyPlayer myPlayer = new MyPlayer("HaruHaru.mp3");
			new Thread(myPlayer).start();
			mp = new MyPanel();
			new Thread(mp).start();
			this.addKeyListener(mp);
			this.add(mp);
			this.remove(msp);
			this.setVisible(true);
		}else if ("stop".equals(e.getActionCommand())) {
			MyTank mt = mp.mt;
			mt.setSpeed(0);
			for (int i = 0; i < mp.ets.size(); i++) {
				EnemyTank et = mp.ets.get(i);
				et.setSpeed(0);
				for (int j = 0; j < et.getShots().size(); j++) {
					Shot shot = et.getShots().get(j);
					shot.setSpeed(0);
				}
			}
		}else if ("continue".equals(e.getActionCommand())) {
			MyTank mt = mp.mt;
			mt.setSpeed(4);
			for (int i = 0; i < mp.ets.size(); i++) {
				EnemyTank et = mp.ets.get(i);
				et.setSpeed(3);
				for (int j = 0; j < et.getShots().size(); j++) {
					Shot shot = et.getShots().get(j);
					shot.setSpeed(4);
				}
			}
		}else if ("read".equals(e.getActionCommand())) {
			mp = new MyPanel();
			new Thread(mp).start();
			this.addKeyListener(mp);
			this.add(mp);
			this.remove(msp);
			this.setVisible(true);
		}else if ("exit".equals(e.getActionCommand())) {
			Recorder.clear();
			Recorder.saveRecord(mp.mt,mp.ets);
			System.exit(0);
		}
	}
}
