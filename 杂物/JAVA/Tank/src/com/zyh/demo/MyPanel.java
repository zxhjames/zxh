package com.zyh.demo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JPanel;

public class MyPanel extends JPanel implements KeyListener,Runnable{
	MyTank mt;
	Vector<EnemyTank> ets = new Vector<>();
	Vector<Bomb> bombs = new Vector<>();
	Image img1,img2,img3,img4,img5,img6,img7,img8,img9,img10;
	int etsSize = 4;//敌方坦克数量
	int count = 0;//射击次数
	int life = 3;//我方坦克生命
	
	public MyPanel() {
		Recorder.readRecord();
		
		img1 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/bomb_1.gif"));
		img2 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/bomb_2.gif"));
		img3 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/bomb_3.gif"));
		img4 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/home.png"));
		img5 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/steel.gif"));
		img6 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/grass.png"));
		img7 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/over.gif"));
		img8 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/water.gif"));
		img9 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/back.jpg"));
		img10 = Toolkit.getDefaultToolkit().getImage(Panel.class.getResource("/shot.gif"));
		
		//初始化我方坦克
		mt = new MyTank(Recorder.myTankX, Recorder.myTankY);
		mt.setDirect(Recorder.myTankDirect);
		mt.setSpeed(5);
		mt.setType(0);
		
		//初始化敌方坦克集合
		if (Recorder.enemyTankCount>=etsSize) {
			for (int i = 0; i < etsSize; i++) {
				if (("true").equals(Recorder.hasRecord)) {
					EnemyTank et = new EnemyTank(Recorder.enemyTankX[i], Recorder.enemyTankY[i]);
					et.setDirect(Recorder.enemyTankDirect[i]);
					if (Recorder.enemyTankType[i]==1) {
						et.setType(1);
						et.setSpeed(3);
					}else if(Recorder.enemyTankType[i]==2){
						et.setType(2);
						et.setSpeed(4);
					}
					new Thread(et).start();
					//初始化敌方子弹
					Shot shot = new Shot(et.getX(), et.getY()+20);
					shot.setDirect(et.getDirect());
					et.getShots().add(shot);
					new Thread(shot).start();
					
					et.setEts(ets);
					ets.add(et);
				}else {
					EnemyTank et = new EnemyTank(50+i*150+10, 15);
					et.setDirect(2);
					et.setSpeed(3);
					et.setType(1);
					new Thread(et).start();
					//初始化敌方子弹
					Shot shot = new Shot(et.getX(), et.getY()+20);
					shot.setDirect(et.getDirect());
					et.getShots().add(shot);
					new Thread(shot).start();
					
					et.setEts(ets);
					ets.add(et);
				}
			}
		}else{
			for (int i = 0; i < Recorder.enemyTankCount; i++) {
				EnemyTank et = new EnemyTank(Recorder.enemyTankX[i], Recorder.enemyTankY[i]);
				et.setDirect(Recorder.enemyTankDirect[i]);
				if (Recorder.enemyTankType[i]==1) {
					et.setType(1);
					et.setSpeed(3);
				}else if(Recorder.enemyTankType[i]==2){
					et.setType(2);
					et.setSpeed(4);
				}
				new Thread(et).start();
					
				//初始化敌方子弹
				Shot shot = new Shot(et.getX(), et.getY()+20);
				shot.setDirect(et.getDirect());
				et.getShots().add(shot);
				new Thread(shot).start();
				
				et.setEts(ets);
				ets.add(et);
			}
		}
	}
	
	@Override
	public void paint(Graphics gra) {
		//画出游戏界面
		super.paint(gra);
		gra.drawImage(img9, 0, 0, 600, 400, null);
		gra.drawImage(img4, 285, 370, 30, 30, null);
		for (int i = 0; i < 5; i++) {
			gra.drawImage(img5, 275+i*10, 350, 10, 10, null);
		}
		
		for(int i = 0;i < 4;i++){
			gra.drawImage(img8, 200+i*50, 250, 50, 30, this);
		}
		
		//剩余坦克数量及生命值
		drawTank(50, 470, gra, 0, 1);
		drawTank(120, 470, gra, 0, 0);
		gra.setColor(Color.BLACK);
		gra.setFont(new Font("宋", Font.BOLD, 20));
		gra.drawString("剩余数量：", 20, 440);
		gra.setFont(new Font("楷体", Font.BOLD, 20));
		gra.drawString(Recorder.enemyTankCount+"", 65, 480);
		if (Recorder.myTankLife>=0) {
			gra.drawString(Recorder.myTankLife+"", 135, 480);
		}else {
			gra.drawString("0", 135, 480);
		}
		
		//记录
		gra.setColor(Color.BLACK);
		gra.setFont(new Font("宋体", Font.BOLD, 15));
		gra.drawString("您的总战绩是：", 610, 50);
		gra.setFont(new Font("楷体", Font.BOLD, 20));
		drawTank(630, 90, gra, 0, 1);
		gra.drawString(Recorder.score+"", 650, 95);
		
		//画出我方坦克
		if (mt.isLive()) {
			drawTank(mt.getX(), mt.getY(), gra, mt.getDirect(), mt.getType());
		}
		
		//画出敌方坦克
		if (Recorder.enemyTankCount>0) {
			for (int i = 0; i < ets.size(); i++) {
				EnemyTank et = ets.get(i);//从集合里取出敌方坦克
				if (et!=null && et.isLive()) {
					drawTank(et.getX(), et.getY(), gra, et.getDirect(), et.getType());
					for (int j = 0; j < et.getShots().size(); j++) {
						Shot shot = et.getShots().get(j);
						if (shot!=null && shot.isLive()) {
							gra.drawImage(img10, shot.getX()-2, shot.getY()-2, 5, 5, this);
						}
						if (!shot.isLive()) {
							et.getShots().remove(shot);
						}
					}
				}else if(et!=null && !et.isLive()){
					ets.remove(et);
				}
			}
		}
			
		for(int i = 0;i < 12;i++){
			gra.drawImage(img6, 0+i*50, 130, 50, 50, this);
		}
		
		//如果我方生命值为0
			if (Recorder.myTankLife<0) {
				gra.drawImage(img7, 180, 100, 240, 135, this);
				ets.clear();
				mt.setLive(false);
			}
			
		//如果我方基地被击中
			if (hitPoint() == true) {
				Recorder.myTankLife=-1;
				ets.clear();
				mt.setLive(false);
			}
			
		//敌方剩余数量为0
		if (Recorder.enemyTankCount<=0) {
			gra.setColor(Color.YELLOW);
			gra.setFont(new Font("楷体", Font.BOLD, 80));
			gra.drawString("YOU WIN", 140, 110);
			if (count == 5 || life == 3) {
				gra.setColor(Color.BLACK);
				gra.setFont(new Font("宋体", Font.BOLD, 15));
				gra.drawString("达成成就：", 610, 150);
				if (count == 5 && life != 3) {
					gra.drawString("神枪手", 610, 170);
				}else if (life == 3 && count != 5){
					gra.drawString("无生命损失", 610, 170);
				}else if(count == 5 && life == 3){
					gra.drawString("神枪手", 610, 170);
					gra.drawString("无生命损失", 610, 190);
				}
			}
			ets.clear();
			mt.setLive(false);
		}
		
		//画出爆炸效果
		for (int i = 0; i < bombs.size(); i++) {
			Bomb b = bombs.get(i);
			if (b.isLive()) {
				if (b.getLive()>14) {
					gra.drawImage(img1, b.getX(), b.getY(), 32, 30, this);
				}else if(b.getLive()>7){
					gra.drawImage(img2, b.getX(), b.getY(), 32, 30, this);
				}else {
					gra.drawImage(img3, b.getX(), b.getY(), 32, 30, this);
				}
			}
			b.liveDown();
			if (b.getLive()==0) {
				bombs.remove(b);
			}
		}
		
		//发射子弹
		Vector<Shot> shots = mt.getShots();
		
		for (int i = 0; i < shots.size(); i++) {
			Shot shot = shots.get(i);
			if (shot!=null && shot.isLive()) {
				gra.drawImage(img10, shot.getX()-2, shot.getY()-2, 5, 5, this);
			}
			if (!shot.isLive()) {
				shots.remove(shot);
			}
		}
		
	}
	
	//消灭坦克
	public boolean hitTank(Shot shot,Tank t){
		switch (t.getDirect()) {
		case 0:
		case 2:
			if (shot.getX() > t.getX()-11 && shot.getX() < t.getX()+11 && shot.getY() > t.getY()-15 && shot.getY() < t.getY()+15) {
				shot.setLive(false);
				t.setLive(false);
				Bomb b = new Bomb(t.getX()-11, t.getY()-15);
				bombs.add(b);
				return true;
			}
			break;
		case 1:
		case 3:
			if (shot.getX() > t.getX()-15 && shot.getX() < t.getX()+15 && shot.getY() > t.getY()-12 && shot.getY() < t.getY()+12) {
				shot.setLive(false);
				t.setLive(false);
				Bomb b = new Bomb(t.getX()-15, t.getY()-11);
				bombs.add(b);
				return true;
			}
			break;
		}
		return false;
	}
	
	//消灭敌方坦克
	public void hitEnemyTank(){
		for (int i = 0; i < mt.getShots().size(); i++) {
			Shot shot = mt.getShots().get(i);
			if (shot.isLive()) {
				for (int j = 0; j < ets.size(); j++) {
					EnemyTank et = ets.get(j);
					if (et.isLive()) {
						boolean b = hitTank(shot, et);
						if (b) {
							Recorder.enemyTankCount--;
							Recorder.score++;
							if (Recorder.enemyTankCount>=etsSize) {
								int r = (int) (Math.random()*3);
								if (r==0) {
									EnemyTank et1 = new EnemyTank(11, 15);
									et1.setDirect(2);
									et1.setSpeed(4);
									et1.setType(2);
									new Thread(et1).start();
									ets.add(et1);
									et1.setEts(ets);
								}else if(r==1){
									EnemyTank et1 = new EnemyTank(300, 15);
									et1.setDirect(2);
									et1.setSpeed(4);
									et1.setType(2);
									new Thread(et1).start();
									ets.add(et1);
									et1.setEts(ets);
								}else {
									EnemyTank et1 = new EnemyTank(589, 15);
									et1.setDirect(2);
									et1.setSpeed(4);
									et1.setType(2);
									new Thread(et1).start();
									ets.add(et1);
									et1.setEts(ets);
								}
							}else {
								etsSize--;
							}
						}
					}
				}
			}
		}
	}

	
	//消灭我方坦克
	public void hitMyTank(){
		for (int i = 0; i < ets.size(); i++) {
			EnemyTank et = ets.get(i);
			if (et.isLive()) {
				for (int j = 0; j < et.getShots().size(); j++) {
					Shot shot = et.getShots().get(j);
					if (shot!=null && shot.isLive()) {
						boolean b = hitTank(shot, mt);
						if (b) {
							Recorder.myTankLife--;
							life--;
							mt = new MyTank(250, 385);
							mt.setDirect(0);
							mt.setSpeed(3);
							mt.setType(0);
						}
					}
				}
			}
		}
	}
	
	//击中我方基地
	public boolean hitPoint(){
		for (int i = 0; i < ets.size(); i++) {
			EnemyTank et = ets.get(i);
			for (int j = 0; j < et.getShots().size(); j++) {
				Shot shot = et.getShots().get(j);
				if (shot!=null && shot.isLive()){
					if (shot.getX() > 285 && shot.getX() < 315 && shot.getY() > 370 && shot.getY() < 400) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	//我方坦克与敌方坦克相撞
		public boolean isTouch(){
			for (int i = 0; i < ets.size(); i++) {
				EnemyTank et = ets.get(i);
				switch (mt.getDirect()) {
				//向上
				case 0:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//左边的点
						if (mt.getX() - 11 >= et.getX()-15 && mt.getX() - 11 <= et.getX() + 15 && //横坐标
						mt.getY() - 15 >= et.getY() - 11 && mt.getY() - 15 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//右边的点
						if (mt.getX() + 11 >= et.getX()-15 && mt.getX() + 11 <= et.getX() + 15 && //横坐标
						mt.getY() - 15 >= et.getY() - 11 && mt.getY() - 15 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//左边的点
						if (mt.getX() - 11 >= et.getX()-11 && mt.getX() - 11 <= et.getX() + 11 && //横坐标
						mt.getY() - 15 >= et.getY() - 15 && mt.getY() - 15 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//右边的点
						if (mt.getX() + 11 >= et.getX()-11 && mt.getX() + 11 <= et.getX() + 11 && //横坐标
						mt.getY() - 15 >= et.getY() - 15 && mt.getY() - 15 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					break;
				//向右
				case 1:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//上边的点
						if (mt.getX() + 15 >= et.getX()-15 && mt.getX() + 15 <= et.getX() + 15 && //横坐标
						mt.getY() - 11 >= et.getY() - 11 && mt.getY() - 11 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//下边的点
						if (mt.getX() + 15 >= et.getX()-15 && mt.getX() + 15 <= et.getX() + 15 && //横坐标
						mt.getY() + 11 >= et.getY() - 11 && mt.getY() + 11 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//上边的点
						if (mt.getX() + 15 >= et.getX()-11 && mt.getX() + 15 <= et.getX() + 11 && //横坐标
						mt.getY() - 11 >= et.getY() - 15 && mt.getY() - 11 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//下边的点
						if (mt.getX() + 15 >= et.getX()-11 && mt.getX() + 15 <= et.getX() + 11 && //横坐标
						mt.getY() + 11 >= et.getY() - 15 && mt.getY() + 11 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					break;
				//向下
				case 2:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//左边的点
						if (mt.getX() - 11 >= et.getX()-15 && mt.getX() - 11 <= et.getX() + 15 && //横坐标
						mt.getY() + 15 >= et.getY() - 11 && mt.getY() + 15 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//右边的点
						if (mt.getX() + 11 >= et.getX()-15 && mt.getX() + 11 <= et.getX() + 15 && //横坐标
						mt.getY() + 15 >= et.getY() - 11 && mt.getY() + 15 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//左边的点
						if (mt.getX() - 11 >= et.getX()-11 && mt.getX() - 11 <= et.getX() + 11 && //横坐标
						mt.getY() + 15 >= et.getY() - 15 && mt.getY() + 15 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//右边的点
						if (mt.getX() + 11 >= et.getX()-11 && mt.getX() + 11 <= et.getX() + 11 && //横坐标
						mt.getY() + 15 >= et.getY() - 15 && mt.getY() + 15 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					break;
				//向左
				case 3:
					if (et.getDirect()== 1 || et.getDirect() == 3) {//其他坦克向左或向右
						//上边的点
						if (mt.getX() - 15 >= et.getX()-15 && mt.getX() - 15 <= et.getX() + 15 && //横坐标
						mt.getY() - 11 >= et.getY() - 11 && mt.getY() - 11 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//下边的点
						if (mt.getX() - 15 >= et.getX()-15 && mt.getX() - 15 <= et.getX() + 15 && //横坐标
						mt.getY() + 11 >= et.getY() - 11 && mt.getY() + 11 <= et.getY() + 11) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					if (et.getDirect()== 0 || et.getDirect() == 2) {//其他坦克向上或向下
						//上边的点
						if (mt.getX() - 15 >= et.getX()-11 && mt.getX() - 15 <= et.getX() + 11 && //横坐标
						mt.getY() - 11 >= et.getY() - 15 && mt.getY() - 11 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
						//下边的点
						if (mt.getX() - 15 >= et.getX()-11 && mt.getX() - 15 <= et.getX() + 11 && //横坐标
						mt.getY() + 11 >= et.getY() - 15 && mt.getY() + 11 <= et.getY() + 15) {//纵坐标
							mt.setLive(false);
							et.setLive(false);
							Bomb b1 = new Bomb(mt.getX()-11, mt.getY()-15);
							bombs.add(b1);
							Bomb b2 = new Bomb(et.getX()-11, et.getY()-15);
							bombs.add(b2);
							return true;
						}
					}
					break;
				}
			}
			return false;
		}
		
		
		//相撞后重生的方法
		public void TouchTank() {
			for (int i = 0; i < ets.size(); i++) {
				EnemyTank et = ets.get(i);
				if(et.isLive() && mt.isLive()) {
					boolean touch = isTouch();
					if(touch) {
						Recorder.enemyTankCount--;
						Recorder.score++;
						Recorder.myTankLife--;
						life--;
						mt = new MyTank(250, 385);
						mt.setDirect(0);
						mt.setSpeed(3);
						mt.setType(0);
						if(Recorder.enemyTankCount>=etsSize) {
							int r = (int) (Math.random()*4);
							if(r==0) {
								EnemyTank et1 = new EnemyTank(10,15);
								et1.setDirect(2);
								et1.setSpeed(4);
								et1.setType(2);
								new Thread(et1).start();
								ets.add(et1);
								et1.setEts(ets);
							}else if(r==1) {
								EnemyTank et1 = new EnemyTank(140,15);
								et1.setDirect(2);
								et1.setSpeed(4);
								et1.setType(2);
								new Thread(et1).start();
								ets.add(et1);
								et1.setEts(ets);
							}else if(r==2) {
								EnemyTank et1 = new EnemyTank(270,15);
								et1.setDirect(2);
								et1.setSpeed(4);
								et1.setType(2);
								new Thread(et1).start();
								ets.add(et1);
								et1.setEts(ets);
							}else if(r==3) {
								EnemyTank et1 = new EnemyTank(400,15);
								et1.setDirect(2);
								et1.setSpeed(4);
								et1.setType(2);
								new Thread(et1).start();
								ets.add(et1);
								et1.setEts(ets);
							}else {
								EnemyTank et1 = new EnemyTank(530,15);
								et1.setDirect(2);
								et1.setSpeed(4);
								et1.setType(2);
								new Thread(et1).start();
								ets.add(et1);
								et1.setEts(ets);
							}
						}
					}
				}
			}
		}
	
	//画坦克
	public void drawTank(int x, int y, Graphics gra, int direct, int type){
		switch (type) {
			case 0://我方坦克
				gra.setColor(Color.GRAY);
			break;
			case 1://第一种敌方坦克
				gra.setColor(Color.RED);
			break;
			case 2 ://第二种敌方坦克
				gra.setColor(Color.GREEN);
		}
		switch (direct) {
			case 0://向上
				gra.fill3DRect(x-10, y-15, 5, 30,false);//左边长方形
				gra.fill3DRect(x+7, y-15, 5, 30,false);//右边长方形
				gra.fill3DRect(x-5, y-10, 12, 20,false);//中间矩形
				gra.fillOval(x-5, y-5, 10, 10);//中间圆形
				gra.fillRect(x-1, y-20, 3, 20);//中间线段
				break;
			case 1://向右
				gra.fill3DRect(x-15, y-10, 30, 5,false);//上方长方形
				gra.fill3DRect(x-15, y+7, 30, 5,false);//下方长方形
				gra.fill3DRect(x-10, y-5, 20, 12,false);//中间矩形
				gra.fillOval(x-5, y-5, 10, 10);//中间圆形
				gra.fillRect(x, y-1, 20, 3);//中间线段
				break;
			case 2://向下
				gra.fill3DRect(x-10, y-15, 5, 30,false);//左边长方形
				gra.fill3DRect(x+7, y-15, 5, 30,false);//右边长方形
				gra.fill3DRect(x-5, y-10, 12, 20,false);//中间矩形
				gra.fillOval(x-5, y-5, 10, 10);//中间圆形
				gra.fillRect(x-1, y, 3, 20);//中间线段
				break;
			case 3://向左
				gra.fill3DRect(x-15, y-10, 30, 5,false);//上方长方形
				gra.fill3DRect(x-15, y+7, 30, 5,false);//下方长方形
				gra.fill3DRect(x-10, y-5, 20, 12,false);//中间矩形
				gra.fillOval(x-5, y-5, 10, 10);//中间圆形
				gra.fillRect(x-20, y-1, 20, 3);//中间线段
				break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	//坦克改变方向
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == e.VK_W || e.getKeyCode() == e.VK_UP) {
			mt.setDirect(0);
			if (mt.getY()-20>=0) {
				if ((!(mt.getX()+11 >= 200 && mt.getX()-11<=400 && mt.getY()-20<=280 && mt.getY()-20>=250)) &&
					(!(mt.getX()+11 >= 275 && mt.getX()-11 <= 325 && mt.getY()-20 <= 360 && mt.getY()-20 >=350))) {
				mt.moveUp();
				}
			}
		}else if (e.getKeyCode() == e.VK_D|| e.getKeyCode() == e.VK_RIGHT){
			mt.setDirect(1);
			if (mt.getX()+20<=600) {
				if ((!(mt.getX()+20 >= 200 && mt.getX()+20<=400 && mt.getY()-11<=280 && mt.getY()+11>=250)) &&
					(!(mt.getX()+20 >= 275 && mt.getX()+20<=325 && mt.getY()+11<=360 && mt.getY()-11>=350))) {
						mt.moveRight();
				}
			}
		}else if (e.getKeyCode() == e.VK_S|| e.getKeyCode() == e.VK_DOWN){
			mt.setDirect(2);
			if (mt.getY()+20<=400) {
				if ((!(mt.getX()+11 >= 200 && mt.getX()-11 <= 400 && mt.getY()+20 <= 280 && mt.getY()+20 >= 250)) &&
					(!(mt.getX()+11 >= 275 && mt.getX()-11 <= 325 && mt.getY()+20 <= 360 && mt.getY()+20 >=350))) {
						mt.moveDown(); 
				}
			}
		}else if (e.getKeyCode() == e.VK_A|| e.getKeyCode() == e.VK_LEFT){
			mt.setDirect(3);
			if (mt.getX()-20>=0) {
				if ((!(mt.getX()-20 >= 200 && mt.getX()-20<=400 && mt.getY()-11<=280 && mt.getY()+11>=250)) &&
					(!(mt.getX()-20 >= 275 && mt.getX()-20<=325 && mt.getY()+11<=360 && mt.getY()-11>=350))) {
							mt.moveLeft();
				}
			}
		}
		if (e.getKeyCode() == e.VK_J) {
			count++;
			mt.shotEnemyTank();
		}
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
			hitEnemyTank();
			hitMyTank();	
			TouchTank();
			repaint();
		}
	}
}
