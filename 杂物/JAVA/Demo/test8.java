interface Ioperator 
{
	void add(Object o1,Object o2);
	//void sub(Object o1,Object o2);
	void print(Object o);
}

class Complex//定义一个复数类型
{
	private int real;
	private int imaginary;
	public int getreal()
	{
		return this.real;
	}

	public void setreal(int real)
	{
		this.real=real;
	}

	public int getImaginary()
	{
		return this.imaginary;
	}

	public void setImaginary(int imag)
	{
		this.imaginary=imag;
	}

}


//实现接口的复数操作
class ComplexImpl implements Ioperator 
{
	public void add(Object o1,Object o2)
	{
		Complex c=new Complex();
		Complex c1=(Complex)o1;
		Complex c2=(Complex)o2;
		c.setreal(c1.getreal()+c2.getreal());
		c.setImaginary(c1.getImaginary()+c2.getImaginary());
		print(c);
	}

	public void print(Object o)
	{
		Complex c=(Complex)o;
		System.out.println(c.getreal()+"+"+c.getImaginary()+"i");
	}
}

public class test8
{
	public static void main(String[] args) {
		Complex c1=new Complex();
		c1.setreal(2);
		c1.setImaginary(4);
		Complex c2=new Complex();
		c2.setreal(3);
		c2.setImaginary(3);
		Ioperator o=new ComplexImpl();
		o.add(c1,c2);
		//o.sub(c1,c2);
	}
}