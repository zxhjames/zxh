class Complex
{
	int real;
	int imag;
	Complex()
	{
		this.real=0;
		this.imag=0;
	}
	Complex(int r,int i)
	{
		this.real=r;
		this.imag=i;
	}
	Complex complexAdd(Complex a) 
	{
		this.real=this.real+a.real;
		this.imag=this.imag+a.imag;
		return a;
	}
}
public class test16
{
	public static void main(String[] args) {
		Complex b1=new Complex();
		System.out.println(b1.real+" "+b1.imag);
		Complex b2=new Complex(2,2);
		System.out.println(b2.real+" "+b2.imag);
		Complex a=new Complex(3,3);
		a.complexAdd(b1);
		System.out.println(a.real+" "+a.imag);
		//System.out.println(a.complexAdd(b2));
	}
}