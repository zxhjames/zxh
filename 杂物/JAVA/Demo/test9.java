/**************对象一，动物**********************/
interface Animal 
{
	public void eat(Food food);
}

class Cat implements Animal 
{
	public void eat(Food food)
	{
		System.out.println("小猫吃"+food.getname());
	}
}

class Dog implements Animal 
{
	public void eat(Food food)
	{
		System.out.println("小狗啃"+food.getname());
	}
}




/*******************食物************************/
abstract class Food 
{
	protected String name;
	public String getname()
	{
		return this.name;
	}

	public void setname(String name)
	{
		this.name=name;
	}
}

class fish extends Food 
{
	public fish(String name)
	{
		this.name=name;
	}
}

class Bone extends Food 
{
	public Bone(String name)
	{
		this.name=name;
	}
}

class feeder
{
	public void feed(Animal animal,Food f)
	{
		animal.eat(f);
	}
}

public class test9
{
	public static void main(String[] args) {
		feeder feeder=new feeder();
		Animal animal=new Dog();
		Food food=new Bone("骨头");
		feeder.feed(animal,food);
		animal=new Cat();
		food=new fish("鱼");
		feeder.feed(animal,food);
	}
}