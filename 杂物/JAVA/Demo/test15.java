import java.util.Arrays;

public class test15 
{
	public static void main(String[] args) {
		int[] a1=new int[]{3,4,5,6};
		int[] a2=new int[]{3,4,5,6};
		System.out.println("a1a2是否相等:"+Arrays.equals(a1,a2));
		int[] b=Arrays.copyOf(a1,3);
		for(int m:b)
		{
			System.out.println(m);
		}
		System.out.println("a1与b是否相等:"+Arrays.equals(a1,b));
		System.out.println("b的数组元素为:"+Arrays.toString(b));

		//Arrays.fill(b,2,4,1);
		System.out.println("b的数组元素为:"+Arrays.toString(b));

		Arrays.sort(b);
		System.out.println("b的数组元素为:"+Arrays.toString(b));

	}
}