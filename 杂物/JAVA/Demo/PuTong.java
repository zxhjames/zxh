public class PuTong 
{
	public PuTong()
	{
		System.out.println("默认构造方法!");
	}


	{
		System.out.println("非静态代码块!");
	}

	static 
	{
		System.out.println("静态代码块!");
	}

	public static void test()
	{
		System.out.println("普通方法的代码块！");
	}


	public static void main(String[] args) {
		PuTong p=new PuTong();
		p.test();
	}

}