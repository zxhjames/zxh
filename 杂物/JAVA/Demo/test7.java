interface IFood 
{
	int max=2;
	String name="aa";
	void eat(String name);
	void fly();
}

class IFoodImpl implements IFood 
{
	String name;
	public void eat(String name)
	{
		System.out.println(name+"food!");
	}
	public void fly()
	{
		System.out.println("I can fly!");
	}
}

public class test7
{
	public static void main(String[] args) {
		IFood food=new IFoodImpl();
		food.eat("b");
		food.fly();
	}
}