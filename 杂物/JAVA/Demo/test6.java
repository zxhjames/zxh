abstract class A 
{
	public abstract void eat();
}

 class B extends A 
{
	public  void eat()
	{
		System.out.println("A is very cool!");
	}
}

public class test6 
{
	public static void main(String[] args) {
		A a=new B();
		a.eat();
	}
}