import java.util.*;
public class test14
{
	public static void print(Collection c)
	{
		Iterator it=c.iterator();
		while(it.hasNext())
		{
			System.out.println(it.next());
		}
	}

	public static void main(String[] args) {
		Map<String,String> ha=new HashMap<String,String>();
		ha.put("one","lisi");
		ha.put("two","zhangsan");
		ha.put("three","james");
		System.out.println("通过键值对应的值");
		System.out.println(ha.get("two"));
		System.out.println(ha.get("three"));
		System.out.println("获取键值");
		Set se=ha.keySet();
		print(se);
		System.out.print("获取元素值");
		Collection co=ha.values();
		print(co);
		System.out.println("获取键值和元素值");
		Set entry =ha.entrySet();
		print(entry);
		System.out.println(ha.size());
	}
}