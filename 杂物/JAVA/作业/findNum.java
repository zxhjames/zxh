

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/*
 * 该程序的功能是，接受控制台输入一个浮点数，进行判断，如果是浮点数，
 * 那么分别输出干浮点数的整数部分和小数部分，否则就会报输入浮点数的格式有误
 */

/*
 * 通过IO类从命令行读入一个字符串，如果输入的字符串为非数字字符串，
 * 则提示异常，要求重新输入。如果输入的是一个没有小数点的数字字符串（
 * 即整数类型），则将其转换为double型浮点数，最后完成将浮点数的小数位
 * 和整数位分别输出。
 */
public class findNum
{
	public static void main(String[] args) {
		String s;
		double d;
		int i;
		boolean b=false;
		
		do
		{
			try {
			System.out.println("请输入一个浮点数:");
			BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
			s=br.readLine();//以字符串方式读入
			i=s.indexOf('.');//找到小数点的位置
			d=Double.parseDouble(s);//将字符串转换为浮点数
			System.out.println(d+"整数部分为:"+(long)d);
			if(i==-1)//若没有小数点，则截取小数点后的字符串合成浮点数
			{
				System.out.println(d+"小数部分为:0.0");
			}
			else
			{
				System.out.println(d+"小数部分为:"+Double.parseDouble(((s.charAt(0)=='-')?"-":"")+"0."+s.substring(i+1,s.length())));
				b=false;
			}
			} catch (NumberFormatException nfe) {
				// TODO Auto-generated catch block
				System.out.println("输入浮点数的格式有误\n");
				b=true;
			}
			catch(IOException e)
			{
				b=false;
			}
			
		}while(b);
	}

}
