public class Rectangle 
{
	int height;
	int width;
	Rectangle(int h,int w)
	{
		this.height=h;
		this.width=w;
	}
	int getArea()
	{
		return this.height*this.width;
	}
	int getPerimeter()
	{
		return 2*(this.height+this.width);
	}
	void draw()
	{
		//boolean flag=true;
		for(int j=0;j<this.height;j++)
		{
			for(int i=0;i<this.width;i++)
			{
				if(j==0 || j==this.height-1)
				{
					System.out.print("* ");
				}
				else
				{
					if(i==0 || i==this.width-1)
					{
						System.out.print("* ");
					}
					else
					System.out.print("  ");
				}
	
			}
			System.out.println();
		}
		

	}
	public static void main(String[] args)
	{
		Rectangle rec=new Rectangle(6,10);
		System.out.println(rec.getArea());
		System.out.println(rec.getPerimeter());
		rec.draw();	
	}

}