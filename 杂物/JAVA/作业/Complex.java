public class Complex 
{
	int realPart;
	int imaginPart;
	public Complex()
	{
		this.realPart=0;
		this.imaginPart=0;
	}
	public Complex(int r,int i)
	{
		this.realPart=r;
		this.imaginPart=i;
	}
	Complex complexAdd(Complex a)
	{
		this.realPart=this.realPart+a.realPart;
		this.imaginPart=this.imaginPart+a.imaginPart;
		return this;
	}
	public String toString()
	{
		return (this.imaginPart==0)?String.valueOf(this.realPart):(this.realPart==0)?
			String.valueOf(this.imaginPart+"i"):(this.imaginPart>0)?
			this.realPart+"+"+this.imaginPart+"i":this.realPart+"-"+(-this.imaginPart)+"i";
	}
	public static void main(String[] args)
	{
		Complex b=new Complex(0,-5);
		Complex a=new Complex(0,1);
		b.complexAdd(a);
		System.out.println(b.toString());
	}
}