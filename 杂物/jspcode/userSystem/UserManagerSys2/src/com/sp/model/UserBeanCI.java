package com.sp.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

//这是一个处理类，有些人把它当做bo,主要是封装对users表的各种操作
//增删改查
public class UserBeanCI
{
	
	private Statement sm=null;
	private ResultSet rs=null;
	private Connection ct=null;
	private int pageSize=3;//每页显示的数量
	private int rowCount=0;//该值从数据库查询
	private int pageCount=0;//该值通过pageSize和rowCount查询
	//关闭资源
	
	//删除用户
	public boolean delUserById(String id)
	{
		boolean b=false;
		try {
			ct=new ConnDB().getconn();
			sm=ct.createStatement();
			int a=sm.executeUpdate("delete from test where userid="+id);//a返回的是一个数值，代表该id删除了几次
			
			if(a==1)
			{
				b=true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	//	return b; 
		return b;
	}
	public void close()
	{
		//关闭资源
		try {
			if(rs!=null)
			{
				rs.close();
				rs=null;
			}
			else if(sm!=null)
			{
				sm.close();
				sm=null;
			}
			else if(ct!=null)
			{
				ct.close();
				ct=null;
			}
			
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	//验证用户是否存在
	public boolean checkUser(String u,String p)
	{
		boolean b=false;
		try {
			ct=new ConnDB().getconn();
			sm=ct.createStatement();
			rs=sm.executeQuery("select password from test where username='"+u+"'");
			if(rs.next())
			{
				//说明用户存在
				if(rs.getString(1).equals(p))
				{
					//一定合法
					b=true;
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			close();
		}
		return b;
	}
	
	//返回分页的总页数
	public int getPageCount()
	{
		try {
			//得到链接
			
			//4.显示表中有多少条记录
			 rs=sm.executeQuery("select count(*) from test");
			if(rs.next())//此处一定要rs.next()，不然是null
			{
				rowCount=rs.getInt(1);//取出行数
			}

			//计算pageCount,算法1
			if(rowCount%pageSize==0)
			{
				pageCount=rowCount/pageSize;
			}
			else
			{
				pageCount=rowCount/pageSize+1;
			}
			//计算结果pageCount
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			this.close();//关闭资源
		}
		return pageCount;
	}
	
	//得到用户需要显示的用户信息(分页),返回结果集，使用arraylist存储
	public ArrayList<UserBean> getUsersByPage(int pageNow)
	{
		
		ArrayList<UserBean> a1=new ArrayList<UserBean>();
		try {
			//3.创建statement
			ct=new ConnDB().getconn();
			sm=ct.createStatement();
			rs=sm.executeQuery("select * from test limit "+(pageNow-1)*pageSize+","+pageSize);
			
			//开始将rs封装到Arraylist
			while(rs.next())
			{
				UserBean ub=new UserBean();
				ub.setUsername(rs.getString(1));
				ub.setPassword(rs.getString(2));
				ub.setSex(rs.getString(3).charAt(0));
				ub.setHome(rs.getString(4));
				a1.add(ub);//将对象存入ArrayList
			}
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			this.close();
		}
		return a1;
	}
	
	//返回用户的个人信息
	public ResultSet getuserinfo(String userinfo)
	{
		UserBean user=new UserBean();
		try {
			//3.创建statement
			ct=new ConnDB().getconn();
			sm=ct.createStatement();
			rs=sm.executeQuery("select userinfo from test where username='"+userinfo+"'");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			this.close();
		}
		return rs;
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
