package com.sp.controller;
//这是一个控制器，主要完成对用户身份的验证，
//控制器本身是不会完成业务逻辑的，他主要是可以去调用model下面的类的功能
//完成对数据的处理
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sp.model.Tools;
import com.sp.model.UserBeanCI;

/**
 * Servlet implementation class LoginCIServlet
 */
public class LoginCIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public LoginCIServlet() {
        super();
      
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//得到用户名和密码
		//request.setCharacterEncoding("GBK");//修改收到的字符集
		String u=request.getParameter("username");
		System.out.println(u);
		u=Tools.getNevString(u);//转换字符类型
		String p=request.getParameter("passwd");
		System.out.println(u+" "+p);
		//开始使用模型，完成对用户的验证(UserbeanCI)
		//1.创建一个UserbeanCI对象
		UserBeanCI ubc=new UserBeanCI();
		//2.调用方法
		if(ubc.checkUser(u, p))
		{
			//合法
			//1.这种方法跳转效率不高
			//response.sendRedirect("wel.jsp");
			//2.因为sendRedirect效率不高，所以软件公司常使用
			//转发方法，这种方法效率很高，同时request的对象还可以在下一页面使用
			System.out.println("这是使用的controller完成验证");
			//在跳转到wel.jsp页面时，就要把要显示的数据，给wel.jsp
			//准备好
			ArrayList aL=ubc.getUsersByPage(1);
			int pageCount=ubc.getPageCount();
			//将aL，pageCount,pageNow放到request中
			request.setAttribute("result", aL);
			request.setAttribute("pageCount", pageCount+"");
			request.setAttribute("pageNow", "1");
			
			//将用户名放入Session，以备后用
			request.getSession().setAttribute("myName",u);
			
			
			
			request.getRequestDispatcher("main.jsp").forward(request, response);
		}
		else
		{
			//不合法
			//response.sendRedirect("login.jsp");
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}
	}

	
	protected void  doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//合二为一
		this.doGet(request, response);
	}

}
