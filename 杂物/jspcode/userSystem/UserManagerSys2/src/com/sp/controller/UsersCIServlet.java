package com.sp.controller;
//这个控制器将处理用户的分页显示，还有用户的删除修改添加
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sp.model.UserBean;
import com.sp.model.UserBeanCI;

/**
 * Servlet implementation class UsersCIServlet
 */
public class UsersCIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsersCIServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String flag=request.getParameter("flag");
		 if(flag.equals("fengye")){
					//得到用户希望显示的pageNow
					String s_pageNow=request.getParameter("pageNow");
					try {
						System.out.println("分页用UserCIServlet");
						int pageNow=Integer.parseInt(s_pageNow);
						//调用userbeanCI
						UserBeanCI ubc=new UserBeanCI();
						ArrayList<UserBean> aL=ubc.getUsersByPage(pageNow);
						int pageCount=ubc.getPageCount();
						//将aL，pageCount放到request里
						request.setAttribute("result", aL);
						request.setAttribute("pageCount", pageCount+"");
						//重新跳转回到wel.jsp
						request.setAttribute("pageNow", pageNow+"");
						request.getRequestDispatcher("wel.jsp").forward(request, response);
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
		 }
		 
		 else if(flag.equals("delUser"))
		 {
			 //完成删除用户
			 //1.得到删除用户的id
			 String userid=request.getParameter("userid");
			 UserBeanCI ubc=new UserBeanCI();
			 if(ubc.delUserById(userid))
			 {
				 //删除成功!
				 System.out.println("删除成功!");
			 }
			 else
			 {
				 //删除失败!
				 System.out.println("删除失败!");
			 }
		 }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
