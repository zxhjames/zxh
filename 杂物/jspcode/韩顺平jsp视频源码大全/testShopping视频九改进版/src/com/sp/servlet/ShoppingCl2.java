package com.sp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sp.model.*;
public class ShoppingCl2 extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		//处理乱码
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		//检测用户是否登录过，没有则进入登陆界面，否则直接跳转
		
		//1.看session是否有用户登录的信息
		UserBean ub=(UserBean)request.getSession().getAttribute("userInfo");
		if(ub==null){
			//用户没有登录
			//跳转到shopping2.jsp
			request.getRequestDispatcher("shopping2.jsp").forward(request, response);
		}else{
			MyCartBO mcb=(MyCartBO)request.getSession().getAttribute("mycart");
			ArrayList al=mcb.showMyCart();
			//把al放入request
			request.setAttribute("mycartInfo", al);
			request.getRequestDispatcher("shopping3.jsp").forward(request, response);
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

}
