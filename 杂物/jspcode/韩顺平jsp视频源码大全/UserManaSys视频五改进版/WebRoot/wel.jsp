<%@ page language="java" import="java.util.*,java.sql.*,com.sp.model.*" pageEncoding="gb2312"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'wel.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
 	<script type="text/javascript">
 	<!--
 	    function abc(){
 		return window.confirm("你真的要删除吗？");
 		}
 	 -->
 	</script>
  </head> 
  <body bgcolor="#CDCFFF">
  <%
      //防止用户非法登陆
      String u=(String)session.getAttribute("myName");
      //如果用户没有登录
      if(u==null){
      response.sendRedirect("login.jsp?err=1");
      return ;
      }
   %>
  <center>  
  登陆成功！恭喜你！<%=(String)session.getAttribute("myName")%><br>
  <a href="login.jsp">重新登陆</a>
  &nbsp<a href="main.jsp">返回主界面</a>
  <h1>用户信息</h1>
  <%
  
    //调用UserBeanCl中的方法（创建一个UserBeanCl实例，然后调用它的某个方法），进行分页
    //UserBeanCl ubc=new UserBeanCl();
    //ArrayList al=ubc.getUsersByPage(pageNow);
    //要显示的用后信息从request
    ArrayList al=(ArrayList)request.getAttribute("result");
   %>
   <table border="1">
   <tr bgcolor="pink"><td>用户ID</td><td>用户名</td><td>用户密码</td><td>用户级别</td>
   <td>修改用户</td><td>删除用户</td></tr>   
   <%
     //定义颜色数组
     String []color={"silver","pink"};
     for(int i=0;i<al.size();i++){
     UserBean ub=(UserBean)al.get(i);
   %>    
   <tr bgcolor="<%=color[i%2] %>">
   <td ><%=ub.getUserId()%></td>
   <td><%=ub.getUsername()%></td>
   <td><%= ub.getPasswd()%></td>
   <td><%= ub.getGrade()%></td>
   <td><a href="updataUser.jsp?userName=<%=ub.getUsername()%>
   &passwd=<%=ub.getPasswd()%>&grade=<%=ub.getGrade()%>&userId=<%=ub.getUserId()%>">修改用户</a></td>
   <td><a onclick="return abc();" href="UsersClServlet?flag=delUser&userId=<%=ub.getUserId()%>">删除用户</a></td>
   </tr> 
   <% }%> 
  </table><br>
  <%
  int pageNow=Integer.parseInt((String )request.getAttribute("pageNow"));
  if(pageNow!=1){
     out.println("<a href=UsersClServlet?flag=fenye&pageNow="+(pageNow-1)+">上一页</a>"); 
  }
  String s_pageCount=(String)request.getAttribute("pageCount");
  int pageCount=Integer.parseInt(s_pageCount);
  
  for(int i=1;i<=pageCount;i++){
   out.println("<a href=UsersClServlet?flag=fenye&pageNow="+i+">["+i+"]</a>");
  }
  if(pageNow!=pageCount){
     out.println("<a href=UsersClServlet?flag=fenye&pageNow="+(pageNow+1)+">下一页</a>"); 
  }
  %>
  </center>
  </body>
</html>
