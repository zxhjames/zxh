<%@ page language="java" import="java.util.*,java.sql.*,com.sp.model.*" pageEncoding="gb2312"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'wel.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head> 
  <body>
  <%
      //防止用户非法登陆
      String u=(String)session.getAttribute("myName");
      //如果用户没有登录
      if(u==null){
      response.sendRedirect("login.jsp?err=1");
      return ;
      }
   %>
  <center>  
  登陆成功！恭喜你！<%=(String)session.getAttribute("myName")%><br>
  <a href="login.jsp">重新登陆</a>
  <h1>用户信息</h1>
  <%
  
    //调用UserBeanCl中的方法（创建一个UserBeanCl实例，然后调用它的某个方法），进行分页
    //UserBeanCl ubc=new UserBeanCl();
    //ArrayList al=ubc.getUsersByPage(pageNow);
    //要显示的用后信息从request
    ArrayList al=(ArrayList)request.getAttribute("result");
   %>
   <table border="1">
   <tr><td>用户ID</td><td>用户名</td><td>用户密码</td><td>用户级别</td></tr>   
   <%
     for(int i=0;i<al.size();i++){
     UserBean ub=(UserBean)al.get(i);
   %>    
   <tr>
   <td><%=ub.getUserId()%></td>
   <td><%=ub.getUsername()%></td>
   <td><%= ub.getPasswd()%></td>
   <td><%= ub.getGrade()%></td>
   </tr> 
   <% }%> 
  </table><br>
  <%
  int pageNow=Integer.parseInt((String )request.getAttribute("pageNow"));
  if(pageNow!=1){
     out.println("<a href=UsersClServlet?pageNow="+(pageNow-1)+">上一页</a>"); 
  }
  String s_pageCount=(String)request.getAttribute("pageCount");
  int pageCount=Integer.parseInt(s_pageCount);
  
  for(int i=1;i<=pageCount;i++){
   out.println("<a href=UsersClServlet?pageNow="+i+">["+i+"]</a>");
  }
  if(pageNow!=pageCount){
     out.println("<a href=UsersClServlet?pageNow="+(pageNow+1)+">下一页</a>"); 
  }
  %>
  </center>
  </body>
</html>
