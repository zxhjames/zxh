CREATE TABLE [orderDetail] (
	[ordesIid] [bigint] NULL ,
	[goodsId] [bigint] NULL ,
	[nums] [int] NOT NULL ,
	CONSTRAINT [fk_order_id] FOREIGN KEY 
	(
		[ordesIid]
	) REFERENCES [orders] (
		[ordersId]
	),
	CONSTRAINT [fk_shangpin_id] FOREIGN KEY 
	(
		[goodsId]
	) REFERENCES [goods] (
		[goodsId]
	)
) ON [PRIMARY]
GO


