package com.sp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sp.model.*;
public class UsersClServlet extends HttpServlet {

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//获得标志位
		String flag=request.getParameter("flag");
		if(flag.equals("fenye"))
		{
			String s_pageNow=request.getParameter("pageNow");
			try{
				int pageNow=Integer.parseInt(s_pageNow);
				UserBeanCl ubc=new UserBeanCl();
				//在跳转到wel。jsp
				ArrayList al=ubc.getUsersByPage(pageNow);
				int pageCount=ubc.getPageCount();
				request.setAttribute("result", al);
				request.setAttribute("pageCount", pageCount+"");
				request.setAttribute("pageNow",pageNow+"");
				//重新跳转wel
				request.getRequestDispatcher("wel.jsp").forward(request,response);
			 
			}catch(Exception e){
			e.printStackTrace();
			}
		}else if(flag.equals("delUser")){
			String userId=request.getParameter("userId");
			UserBeanCl ubc=new UserBeanCl();
			if(ubc.delUserById(userId)){
				request.getRequestDispatcher("suc.jsp").forward(request,response);//删除成功
			}else{
				request.getRequestDispatcher("err.jsp").forward(request,response);//删除失败
			}			
		}else if(flag.equals("addUser")){
			//完成添加用户
			String name=request.getParameter("userName");
			String passwd=request.getParameter("passwd");
			String grade=request.getParameter("grade");
			UserBeanCl ubc=new UserBeanCl();
			if(ubc.addUser(name, passwd, grade)){
				request.getRequestDispatcher("suc.jsp").forward(request,response);//添加成功
			}else{
				request.getRequestDispatcher("err.jsp").forward(request,response);//添加失败
			}			
		}else if(flag.equals("updataUser")){
			//完成修改用户
			String id=request.getParameter("userId");
			String name=request.getParameter("userName");
			String passwd=request.getParameter("passwd");
			String grade=request.getParameter("grade");
			UserBeanCl ubc=new UserBeanCl();
			if(ubc.updadaUser(id,name, passwd, grade)){
				request.getRequestDispatcher("suc.jsp").forward(request,response);//修改成功
			}else{
				request.getRequestDispatcher("err.jsp").forward(request,response);//修改失败
			}			
		}
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.doGet(request, response);
	}

}
