//这是一个处理类，主要封装对users表的各种操作（增，删，修）
package com.sp.model;

import java.sql.*;
import java.util.*;

public class UserBeanCl {
	private Statement sm = null;
	private ResultSet rs = null;
	private Connection ct = null;
	private int pageCount = 0;
	private int rowCount = 0;
	private int pageSize = 3;
	
    public boolean updadaUser(String id,String name,String passwd,String grade){   //修改用户
		
		boolean b=false;
		try{
			ct=new ConnDB().getConn();
			sm=ct.createStatement();
			int a=sm.executeUpdate("update users SET username='"+name+"',passwd='"+passwd+"',grade='"+grade+"' where userId='"+id+"'");
			//a是修改的记录数
			if(a==1){
				b=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return b;
	}
	
/**
 * @author izq
 * @param name
 * @param passwd
 * @param grade
 * @return
 */
	public boolean addUser(String name,String passwd,String grade){   //添加用户
		
		boolean b=false;
		try{
			ct=new ConnDB().getConn();
			sm=ct.createStatement();
			int a=sm.executeUpdate("insert into users values('"+name+"','"+passwd+"','"+grade+"')");
			//a是添加的记录数
			if(a==1){
				b=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return b;
	}
	
	public boolean delUserById(String id){   //删除用户
		boolean b=false;
		try{
			ct=new ConnDB().getConn();
			sm=ct.createStatement();
			int a=sm.executeUpdate("delete from users where userId='"+id+"'");
			//a是删除的记录数
			if(a==1){
				b=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return b;
	}
	
	public int getPageCount() {   //返回分页的总数
		try {
			ct = new ConnDB().getConn();
			sm=ct.createStatement();
			rs = sm.executeQuery("select count(*) from users");
			if (rs.next()) {
				rowCount = rs.getInt(1);
			}
			if (rowCount % pageSize == 0) {
				pageCount = rowCount / pageSize;
			} else {
				pageCount = rowCount / pageSize + 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close();
		}
		return pageCount;
	}

	public void close() { // 关闭各种打开的资源
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (sm != null) {
				sm.close();
				sm = null;
			}
			if (ct != null) {
				ct.close();
				ct = null;
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常，以便修改
		}
	}

	public ArrayList getUsersByPage(int pageNow) {
		ArrayList al = new ArrayList();
		try {
			ct = new ConnDB().getConn();
			sm = ct.createStatement();
			rs = sm.executeQuery("select top " + pageSize
					+ " * from users where userId not in (select top "
					+ pageSize * (pageNow - 1) + " userId from users) ");
			while (rs.next()) {
				UserBean ub = new UserBean();
				ub.setUserId(rs.getInt(1));
				ub.setUsername(rs.getString(2));
				ub.setPasswd(rs.getString(3));
				ub.setGrade(rs.getInt(4));
				al.add(ub); // 将al放到arrayList中
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.close();
		}
		return al;
	}

	public boolean checkUser(String u, String p) {
		boolean b = false;
		if(u!=null&&p!=null)
		{
			try {
				ct = new ConnDB().getConn();// 获取ConnDB中的方法，操作数据库
				sm = ct.createStatement();
				rs = sm.executeQuery("select passwd from users where username='"
						+ u + "'");
				if (rs.next()) {
					if (rs.getString(1).equals(p)) {
						b = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally { // 关闭各种打开的资源，释放内存
				this.close();
			}
		}
		
		return b;
	}

}
