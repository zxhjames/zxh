package com.sp.model;
import java.sql.*;

public class GoodsBeanBO {
	private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection ct = null;
	
	public void close() { // 关闭各种打开的资源
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (ct != null) {
				ct.close();
				ct = null;
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常，以便修改
		}
	}
	//根据一个货物id，得到货物具体信息函数
	public GoodsBean getGoodsBean(String id){		
		
		GoodsBean gb=new GoodsBean();
		try{
			ct=new ConnDB().getConn();
			ps=ct.prepareStatement("select * from goods where goodsId=?");
			ps.setString(1,id);
			rs=ps.executeQuery();
			
			if(rs.next()){
				gb.setGoodsId(rs.getInt(1));
				gb.setGoodsName(rs.getString(2));
				gb.setGoodsIntro(rs.getString(3));
				gb.setGoodsPrice(rs.getFloat(4));
				gb.setGoodsNum(rs.getInt(5));
				gb.setPublisher(rs.getString(6));
				gb.setPhoto(rs.getString(7));
				gb.setType(rs.getString(8));
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			close();
		}
		return gb;
	}
	
}
