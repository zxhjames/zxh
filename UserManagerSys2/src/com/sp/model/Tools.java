package com.sp.model;

import java.io.UnsupportedEncodingException;

//当数据库用户名是中文时，可能会出现乱码现象
public class Tools
{
	public static String getNevString(String input)
	{
		String result="";
		try {
			result=new String(input.getBytes("iso-8859-1"),"GBK");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
}
