package com.sp.model;
//得到数据库的连接
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnDB {
	private Connection ct=null;
	public Connection getconn()
	{
		try
		{
			//1.加载驱动
			Class.forName("com.mysql.jdbc.Driver");
			//2.得到连接
			String url="jdbc:mysql://localhost:3306/mysql?useSSL=false&characterEncoding=gbk";
			String user="root";
			String password="123456";
			ct = DriverManager.getConnection(url, user, password);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return ct;
	}
}
