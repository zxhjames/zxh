package com.sp.controller;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sp.model.UserBeanCI;

/**
 * Servlet implementation class UserMessageCI
 */
public class UserMessageCI extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserMessageCI() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("’‚ «”√UserMessageCI");
		try {
		String userMessinfo=request.getParameter("userMessinfo");
		UserBeanCI umi=new UserBeanCI();
		ResultSet usermess=umi.getuserinfo(userMessinfo);
		request.setAttribute("usermess",usermess);
		request.getRequestDispatcher("userinfo.jsp").forward(request,response);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

}
