<%@page import="com.sp.controller.UserMessageCI"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.sql.*,com.sp.model.*,java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
//防止用户非法登录
String u=(String)session.getAttribute("myName");
//如果用户登录
if(u==null)
{
	//return..返回登录页面(request,out,response,session)
	response.sendRedirect("login.jsp?err=1");
	return;
}
%>


登陆成功!<%=u%><br>
<a href="login.jsp">返回重新登录</a>
<h1 align="center">用户信息表</h1>
<%
	int pageNow=Integer.parseInt((String)request.getAttribute("pageNow"));
	//调用UserbeanCI的方法，创建一个实例，调用他的某个方法
//	UserBeanCI ubc=new UserBeanCI();
	//ArrayList a1=ubc.getUsersByPage(pageNow);
	request.setCharacterEncoding("UTF-8");
	ArrayList a1=(ArrayList)request.getAttribute("result");
	
%>

<table border="1" align="center" bordercolor="green">
<tr>
	<td>用户姓名</td>
	<td>用户密码</td>
	<td>用户性别</td>
	<td>用户国籍</td>
</tr>
<%
for(int i=0;i<a1.size();i++)
{
	UserBean ub=(UserBean)a1.get(i);
	String sname=ub.getUsername();
	%>

	<tr><td><a href="userinfo.jsp?username='"+<%=sname%>+"'"><%=sname%></a></td><td><%=ub.getPassword()%></td><td><%=ub.getSex() %></td><td><%=ub.getHome() %></td></tr>	
	
	<%
}%>

</table>
<p align="center">
<%
		//上一页
		//从request中取出pageNow
		
		if(pageNow!=1)
		{
		out.println("<a href=UsersCIServlet?pageNow="+(pageNow-1)+">上一页</a>");
		}
		
		//得到pageCount
		String s_pageCount=(String)request.getAttribute("pageCount");
		int pageCount=Integer.parseInt(s_pageCount);
		//显示超链接
		for(int i=1;i<=pageCount;i++)
		{
			out.println("<a href=UsersCIServlet?pageNow="+i+">["+i+"]</a>");
		}
		//下一页
		if(pageNow!=pageCount)
		{
		
		out.println("<a href=UsersCIServlet?pageNow="+(pageNow+1)+">下一页</a>");
		}
		//思考..如果数据很多，如何考虑分页效率..
		%>
</p>

</body>
</html>