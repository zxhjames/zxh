package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Pojo.UserImpl;
import redis.clients.jedis.Jedis;

/**
 * Servlet implementation class RegistService
 */
@WebServlet("/RegistServlet")
public class RegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		username = URLDecoder.decode(username, "UTF-8");
		//开始插入用户
		int ans = -1;
		Jedis rd = new Jedis("127.0.0.1",6379);
		//如果不存在用户，就插入用户名和密码
		if(!rd.exists(username)) {
			rd.set(username, password);
			ans = 1;
			System.out.println(username+" 注册成功");
		}else {
			System.out.println(username+" 已经存在");
		}
		PrintWriter pr = response.getWriter();
		pr.print(ans);
		
		
		
//		try {
//			//首先去数据库查找
//			ans = new UserImpl().select(username);
//			if(ans == -1) {
//				//如果找不到，就直接插入数据
//				UserImpl u = new UserImpl();
//				ans = u.insert(username, password);
//			}else if(ans == 1) {
//				ans = -1;
//			}
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
