package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Pojo.UserDao;
import Pojo.UserImpl;
import redis.clients.jedis.Jedis;

/**
 * Servlet implementation class LoginService
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		username = URLDecoder.decode(username, "UTF-8");
		//连接redis
		Jedis rd = new Jedis("127.0.0.1",6379);
		int ans = -1;
		//如果存在用户
		if(!rd.exists(username)) {
			//用户不存在
			System.out.println(username+" 不存在");
			ans = -1;
		}else if(rd.exists(username)){
			if(rd.get(username).equals(password)) {
				//用户存在，密码也对
				ans = 1;
				System.out.println(username+" 登录成功");
			}else if(!rd.get(username).equals(password)) {
				//用户存在，密码不对
				System.out.println(username+" 密码错误");
				ans = 0;
			}
		}

//使用mysql语句方法
//		try {
//			ans = new UserImpl().select(username);
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//返回字符串
		PrintWriter pr = response.getWriter();
		pr.print(ans);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
