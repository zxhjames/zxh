package Pojo;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtil {
	private static String driver;
	private static String url;
	private static String user;
	private static String password;
	static{
		InputStream in=DBUtil.class.getClassLoader().getResourceAsStream("db.properties");
		//System.out.println(in);
		Properties props=new Properties(); //建立一个读属性文件的对象
		try {
			props.load(in);//加载属性文件（所谓属性文件，就是一个文本）文件，每一行都是由属性名=属性值构成
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		driver=props.getProperty("driver");
		url=props.getProperty("url");
		user=props.getProperty("user");
		password=props.getProperty("password");
	}
	
	//返回一个数据库连接
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		Class.forName(driver);
		Connection conn=DriverManager.getConnection(url, user, password);
		return conn;
	}

	//关闭一个数据库连接
	public static void close(Connection conn){
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}