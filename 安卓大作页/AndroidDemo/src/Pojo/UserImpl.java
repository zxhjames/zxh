package Pojo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserImpl {
	private Connection conn;
	private Statement st;
	private PreparedStatement ps;
	public ResultSet rs;
	
	/*
	 * 注册功能，插入用户
	 */
	public int insert(String username,String password) throws ClassNotFoundException, SQLException {
		conn = DBUtil.getConnection();
		String sql = "insert into ad(username,password) values(?,?)";
		ps = conn.prepareStatement(sql);
		ps.setString(1, username);
		ps.setString(2, password);
		int ans = ps.executeUpdate();
		System.out.println("inserting..."+username);
		this.CLOSE();
		if(ans==1) {
			return 1;
		}
		return -1;
		
	}
	/*
	 * 根据用户名和密码查询用户
	 */
	public int select(String username) throws ClassNotFoundException, SQLException {
		conn = DBUtil.getConnection();
		int ans = -1;
		String sql = "select username,password from ad where username=?";
		ps = conn.prepareStatement(sql);
		ps.setString(1, username);
//		ps.setString(2, password);
		rs = ps.executeQuery();
		System.out.println("selecting..."+username);
		if(rs.next()) {
			ans = 1;//如果找到返回1
		}
		this.CLOSE();
		return ans;
	}
	

	public void CLOSE() {
		if(rs!=null) {
			rs=null;
		}
		else if(st!=null) {
			rs = null;
		}else if(ps!=null) {
			ps = null;
		}
		else if(conn!=null) {
			conn=null;
		}
	}
}
