package com.example.james.myserver;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.net.MalformedURLException;
import java.net.URL;


public class MainActivity extends AppCompatActivity {
    EditText username;//用于用户名输入
    EditText password;//密码输入
    Button Login;//登录按钮
    Button Register;//注册按钮
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionbar = getSupportActionBar();
        if(actionbar !=null){
            actionbar.hide();
        }
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        Login = (Button)findViewById(R.id.login);
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Username = username.getText().toString();
                String Password = password.getText().toString();
                //判断用户名和密码是否为空
                if(Username.length()==0 || Password.length()==0){
                    Toast.makeText(getApplicationContext(),"用户名密码不能为空!",Toast.LENGTH_SHORT).show();
                    //如果为空，直接返回
                    return;
                }
                //发送http请求
                String url ="http://380a9dd7.nat123.cc/AndroidDemo/LoginServlet?username="+Username+"&password="+Password;
                new LoginTask().execute(url);

            }
        });



        Register = (Button)findViewById(R.id.register);
        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Username = username.getText().toString();
                String Password = password.getText().toString();
                if(Username.length()==0 || Password.length()==0){
                    Toast.makeText(getApplicationContext(),"用户名密码不能为空!",Toast.LENGTH_SHORT).show();
                    return;
                }
                String url ="http://380a9dd7.nat123.cc/AndroidDemo/RegistServlet?username="+Username+"&password="+Password;
                new RegistTask().execute(url);
            }
        });




    }


    //异步发送post请求
    class  LoginTask extends AsyncTask<String,Integer,String> {
        @Override
        protected String doInBackground(String... params) {
            String par  = params[0];
            URL url = null;
            try {
                url = new URL(par);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            String result = HttpUtil.doPost(url);
            return result;
        }
        //第四步
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // 1..登录成功
            // 0..密码错误
            // -1..没有注册
            Toast.makeText(getApplicationContext(),result.equals("1")==true?"登陆成功":result.equals("0")==true?"密码错误":"用户名不存在,请注册",Toast.LENGTH_SHORT).show();
            if(result.equals("1")){
                //跳转到第二个页面
                Intent intent = new Intent(MainActivity.this,Main2Activity.class);
                startActivity(intent);

            }
        }
    }



    class  RegistTask extends AsyncTask<String,Integer,String> {
        @Override
        protected String doInBackground(String... params) {
            String par  = params[0];
            URL url = null;
            try {
                url = new URL(par);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            String result = HttpUtil.doPost(url);
            return result;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //保存数据
            Toast.makeText(getApplicationContext(),result.equals("1")==true?"注册OK":"注册名已经存在,请换一个",Toast.LENGTH_SHORT).show();
        }
    }
}
