package com.example.james.myserver;

public class Friend {
    private String name;
    private int imageid;
    String phone;
    public Friend(String name, int imageid, String phone) {
        this.name = name;
        this.imageid = imageid;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }
}
