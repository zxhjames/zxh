package com.example.james.myserver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Entrance extends AppCompatActivity {
    /*
    ListView这个控件用来显示大量数据
     */
//    private String[] data = {"apple","Banana","Orange","Watermelon",
//    "pear","Grape","Pineapple","Strawberry","Cherry","Mange","Apple",
//    "Banana","Orange","Watermelon","Pear","Grape","Pinearpple","Strawberry","Mango"};
    /*
    下面使用list用来存储
     */

    private List<Friend> friendList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrance);
        initFruit();
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
//                MainActivity.this,android.R.layout.simple_list_item_1,data);
        FruitAdapter adapter = new FruitAdapter(Entrance.this
                ,R.layout.friend_item, friendList);
        ListView listView = (ListView) findViewById(R.id.list_view);
        listView.setAdapter(adapter);
    }

    //初始化苹果数据
    private void initFruit(){
        for (int i = 0;i<2;i++){
            Friend apple = new Friend("刘亦菲",R.mipmap.lyf,"13202562135");
            friendList.add(apple);
            Friend banana = new Friend("obama",R.mipmap.obama,"13206412345");
            friendList.add(banana);
            Friend orange = new Friend("kobe",R.mipmap.kb,"15625632152");
            friendList.add(orange);
            Friend grape = new Friend("李彦宏",R.mipmap.lyh,"13274113445");
            friendList.add(grape);
            Friend pear = new Friend("马云",R.mipmap.my,"13274113445");
            friendList.add(pear);
            Friend strawnerry = new Friend("马化腾",R.mipmap.mht,"13274113445");
            friendList.add(strawnerry);
        }
    }
}
