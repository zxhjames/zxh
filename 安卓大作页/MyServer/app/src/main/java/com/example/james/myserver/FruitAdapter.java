package com.example.james.myserver;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/*
适配器
 */
public class FruitAdapter extends ArrayAdapter<Friend> {
    private int resourceId;
    public FruitAdapter(Context context, int textViewResourceId, List<Friend> objects){
        super(context,textViewResourceId,objects);
        resourceId = textViewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        Friend friend = getItem(position);
        View view;
        if(convertView == null) {
            /*
            此方法可以提前对布局进行缓存，以便之后可以重用
             */
            view = LayoutInflater.from(getContext()).inflate(resourceId, parent, false
            );
        }else{
            view = convertView;
        }
        ImageView fruitImage = (ImageView)view.findViewById(R.id.fruit_image);
        TextView fruitName = (TextView) view.findViewById(R.id.fruit_name);
        fruitImage.setImageResource(friend.getImageid());
        fruitName.setText(friend.getName()+" "+ friend.getPhone());
        return view;
    }
}
