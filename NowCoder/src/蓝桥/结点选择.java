package 蓝桥;

import java.util.Scanner;

public class 结点选择 {
	static int[][] dp = new int[100002][2];
	static int[][] tree = new int[100002][300];
	
	static void createTree(int p1,int p2) {
		/**
		 * 建立领接表
		 */
		int i=0;
		while(tree[p1][i]!=0) {
			i++;
		}
		tree[p1][i] = p2;
		int j=0;
		while(tree[p2][j]!=0) {
			j++;
		}
		tree[p2][j] = p1;
	}
	
	static void dfs(int start,int root) {
		//dp[i][0]表示当不选择i号结点时在i的子树中所能得到的最大权值
		//dp[i][1]表示当选择i号结点时能在i与i的子树中所能获得的最大权值
		//状态转移方程为
		//对于叶子结点来说:dp[i][0] = 0  i为叶子结点时，已经没有子树了，所以权为0 
		//              dp[i][1] = k  这时i只能选择自己的权
		
		//对于非叶结点来说:
		//              dp[k][0] = max(dp[i][0],dp[i][1]), k为非叶结点，这时只能其叶子结点中的两种情况中的一种
		//				dp[k][1] = max(k,dp[i][0]) 这时只能选择自己的权值加上叶子结点不包括其本身的权值的情况
		int child = tree[start][0];
		for(int i=0;child!=0;i++) {
			child = tree[start][i];
			if(child!=root) {//防止出现环
				dfs(child,start);
				dp[start][1] += dp[child][0];
//				System.out.println(dp[s][1]+"--->"+dp[child][0]);
				dp[start][0] += Math.max(dp[child][0], dp[child][1]);
//				System.out.println(dp[s][0]+"--->"+dp[child][0]+"--->"+dp[child][1]);
			}
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		for(int i=0;i<n;i++) {
			dp[i+1][1] = in.nextInt();//假设刚开始各个结点都是独立的，即dp[i][0] = 0  dp[i][1] = 初始下表
		}
		for(int i=0;i<n-1;i++) {
			int p1 = in.nextInt();
			int p2 = in.nextInt();
			createTree(p1, p2);
		}
		dfs(1,0);
		System.out.println(Math.max(dp[1][1], dp[1][0]));
	}
}

