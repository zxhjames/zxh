package 蓝桥;
/*
 * AC 40% 
 */
import java.util.Scanner;
import java.util.Stack;

public class 前缀表达式 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		s=s.replace(" ", "");
		int len = s.length();
		Stack<String> S = new Stack<String>();
		for(int i=0;i<len;i++) {
			S.push((String) s.subSequence(i, i+1));
		}
		int a = 0;
		int b = 0;
		while(S.size()!=1) {
			a = Integer.parseInt(S.pop());
			b = Integer.parseInt(S.pop());
			if(S.peek().equals("+")) {
				S.pop();
				S.push(String.valueOf(a+b));
				continue;
			}else if(S.peek().equals("-")) {
				S.pop();
				S.push(String.valueOf(a-b));
				continue;
			}else if(S.peek().equals("*")) {
				S.pop();
				S.push(String.valueOf(a*b));
				continue;
			}else {
				S.pop();
				S.push(String.valueOf(a/b));
			}
		}
		System.out.println(S.peek());
	}
}
