package 蓝桥;

import java.util.Arrays;
import java.util.Scanner;
/*
 * AC 25%
 */
public class 航路和道路 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int T = in.nextInt();//城市数
		int R = in.nextInt();//公路数
		int P = in.nextInt();//航路数
		int S = in.nextInt();//起点数
		int xlen = R*2+P;
		int[][] res = new int[xlen][3];
		int t = 0;
		for(int i=0;i<R;i++) {
			int a = in.nextInt();
			int b = in.nextInt();
			int c = in.nextInt();
			res[t][0] = a;
			res[t][1] = b;
			res[t][2] = c;
			res[t+1][0] = b;
			res[t+1][1] = a;
			res[t+1][2] = c;
			t+=2;
		}
		for(int i=0;i<P;i++) {
			res[t][0] = in.nextInt();
			res[t][1] = in.nextInt();
			res[t][2] = in.nextInt();
			t++;
		}
//		for(int i=0;i<xlen;i++) {
//			for(int j=0;j<3;j++) {
//				System.out.print(res[i][j]+ 
//						" ");
//			}
//			System.out.println();
//		}
		getMini(res, T, S);
	}
	
	static void getMini(int[][] res,int T,int S) {
		int[] dp = new int[T+1];
		int len = res.length;
		for(int i=1;i<=T;i++) {
			dp[i] = 20000;//初始化一个极大值
		}
		dp[S] = 0;
		for(int i=0;i<T-1;i++) {
//			int[] dp_ = dp;
			for(int k = 0;k<len-1;k++) {
				dp[res[k][1]] = Math.min(dp[res[k][1]], dp[res[k][0]]+res[k][2]);
			}
//			if(Arrays.equals(dp, dp_)) {
//				break;
//			}
		}
		for(int i=1;i<=T;i++) {
			System.out.println(dp[i]>=10000?"NO PATH":dp[i]);
		}
	}
}
