package 蓝桥;

import java.util.Scanner;

public class N内素数 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		if(N==1) {
			return;
		}
		if(N==2) {
			System.out.println(2);
			return;
		}
		System.out.println(2);
		for(int i=1;i<=N;i++) {
			boolean flag = false;
			for(int j = i-1;j>1;--j) {
				flag = true;
				if(i%j == 0) {
					flag = false;
					break;
				}
			}
			if(flag) {
				System.out.println(i);
			}
		}
	}
}
