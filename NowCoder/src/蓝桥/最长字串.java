package 蓝桥;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
/*
 * AC 80%
 */
public class 最长字串 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int L = in.nextInt();
		String s = in.next();
		int len = s.length();
		Map<String, Integer> map = new HashMap<String, Integer>();
		for(int i=0;i<len-L;i++) {
			String ss = s.substring(i,i+L);
			if(!map.containsKey(ss)) {
				map.put(ss, 1);
			}else {
				map.put(ss, map.get(ss)+1);
			}
		}
		
		//寻找出最大的key
		List<Integer> l = new ArrayList<Integer>();
		for(int i:map.values()) {
			l.add(i);
		}
		int t = Collections.max(l);
		for(Map.Entry<String, Integer> entry:map.entrySet()) {
			if(t == entry.getValue()) {
				System.out.println(entry.getKey());
				return;
			}
		}
	}
}
