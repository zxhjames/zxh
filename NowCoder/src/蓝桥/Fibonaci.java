package 蓝桥;
import java.util.Scanner;

public class Fibonaci {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		System.out.println(Fibo(n));
	}
	
	static long Fibo(int n) {
		if(n<=2) {
			return 1;
		}
			long cur = 0;
			long prev1 = 1;
			long prev2 = 1;
			int p = 3;
			long temp = 0;
			while(p<=n) {
				cur = prev1+prev2;
				cur%=10007;
				temp = prev1;
				prev1 = cur;
				prev2 = temp;
				p++;
			}
		
		return cur%10007;
	}
}
