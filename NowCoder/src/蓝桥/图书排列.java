package 蓝桥;
public class 图书排列 {
    static int res=0;
    public static boolean check(int a[])
    {
        int l=a.length;
        for(int i=0;i<l-1;i++)
        {
            if(Math.abs(a[i]-a[i+1])==1)
            {
                return false;
            }
        }
        return true;
    }
    public static void swap(int a[],int x,int y)
    {
        int temp =a[x];
        a[x]=a[y];
        a[y]=temp;
    }
    public static void sort(int a[],int begin,int end)
    {
        if(begin==end)
        {
            if(check(a))
            res++;
        }
 
        for(int i=begin;i<=end;i++)
        {
            swap(a,begin,i);
            sort(a,begin+1,end);
            swap(a,begin,i);
        }
    }
    public static void main(String[] args) {
 
        int a[]={1,2,3,4,5,6,7,8,9,10};
        sort(a,0,a.length-1);
        System.out.println(res);
    }
 
}