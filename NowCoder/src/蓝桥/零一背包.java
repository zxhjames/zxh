package 蓝桥;

import java.util.Scanner;

public class 零一背包 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();//物品个数
		int m = in.nextInt();//背包重量
		int[][] res = new int[n+1][2];
		for(int i=1;i<=n;i++) {
			res[i][0] = in.nextInt();//宝贝重量w
			res[i][1] = in.nextInt();//宝贝价值v
		}
		int[][] dp = new int[2][m+1];
		for(int i=1;i<=m;i++) {
			dp[0][i] = i>=res[1][0]?res[1][1]:0;
		}
		int flag = 0;
		for(int i=2;i<=n;i++) {//从第二行开始计算
			flag = 1-flag;
			for(int j=1;j<=m;j++) {//每一列进行更新
				if(j>=res[i][0]) { 
					dp[flag][j] = Math.max(dp[1-flag][j], dp[1-flag][j-res[i][0]]+res[i][1]);//滚动数组
				}else{
					dp[flag][j] = dp[1-flag][j];
				}
			}
		}
		System.out.println(Math.max(dp[0][m], dp[1][m]));
	}
}
