package 蓝桥;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

public class 重复的字符串 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String ss = in.next();
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		int len = ss.length();
		while(len>0) {
			char s = ss.charAt(len-1);
			if(!map.containsKey(s)) {
				map.put(s, 1);
			}else {
				map.put(s, map.get(s)+1);
			}
			len--;
		}
		HashSet<Integer> hs = new HashSet<Integer>();
		int max = Integer.MAX_VALUE;
		for(Map.Entry<Character, Integer> m:map.entrySet()) {
			max = Math.min(max, m.getValue());
			hs.add(m.getValue());
		}
		if(hs.size() == map.size()) {
			System.out.println(1);
			return ;
		}
		System.out.println(max%2==0?2:max);
	}
}
