package 蓝桥;

import java.util.Arrays;
import java.util.Scanner;
/*
 * AC 100%
 */
public class Anagram {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s1 = in.next();
		String s2 = in.next();
		char[] ss1 = s1.toCharArray();
		char[] ss2 = s2.toCharArray();
		int len = ss1.length;
		for(int i=0;i<len;i++) {
			if((int)ss1[i] <97) {
				ss1[i]+=32;
			}
			if((int)ss2[i] <97) {
				ss2[i]+=32;
			}
		}
		Arrays.sort(ss1);
		Arrays.sort(ss2);
//		for(char i:ss1) {
//			System.out.print(i);
//		}
//		System.out.println();
//		for(char i:ss2) {
//			System.out.print(i);
//		}
		
		
		System.out.println(Arrays.equals(ss1, ss2)==true?"Y":"N");
	}
}
