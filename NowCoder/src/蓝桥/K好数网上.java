package 蓝桥;

import java.util.Scanner;

public class K好数网上 {
	final static int MOD = 1000000007;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int k = in.nextInt();
		int l = in.nextInt();
		int num = 0;
		int[][] nums = new int[l][k];
		
		for(int j=0;j<k;j++) {
			nums[0][j] = 1;
		}
		for(int i=1;i<l;i++) {
			for(int j=0;j<k;j++) {
				for(int x=0;x<k;x++) {
					if(Math.abs(x-j)!=1) {
						nums[i][j] += nums[i-1][x];
						nums[i][j]%=MOD;
					}
				}
			}
		}
		for(int i=0;i<l;i++) {
			for(int j=0;j<k;j++) {
				System.out.print(nums[i][j]+" ");
			}
			System.out.println();
		}
		for(int j=1;j<k;j++) {
			num+=nums[l-1][j];
			num%=MOD;
		}
		System.out.println(num);
	}
}
