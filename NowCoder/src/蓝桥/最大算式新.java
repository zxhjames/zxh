package 蓝桥;

import java.util.Scanner;

public class 最大算式新 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();//数字的个数
		int k = in.nextInt();//使用乘号的次数
		//构建dp数组，dp[i][j]前i个数字中使用j次乘号的最大数
		int dp[][] = new int[n+1][k+1];
		int sum[] = new int[n+1];
		int count = 0;
		for(int i=1;i<=n;i++) { 
			int cur = in.nextInt();
			count+=cur;
			sum[i]=count;
			dp[i][0] = sum[i];//dp初始化表示算式中没有出现乘号，所以最大数只有前i项和
		}
		System.out.println(getMaxNum(dp, sum, n, k));
	}
	
	static int getMaxNum(int[][] dp,int[] sum,int n,int k) {
		for(int j = 1;j<=k;j++) {
			for(int i=1;i<=n;i++) {
				if(i>j)//剪枝
				{
					for(int p = 1;p<i;p++) {
						dp[i][j] = Math.max(dp[i][j], dp[p][j-1]*(sum[i]-sum[p]));//状态转移方程
					}
				}
			}
		}
		return dp[n][k];
	}
}
