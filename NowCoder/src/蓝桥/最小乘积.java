package 蓝桥;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
/*
 * AC 100%
 */
public class 最小乘积 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		ArrayList<Integer> L = new ArrayList<Integer>();
		while(N>0) {
			int n = in.nextInt();
			int[] arr1 = new int[n];
			int[] arr2 = new int[n];
			for(int i=0;i<n;i++) {
				arr1[i] = in.nextInt();
			}
			for(int i=0;i<n;i++) {
				arr2[i] = in.nextInt();
			}
			Arrays.sort(arr1);
			Arrays.sort(arr2);
			int ans = 0;
			int l = 0;
			int r = n-1;
			while(l<=r) {
				ans+= l==r?(arr1[l]*arr2[r]):(arr1[l]*arr2[r] + arr1[r]*arr2[l]);
				l++;
				r--;
			}
			L.add(ans);
			N--;
		}
		for(int i:L) {
			System.out.println(i);
		}
	}
	
}
