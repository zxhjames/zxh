package 蓝桥;

import java.util.Scanner;

public class 最大算式 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		int nums[] = new int[n];
		for(int i=0;i<n;i++) {
			nums[i] = in.nextInt();
		}
		System.out.println(getMax(nums, 0, k));
	}
	static long getSum(int[] a,int start,int end) {
		long sum =0;
		for(int i=start;i<=end;i++) {
			sum+=a[i];
		}
		return sum;
	}
	static long getMax(int[] a,int start,int mulCount) {
		if(mulCount == 0) {
			//如果乘号数目为0,则只需要对数组球和即可
			return getSum(a, start, a.length-1);
		}
		long max = 0;
		for(int i=start;i<a.length-1;i++) {
			long temp = getSum(a, start, i)*getMax(a, i+1, mulCount-1);
			max = Math.max(max, temp);
		}
		return max;
	}
}
