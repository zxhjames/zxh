package 蓝桥;
import java.util.Scanner;
 
public class Password {
 
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
//      long n = cin.nextInt();
        long n = cin.nextLong();
         
        long ans = 1;
        long x = n;
        for(long i = 2; i <= x; i++) {
            if(x % i == 0) {
//          if(x % i == 0 && isPrime(i)) {
                ans *= i;
            }
            while(x % i == 0) {
                x /= i;
            }
        }
        System.out.println(ans);
    }
     
    public static boolean isPrime(long x) {
        for(long i = 2; i <= Math.sqrt(x); i++) {
            if(x % i == 0)
                return false;
        }
        return true;
    }
     
}