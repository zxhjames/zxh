package 蓝桥;

import java.util.Scanner;

public class K好数 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int K = in.nextInt();
		System.out.println(getK(N, K)%1000000007);
	}
	
	static long getK(int N,int K) {
		//N表示0-N-1的范围，K表示划分的位数
		long[][] dp = new long[N][K];
		long ans = 0;
		//考虑K=1的情况
		if(K==1) {
			return 0;
		}
		//考虑K大于1的情况
		for(int i=0;i<N;i++) {
			dp[i][0] = 1;
		}
		for(int j=1;j<K;j++) {
			for(int i=0;i<N;i++) {
				//dp[i][j] = SUM (dp[K][j-1]) |i-K|!=1
				for(int k=0;k<N;k++) {
					dp[i][j]+= Math.abs(i-k)!=1?dp[k][j-1]%1000000007:0;
				}
//				System.out.println(dp[i][j]);
			}
		}
		for(int i=1;i<N;i++) {
			ans+=dp[i][K-1];
		}
		return ans;
		
	}
}
