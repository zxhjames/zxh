package 蓝桥;

import java.util.Scanner;

public class 最大算式_自己实现 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();//输入数字的数目 (1~N)
		int K = in.nextInt();//出现的乘号数目
		int[][] dp = new int[N+1][K+1];//dp[i][j]表示前i个数组成的式子使用j个乘号可以得到的最大值
		//初始化dp的第一列，因为当使用的乘号数目为0时，dp[i][j]只能在使用加号的时候才能获得最大值
		int sum = 0;
		for(int i=1;i<=N;i++) {
			int input = in.nextInt();
			sum+=input;
			dp[i][0] = sum;//dp数组第一列已经初始化好
		}
		System.out.println(getMax(dp, N, K));
	}
	
	static int getMax(int[][] dp,int N,int K) {
		//注意，dp数组的遍历是按每一列每一列来的
		for(int j=1;j<=K;j++) {
			for(int i=2;i<=N;i++) {
				if(i>j) {//因为i个数中最多能使用i-1个乘号，所以为了加速dp的遍历，剪枝
					for(int p=1;p<i;p++) {
						dp[i][j] = Math.max(dp[i][j],dp[p][j-1]*(dp[i][0]-dp[p][0]));//状态转移方程
					}
				}
			}
		}
		return dp[N][K];
	}
}
