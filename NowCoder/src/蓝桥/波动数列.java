package 蓝桥;

import java.util.Scanner;

public class 波动数列 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int s = in.nextInt();
		int a = in.nextInt();
		int b = in.nextInt();
		int count = 0;
		int range = (n-1)*n >> 1;
		for(int i = -100 ; i<100;i++) {
			for(int k = 0;k<=range;k++) {
				if(i*n + k*a - (range-k)*b == s) {
					count++;
				}
			}
		}
		System.out.println(count);
	}
}
