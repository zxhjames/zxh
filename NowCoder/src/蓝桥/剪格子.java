package 蓝桥;

import java.util.ArrayList;
import java.util.Scanner;

public class 剪格子 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int m = in.nextInt();
		int n = in.nextInt();
		int arr[][] = new int[m+1][n+1];
		int book[][] = new int[m+1][n+1];
		int total = 0;
		for(int i=1;i<=m;i++) {
			for(int j=1;j<=n;j++) {
				arr[i][j] = in.nextInt();
				total+=arr[i][j];
			}
		}
		in.close();
		if(total%2!=0) {
			return;
		}
		System.out.println(getmini(arr, book, total));
	}
	
	static int getmini(int[][] arr,int[][] book,int total) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		int min = Integer.MAX_VALUE;
		int[][] dir = {
				{1,0},{-1,0},{0,1},{0,-1}
		};
		int count = arr[1][1];
		int t = 1;
		int row = 1;
		int col = 1;
		int half = total >> 1;
		dfs(arr, book, half, count, t, res ,row,col,dir);
		for (Integer integer : res) {
			min = Math.min(min, integer);
		}
		return min==Integer.MAX_VALUE?0:min;
	}
	static void dfs(int[][] arr,int[][] book,int half,int count,int t,ArrayList<Integer> res,int row,int col,int[][] dir) {
		if(count > half) {
			return;
		}
		if(count == half) {
			res.add(new Integer(t));
		}
		for(int i=0;i<4;i++) {
			int r = row+dir[i][0];
			int c = col+dir[i][1];
			if(r<1 || r>=arr.length || c<1 || c>=arr[0].length || book[r][c] == 1) {
				continue;
			}
			book[r][c] = 1;
			dfs(arr, book, half, count+arr[r][c], t+1, res, r, c, dir);
			book[r][c] = 0;
		}
			
	}
}
