package 蓝桥;

import java.util.Scanner;

public class 删除数组中的零元素 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] arr = new int[n];
		for(int i=0;i<n;i++) {
			arr[i] = in.nextInt();
		}
//		removeElement(arr, n);
		delzero_(arr, n);
	}
	
	//我的方法，基于交换
	static void delzero_(int[] arr,int n) {
		int count = arr[n-1]==0?1:0;
		int cur = arr.length-1;
		while(cur>=1) {
			if(arr[cur-1] == 0) {
				for(int k=cur-1;k<n-1;k++) {
					arr[k] = arr[k]^arr[k+1];
					arr[k+1] = arr[k]^arr[k+1];
					arr[k] = arr[k]^arr[k+1];
				}
				count++;
			}
			cur--;
		}
		int newlen = n - count;
		System.out.println(newlen);
		for(int i=0;i<newlen;i++) {
			System.out.print(arr[i]+" ");
		}
	}
	
	//此方法不能会打乱原有的数组顺序
	  public static void removeElement(int[] nums, int n) {
		  	if(nums.length==1 && nums[0]!=0) {
		  		System.out.println(1);
		  		System.out.println(nums[0]);
		  		return;
		  	}
	        int l = 0;
	        int r = nums.length-1;
	        while(l<=r ){
	            if(nums[l] == 0){
	                while(nums[r]==0 && r>0){
	                    r--;
	                }
	                if(r==0 || r<l)
	                	break;
	                int temp = nums[l];
	                nums[l] = nums[r];
	                nums[r] = temp;
	                l++;
		            r--;
	            }else {
	            	l++;
	            }
	           
	        }
	        System.out.println(l);
	        for(int i=0;i<l;i++) {
	        	System.out.print(nums[i]+" ");
	        }
	  }
}
