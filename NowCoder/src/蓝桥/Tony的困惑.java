package 蓝桥;

import java.util.Scanner;

public class Tony的困惑 {
	static final int MOD = 50000;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int ans = 1;
		int cns = 1;
		while(n>0) {
			if(judge(++cns)) {
				ans*=cns;
				ans%=MOD;
				n--;
			}
		}
		System.out.println(ans);
	}
	
	
	static boolean judge(int num) {
		for(int i=2;i<num;i++) {
			if(num%i == 0) {
				return false;
			}
		}
		return true;
	}
}
