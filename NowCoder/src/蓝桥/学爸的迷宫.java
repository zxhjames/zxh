package 蓝桥;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class 学爸的迷宫 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int[][] map = new int[n+1][m+1];
		for(int i=1;i<=n;i++) {
			String st = in.next();
			for(int j=1;j<=m;j++) {
				map[i][j] =    Integer.parseInt( (String.valueOf(st.charAt(j-1))));
			}
		}
//		System.out.println("DFS解法");
//		DFS(map, n, m);
//		System.out.println("BFS解法");
		BFS(map, n, m);
		
	}

	/**
	 * 
	 * @param map 地图
	 * @param n 地图长度
	 * @param m 地图宽度
	 */
	static void BFS(int[][] map,int n,int m) {
		/*
		 * 使用广度优先策略
		 */
		
		/*
		 * 方向数组
		 */
		int[][] dis = {
				{1,0},//下
				{0,-1},//左
				{0,1},//右
				{-1,0},//上
		};
		/*
		 * 轨迹数组，注意，一定要与dis数组相对应
		 * 思考，为什么一定要按这个顺序?因为题目给出路径输出要按字典序输出，所以我们要优先将字典序小的"方向字母"先入队(先访问)
		 */
		String[] str = {"D","L","R","U"};
		/*
		 * 标记数组，用于判断当前结点是否已经访问
		 */
		int[][] book = new int[n+1][m+1];
		/*
		 * 队列
		 */
		Queue<N> Q = new LinkedList<N>();
		Q.add(new N(1,1,"",null));//起点入队
		book[1][1] = 1;//标记已经走过了
		while(!Q.isEmpty()) {//如果队列不为空，还可继续遍历
			N p = Q.peek();//出队，寻找下一个可以走的结点
			for(int i=0;i<4;i++) {
				//遍历四个方向
				int x = p.x+dis[i][0];
				int y = p.y+dis[i][1];
				//边界判断
				if(x<1 || x>n || y<1 || y>m || map[x][y]==1 || book[x][y]==1) {
					continue;
				}
				if(map[x][y]==0 && book[x][y]==0) {
					//找到可以走的结点，入队
					Q.add(new N(x,y,str[i],p));
					if(x==n && y==m) {
						//如果此时已经到达终点，开始输出路径
						N temp = Q.peek();
						int step = 0;//记录最小步数
						StringBuffer ans = new StringBuffer();//由于是逆序(从终点到起点)记载路径，所以我们最终还要使用append反转一下
						if(temp.x+1 == n) {//此处我无法解释，最后一步测试了出不来路径，需要手动判断一下
							ans.append("D");
						}else if(temp.y+1 == m) {
							ans.append("R");
						}
						while(temp!=null) {
							ans.append(temp.dir);
							step++;
							temp = temp.pre;
						}
						System.out.println(step);
						System.out.println(ans.reverse().toString());
						return;
					}
					book[x][y] = 1;//此时标记已经访问
				}
			}
			Q.poll();
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	/***
	 * 
	 * @param map 地图
	 * @param n 地图的长
	 * @param m 地图的宽
	 */
	static void DFS(int[][] map,int n,int m) {
		/*
		 * 方向数组
		 */
		int[][] dis = {
				{-1,0},//上
				{0,1},//右
				{0,-1},//左
				{1,0},//下
		};
		/*
		 * 路径输出数组，注意此时要按字典序从大到小遍历顺序来排，因为栈是后进先出的，越早入栈越晚遍历到
		 */
		String[] str = {"U","R","L","D"};
		int[][] book = new int[n+1][m+1];
		Stack<N> S = new Stack<N>();
		N begin = new N(1,1,"",null);
		S.push(begin);//把迷宫入口推入栈中，并标记已经访问
		book[1][1]=1; 
		while(!S.isEmpty()) {//如果栈不为空，还可以继续遍历
			N p = S.pop();//弹出第一个元素
			for(int i=0;i<4;i++) {
				//枚举四个方向
				int x = p.x+dis[i][0];
				int y = p.y+dis[i][1];
				//边界判断
				if(x<1 || x>n || y<1 || y>m || map[x][y]==1 || book[x][y]==1) {
					continue;
				}
				if(map[x][y] == 0 && book[x][y] == 0) {
					if(x==n && y==m) {
						//终点判断
						S.push(new N(x,y,str[i],p));
						N tail = S.peek();
						StringBuffer ans = new StringBuffer();//依旧是append后反转一下输出
						int t = 0;
						while(tail.pre!=null) {
							ans.append(tail.dir);
							t++;
							tail = tail.pre;
						}
						System.out.println(t);
						System.out.println(ans.reverse().toString());
						return;
					}
					S.push(new N(x,y,str[i],p));
					book[x][y] = 1;
				}
			}
		}
	}
}

class N{
	int x;
	int y;
	N pre;
	String dir;
	N(int x,int y,String d,N pr){
		this.x = x;
		this.y = y;
		this.dir = d;
		this.pre = pr;
	}
}
