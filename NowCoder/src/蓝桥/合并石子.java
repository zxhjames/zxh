package 蓝桥;

import java.util.Arrays;
import java.util.Scanner;

public class 合并石子 {
	static void mergeStore(int[][] dp,int[] sum) {
		int j=0;
		int temp = 0;
		int length = dp.length;
		for(int i=0;i<length;i++) {
			Arrays.fill(dp[i], Integer.MAX_VALUE);
			dp[i][i] = 0;
		}
		for(int len=2;len<=length;len++) {
			for(int i=0;i<length-len+1;i++) {
				j=i+len-1;
				for(int k=i;k<j;k++) {
					temp=dp[i][k]+dp[k+1][j]+sum[j]-(i==0?0:sum[i-1]);
					dp[i][j] = Math.min(dp[i][j], temp);
				}
			}
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] sum = new int[n];
		int[][] dp = new int[n][n];
		for(int i=0;i<n;i++) {
			if(i==0) {
				sum[i] = in.nextInt();
			}else {
				sum[i] = sum[i-1]+in.nextInt();
			}
		}
		mergeStore(dp, sum);
		System.out.println(dp[0][n-1]);
	}
}
