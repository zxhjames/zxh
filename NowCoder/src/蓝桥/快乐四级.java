package 蓝桥;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
/*
 * AC 100%
 */
public class 快乐四级 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();
		int volume = in.nextInt();
		item[] it = new item[num];
		for(int i=0;i<num;i++) {
			it[i] = new item(in.nextInt(), in.nextInt());
		}
		Arrays.sort(it, new SORT());
//		for(int i=0;i<num;i++) {
//			System.out.println(it[i].weight+" "+it[i].value+" "+it[i].property);
//		}
		float ans = 0;
		for(int i=0;i<num && volume>0;i++) {
			if(it[i].weight<volume) {
				volume-=it[i].weight;
				ans+=it[i].value;
			}else {
				ans+= (float)(volume*it[i].value) /(float) it[i].weight;
				volume = 0;
			}
		}
		System.out.printf("%.1f",ans);//保留一个小数
		
	}
}
class SORT implements Comparator<item>{

	@Override
	public int compare(item o1, item o2) {
		// TODO Auto-generated method stub
		if(o1.property>o2.property) {
			return -1;//-1降序排序 1升序排序
		}else if(o1.property<o2.property) {
			return 1;
		}else {
			return 0;
		}
	}
}
class item{
	int weight;
	int value;
	float property;
	item(int w,int v){
		this.weight = w;
		this.value = v;
		this.property = (float)v/(float)w;
	}
	
	
}
