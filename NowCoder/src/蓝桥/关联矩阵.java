package 蓝桥;

import java.util.Scanner;

public class 关联矩阵 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();// 点的个数
		int m = in.nextInt();//边的个数
		int map[][] = new int[n+1][m+1];
		for(int i=1;i<=m;i++) {
			int n1 = in.nextInt();
			int n2 = in.nextInt();
			map[n1][i] = 1;
			map[n2][i] = -1;
		}
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=m;j++) {
				System.out.print(map[i][j]+" ");
			}
			System.out.println();
		}
	}
}
