package 蓝桥;

import java.util.Scanner;

public class 寻找数组最大值 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] a = new int[n];
		for(int i=0;i<n;i++) {
			a[i] = in.nextInt();
		}
		int u = 0;
		int max = Integer.MIN_VALUE;
		for(int i=0;i<n;i++) {
			if(a[i]>max) {
				max = a[i];
				u=i;
			}
		}
		System.out.println(max+" "+u);
	}
}
