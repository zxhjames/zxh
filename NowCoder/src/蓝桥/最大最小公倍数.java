package 蓝桥;

import java.math.BigInteger;
import java.util.Scanner;
/*
 * 需要使用java库bigInteger
 */
public class 最大最小公倍数 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		BigInteger N = in.nextBigInteger();
		BigInteger a = N;
		BigInteger b = a.add(new BigInteger("-1"));
		BigInteger c = b.add(new BigInteger("-1"));
		BigInteger ans = a.multiply(b).multiply(c);
		BigInteger res[] = {a,b,c};
		//9,8,7
		for(int i=0;i<2;i++) {
			ans=ans.divide(Div(res[i],res[i+1]));
		}
		System.out.println(ans);
	}
	
	static BigInteger Div(BigInteger a,BigInteger b) {
		BigInteger div = a;
		while(b.remainder(a)!=BigInteger.ZERO) {
			b = b.remainder(a);
			div = b;
			a = a.xor(b);
			b = b.xor(a);
			a = a.xor(b);
		}
		return div;
	}
}
