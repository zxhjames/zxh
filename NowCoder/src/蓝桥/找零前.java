package 蓝桥;

import java.util.Arrays;
import java.util.Scanner;

public class 找零前 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int money = scanner.nextInt();
		int n = scanner.nextInt();
		int[] selected = new int[n];
		for(int i=0;i<n;i++) {
			selected[i] = scanner.nextInt();
		}
		int[] dp=new int[money+1];
		dp[0] = 0;
		Arrays.fill(dp, 1,money+1,Integer.MAX_VALUE);
		for(int i=0;i<money+1;i++) {
			if(dp[i] == Integer.MAX_VALUE) {
				continue;
			}
			for(int j=0;j<n;j++) {
				int temp = selected[j];
				if(i+temp<=money) {
					dp[i+temp] = Math.min(dp[i+temp], dp[i]+1);
					System.out.println(dp[i+temp]);
				}
			}
		}
		if(dp[money]==Integer.MAX_VALUE) {
			System.out.println(-1);
		}else {
			System.out.println(dp[money]);
		}
	}
}
