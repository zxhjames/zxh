package 蓝桥;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
/*
 * AC 10%
 */
public class 对局匹配 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int N = in.nextInt();
		int K = in.nextInt();
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for(int i=0;i<N;i++) {
			int t = in.nextInt();
			if(!map.containsKey(t)) {
				map.put(t, 1);
			}else {
				map.put(t,map.get(t)+1);
			}
		}
		if(K==0) {
			System.out.println(map.size());
			return;
		}else {
			List<Integer> L = new ArrayList<Integer>();
			for(int i:map.keySet()) {
				L.add(i);
			}
			int ans = 0;
			int step = K;
			int len = L.size();
			for(int i = 0;i<len;i++) {
				if(L.contains(L.get(i) + step)) {
					int key1 = L.get(i);
					int key2 = L.get(i) + step;
					ans += Math.min(map.get(key1), map.get(key2));
				}
			}
			System.out.println(N - ans);
		}
	}
}
