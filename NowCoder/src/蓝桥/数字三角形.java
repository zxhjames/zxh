package 蓝桥;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/*
 * AC 100%
 */
public class 数字三角形 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		List<List<Integer>> L = new ArrayList<List<Integer>>();
		for(int i=0;i<n;i++) {
			List<Integer> l = new ArrayList<Integer>();
			for(int j=n-i-1;j<n;j++) {
				l.add(in.nextInt());
			}
			L.add(new ArrayList<Integer>(l));
		}
		System.out.println(getMax(n,L));
	}
	
	static int getMax(int n,List<List<Integer>> L)  {
		int len = L.size();
		int dp[] = new int[len+1];
		for(int i=len-1;i>=0;i--) {
			for(int j=0;j<=i;j++) {
				int t = L.get(i).get(j);
				dp[j] = Math.max(dp[j], dp[j+1]) + t;
			}
		}
		return dp[0];
	}
	
	
}
