package 蓝桥;

import java.util.Scanner;

public class 全排列 {
	static int count = 0;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int nums = in.nextInt();
		int[] ans = new int[nums];
		for(int i=0;i<nums;i++) {
			ans[i] = i;
		}
		dfs(ans, 0,nums-1,nums);
	}
	
	static void dfs(int[] ans,int cur,int end,int nums) {
		if(cur == end) {
			output(ans);
			return;
		}
		for(int i=cur;i<=end;i++) {
			swap(ans, cur, i);
			dfs(ans, cur+1, end,nums);
			swap(ans, cur, i);
		}
	}
	
	
	static void output(int[] ans) {
		for(int i=0;i<=ans.length-1;i++) {
			System.out.print(ans[i]);
		}
		System.out.println();
	}
	static void swap(int[] ans,int a,int b) {
		int temp = ans[a];
		ans[a] = ans[b];
		ans[b] = temp;
	}
}
