package 蓝桥;

import java.util.Scanner;
/*
 * AC 0%
 */
public class 幸运数字 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int begin = in.nextInt();
		int end = in.nextInt();
		int[] book = new int[end-begin+2];
		int len = book.length;
		int count = 0;
		int cur = 2;
		while(cur < len) {
			int t = book[cur];
			int p = cur;
			while(p<len) {
				t+=book[p]==0?0:1;
				if(t%cur==0) {
					book[p]=0;
				}
				p++;
			}
			while(book[cur]==0) {
				cur++;
			}
			count++;
		}
		System.out.println(count);
	}
}
