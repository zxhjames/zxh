package 蓝桥;

import java.util.Scanner;

public class 大臣的路费 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[][] map = new int[n+1][n+1];
		for(int i = 1;i<=n;i++) {
			for(int j = 1;j<=n;j++) {
				map[i][j]= i==j?0:-100000;
			}
		}
		for(int i=0;i<n-1;i++) {
			int t1 = in.nextInt();
			int t2 = in.nextInt();
			int dis = in.nextInt();
			map[t1][t2] = dis;
			map[t2][t1] = dis;
		}
//		int[] book = new int[n+1];
//		int sum = 0;
//		int max = Integer.MIN_VALUE;
//		for(int i = 1;i<=n;i++) {
//			int m = 0;
//			dfs(i,map,book,sum,n,m);
//			max = Math.max(max, m);
//			sum = 0;
//		}
		System.out.println(floyed(map, n));
	}
	
//	static void dfs(int cur,int[][] map,int[] book,int sum,int n,int m) {
//			int i = cur;
//			for(int j=1;j<=n;j++) {
//				if(map[i][j]!=-1000 && book[j]==0) {
//					sum+=map[i][j];
//					book[i] = 1;
//					m = Math.max(m, sum);
//					dfs(j,map,book,sum,n,m);
//					book[i]=0;
//				}
//			}
//	}
	//尝试使用floyed
	static int floyed(int[][] map , int n) {
		int ans = Integer.MIN_VALUE;
		for(int k=1;k<=n;k++) {
			for(int i=1;i<=n;i++) {
				for(int j=1;j<=n;j++) {
					map[i][j] = Math.max(map[i][j], map[i][k]+map[k][j]);
					ans = Math.max(ans, map[i][j]);
				}
			}
		}
		return ans;
	}
	
}
