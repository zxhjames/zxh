package 蓝桥;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

public class 和根植物优化1 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int m = in.nextInt();
		int n = in.nextInt();
		int map[][] = new int[m+1][n+1];//地图
		int edge = m*n+1;
		List<List<Integer>> L = new ArrayList<List<Integer>>();
		for(int i=0;i<edge;i++) {
			L.add(new ArrayList<Integer>());//m*n+1个
		}
		int dir[][] = {
				{0,-1},
				{1,0},
				{0,1},
				{-1,0}
		};
		int nums = in.nextInt();
		for(int k=0;k<nums;k++) {
			int x1 = in.nextInt();
			int y1 = in.nextInt();
			L.get(x1).add(y1);
		}
		in.close();
		Queue<node> Q = new LinkedList<node>();
		int sum = 0;
		for(int i=1;i<=m;i++) {
			for(int j=1;j<=n;j++) {
				int N = n*(i-1)+j;
				if(L.get(N).size()==0) {
					sum++;
					continue;
				}
				Q.add(new node(i,j));
				boolean flag = false;
				while(!Q.isEmpty()) {
					node head = Q.poll();
					int fnum = n*(head.x-1)+head.y;
					for(int k=0;k<3;k++) {
						int x = head.x + dir[k][0];
						int y = head.y + dir[k][1];
						int bnum = n*(x-1)+y;
						if(x<1 || y<1 || x>m || y>n || !L.get(fnum).contains(bnum)) {
							continue;
						}
						if(L.get(fnum).contains(bnum)) {
							int p=0;
							for(p=0;p<L.get(fnum).size();p++) {
								if(L.get(fnum).get(p) == bnum) {
									break;
								}
							}
							L.get(fnum).remove(p);
							flag = true;
							Q.add(new node(x,y));
						}
					}
				}
				if(flag) {
					sum++;
				}
			}
		}
		System.out.println(sum);
	}
}
