package 蓝桥;

import java.util.Arrays;
import java.util.Scanner;

public class 翻硬币 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s1 = in.nextLine();
		String s2 = in.nextLine();
		char[] arr1 = s1.toCharArray();
		char[] arr2 = s2.toCharArray();
		dp(arr1,arr2);
	}
	
	static void dp(char[] input,char[] output) {
		int len = input.length;
		int step = 0;
		for(int i =0;i<len-1;i++) {
			if(input[i] != output[i]) {
				input[i] = input[i] == '*'?'o':'*';
				input[i+1] = input[i+1] == '*'?'o':'*';
				step++;
			}
			if(Arrays.equals(input, output)) {
				System.out.println(step);
				return;
			}
		}
	}
}
