package 蓝桥;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/*
 * AC 100%
 */
public class 出现次数最多的整数 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		HashMap<Integer, Integer> hash = new HashMap<Integer, Integer>();
		for(int i=0;i<n;i++) {
			int temp = in.nextInt();
			if(!hash.containsKey(temp)) {
				hash.put(temp, 1);
			}else {
				hash.put(temp, hash.get(temp)+1);
			}
		}
		int max = Integer.MIN_VALUE;
		for(Map.Entry<Integer, Integer> entry:hash.entrySet()) {
			max = Math.max(max, entry.getValue());
		}
		for(Map.Entry<Integer, Integer> entry:hash.entrySet()) {
			if(entry.getValue() == max) {
				System.out.println(entry.getKey());
				return;
			}
		}
		
	}
}
