package 蓝桥;

import java.util.Arrays;
import java.util.Scanner;

public class 分核桃 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int[] arr = new int[3];
		int output = 1;
		for(int i=0;i<3;i++) {
			arr[i] = in.nextInt();
			output*=arr[i];
		}
		Arrays.sort(arr);
		for(int i=0;i<2;i++) {
			int div = Div(arr[i],arr[i+1]);
			output/=div;
		}
		System.out.println(output);
	}
	
	static int Div(int a,int b) {
		int div = a;
		while(b%a!=0) {
			b = b % a;
			div = b;
			a = a^b;
			b = b^a;
			a = a^b;
		}
		return div;
	}
}
