package 蓝桥;

import java.util.Scanner;

public class 回型取数 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		//输入数据
		int[][] map = new int[n][m];
		for(int i=0;i<n;i++) {
			for(int j=0;j<m;j++) {
				map[i][j] = in.nextInt();
			}
		}
		output(map);
	}
	
	static void output(int[][] arr) {
		int begin = 0;
		int x = arr.length-1;
		int y = arr[0].length-1;
		//1.如果只有一行，直接输出
		if(x==0 && y!=0) {
			for(int i=0;i<=y;i++) {
				System.out.print(arr[0][i]+" ");
			}
			return;
		}
		//2.如果只有一列，直接输出
		else if(x!=0 && y==0) {
			for(int i=0;i<=x;i++) {
				System.out.print(arr[i][0]+" ");
			}
			return;
		}
		//输出
		else {
			while(begin <= Math.min(x, y)) {
				boolean down =false;
				boolean right = false;
				boolean up = false;
				boolean left = false;
				//向下
				for(int i=begin;i<=x;i++) {
					down = true;
					System.out.print(arr[i][begin]+" ");
				}
				if(!down)break;
				//向右
				for(int i=begin+1;i<=y ;i++) {
					right = true;
					System.out.print(arr[x][i]+" ");
				}
				if(!right)break;
				//向上
				for(int i=x-1;i>=begin;i--) {
					up = true;
					System.out.print(arr[i][y]+" ");
				}
				if(!up)break;
				//向左
				for(int i=y-1;i>=begin+1;i--) {
					left = true;
					System.out.print(arr[begin][i]+" ");
				}
				if(!left)break;
				//缩小边界
				begin++;
				x--;
				y--;
			}
		}
	}



}
