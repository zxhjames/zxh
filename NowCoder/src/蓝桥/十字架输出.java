package 蓝桥;

import java.util.Scanner;

public class 十字架输出 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int edge = 4*n+5;
		char[][] arr = new char[edge+1][edge+1];
		//create
		//left to rights
		int count = edge;
		int res = edge;
		for(int k=1;k<=count;++k) {
			//层层遍历
			if(k==(edge+1) >> 1) {
				arr[k][k] = '$';
				break;
			}
			if(k==1) {
				for(int i=k;i<=count;i++) {
					if(i==1 || i==2 || i==count || i==count-1) {
						arr[k][i] = '.';
						arr[i][k] = '.';
						continue;
					}
					arr[k][i] ='$';
					arr[i][k] = '$';
				}
				count--;
				continue;
			}
			boolean isJishu = k%2 == 0 ? false:true;
			
			if(isJishu) {
				for(int i=k;i<=count;i++) {
					if(i==k+1 || i==count-1) {
						arr[k][i] = '.';
						arr[i][k] = '.';
						continue;
					}
					arr[k][i] ='$';
					arr[i][k] = '$';
				}
			}
			
			
			else if(!isJishu) {
				for(int i=k;i<=count;i++) {
					if(i==k+1 || i==count-1) {
						arr[k][i] = '$';
						arr[i][k] = '$';
						continue;
					}
					arr[k][i] ='.';
					arr[i][k] = '.';
				}
			}
			count--;
			
		}
		
		//旋转数组
		res = edge;
		for(int k1 = res;k1>=1;k1--) {
			int m = res - k1+1;
			for(int i = m+1;i<=res;i++) {
				arr[i][k1] = arr[m][res-i+1];
			}
		}
		//output
		for(int i=1;i<=edge;i++) {
			for(int j=1;j<=edge;j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}
	}
}
