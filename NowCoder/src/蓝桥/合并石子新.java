package 蓝桥;

import java.util.Scanner;

public class 合并石子新 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] sum = new int[n+1];
		int t = 0;
		for(int i=1;i<=n;i++) {
			sum[i] = t+=in.nextInt();//sum[i]表示第一堆石头到第i堆石头之和
		}
		int[][] dp = new int[n+1][n+1];//dp[i][j]代表第i堆石头到第j堆石头合并的最少花费
		System.out.println(getSolution(dp, sum, n));
	}
	
	static int getSolution(int[][] dp,int[] sum,int n) {
		//初始化dp表，当i==j时，不能移动，此时dp[i][j] = 0,其余全部初始化为一个极大值
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=n;j++) {
				dp[i][j] = (i==j?0:Integer.MAX_VALUE);
			}
		}
		
		//开始计算
		int k = 1;//步长
		while(k<n) {
			int j = 0 ;//初始化第j堆石头的下标
			for(int i=1;i<=n;i++) {
				j = i+k;
				for(int t=i;t<j && j<=n;t++) {
					dp[i][j] = Math.min(dp[i][j], dp[i][t]+dp[t+1][j]+(i<=1?sum[j]:sum[j]-sum[i-1]));
				}
			}
			k++;
		}
		return dp[1][n];
	}
}
