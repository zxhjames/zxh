package 蓝桥;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
/*
 * 能输出结果，但是占用过多内存，且耗时
 */
public class 和根植物 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int m = in.nextInt();
		int n = in.nextInt();
		int edge = m*n+1;
		int [][] map = new int[edge][edge];
		int [] res = new int[edge];
		int dir[][] = {
				{0,-1},
				{1,0},
				{0,1},
				{-1,0}
		};
		int nums = in.nextInt();
		for(int k = 0;k<nums;k++) {
			int x = in.nextInt();
			int y = in.nextInt();
			map[x][y]=1;
			map[y][x]=1;
			res[x] = 1;
			res[y] = 1;
		}
		in.close();
		//开始寻找
		Queue<node> Q = new LinkedList<node>();
		int sum = 0;
		for(int i=1;i<=m;i++) {
			for(int j=1;j<=n;j++) {
				int num = n*(i-1)+j;
				if(res[num]==0) {
					sum++;
					continue;
				}
				Q.add(new node(i,j));
				map[i][j]=0;
				boolean flag = false;
				while(!Q.isEmpty()) {
					node head = Q.poll();
					int fnum = n*(head.x-1)+head.y;
					for(int k=0;k<3;k++) {
						int x = head.x + dir[k][0];
						int y = head.y + dir[k][1];
						int bnum = n*(x-1)+y;
						if(x<1 || y<1 || x>m || y>n || map[fnum][bnum]==0) {
							continue;
						}
						if(map[fnum][bnum] == 1) {
							map[fnum][bnum] = 0;
							flag = true;
							Q.add(new node(x,y));
						}
					}
				}
				if(flag) {
					sum++;
				}
				
			}
		}
		System.out.println(sum);
		
	}
}
class node{
	int x;
	int y;
	node(int x,int y){
		this.x = x;
		this.y = y;
	}
}
