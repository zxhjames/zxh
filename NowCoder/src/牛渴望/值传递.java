package 牛渴望;

public class 值传递
{
	static class intvalue
	{
		int data=10;
		intvalue(int data)
		{
			this.data=data;
		}
	}

	static void fun_1(int x)
	{
		x+=10;
		
	}
	static void fun_2(intvalue i)
	{
		i=new intvalue(20);
	}
	static void fun_3(intvalue i)
	{
		i.data=20;
		
	}
	public static void main(String[] args) {
		int x=10;
		fun_1(x);
		System.out.println(x);
		intvalue i=new intvalue(10);
		fun_2(i);
		System.out.println(i.data);
		intvalue ii=new intvalue(10);
		fun_3(ii);
		System.out.println(ii.data);

	}
}