package 牛渴望;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * 旅行问题
 */
public class Travel_Problem {
	static int INF = 999999;
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();//城市个数
		int aim = in.nextInt();//目的地编号
		int m = in.nextInt();//道路个数
		//建立领接矩阵
		int[][] map = new int[n+1][n+1];
		//初始化
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=n;j++)
			{
				if(i==j) {
					map[i][j]=0;
				}else {
					map[i][j]=INF;
				}
			}
		}
		for(int i=1;i<=m;i++)
		{
			int c1 = in.nextInt();//两城市编号1，编号2,距离 c1,c2,dist
			int c2 = in.nextInt();
			int dist = in.nextInt();
			map[c1][c2] = dist;
			map[c2][c1] = dist;
		}
		int num = in.nextInt();//人数
		int begin[] = new int[num+1];
		for(int i=1;i<=num;i++)
		{
			begin[i] = in.nextInt();//每个人的起点城市编号
		}
		GetMini(n, aim, num, map, num,begin);
	}
	
	static void GetMini(int n,int aim,int m,int[][] map,int num,int[] begin) {
		int book[] = new int[n+1];
		int dis[] = new int[n+1];
		int count=0;
		int sum=0;
		int k=0;
		int min =0 ;
		//初始化dist
		for(int i=1;i<=n;i++) {
			dis[i] = map[aim][i];
		}
		book[aim] = 1;
		while(count!=begin.length-1) {
			min = INF;
			for(int i=1;i<=n;i++) {
				if(book[i]==0 && dis[i]<min)
				{
					min = dis[i];
					k=i;
					for(int j=1;j<=num;j++)
					{
						if(k == begin[j]) {
							count++;
							break;
						}
					}
				}
			}
			book[k] = 1;
			sum+=dis[k];
			for(int p=1;p<=n;p++)
			{
				if(book[p]==0 && dis[p]>map[k][p])
				{
					dis[p]=map[k][p];
				}
			}
		}
		System.out.println("distance = "+sum);
	}
}

