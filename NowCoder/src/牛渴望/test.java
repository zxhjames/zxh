package 牛渴望;

public class test {
    public static void main(String[] args) {    
        objPoolTest();
    }
 
    public static void objPoolTest() {
        int i = 40;
        int i0 = 40;
        //int,float,double等基本数据类存放在栈中，java中的数据运算也是在栈中进行的
        //Integer,Double,String等基本数据类型包装类存放在堆里，相当于是一个对象
        Integer i1 = 400;
        Integer i2 = 400;
        Integer i3 = 0;
        Integer i4 = new Integer(40);
        Integer i5 = new Integer(40);
        Integer i6 = new Integer(0);
        Double d1=1.0;
        Double d2=1.0;
        String  a="hahaha";
        String  b="hahaha";
        
        System.out.println("i=i0\t" + (i == i0));
        System.out.println("i1=i2\t" + (i1 == i2));
        System.out.println("i1=i2+i3\t" + (i1 == i2 + i3));//基本运算都在栈里面
        System.out.println("i4=i5\t" + (i4 == i5));
        System.out.println("i4=i5+i6\t" + (i4 == i5 + i6));    
        System.out.println("d1=d2\t" + (d1==d2)); 
        System.out.println(a==b);
        
        System.out.println();        
    }
}   