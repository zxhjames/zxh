package 牛渴望;
import java.lang.String;
import java.util.Stack;

public class 逆波兰后缀式
{
	public static void main(String[] args) {
		String[] str={"2","1","+","3","*"};
		System.out.println(evalRPN(str));
		
	}

	public static int evalRPN(String[] tokens)
	{
		/**
		 * 依次判断，如果不是字母就弹栈
		 */	
			Stack<String> s=new Stack<String>();
			int a,b;
			for(int i=0;i<tokens.length;i++){
				if(tokens[i].equals("+"))
				{
					a=Integer.valueOf(s.pop());
					b=Integer.valueOf(s.pop());
					s.push(String.valueOf(a+b));
				}
			else if(tokens[i].equals("-"))
				{
					a=Integer.valueOf(s.pop());
					b=Integer.valueOf(s.pop());
					s.push(String.valueOf(b-a));
				}
				else if(tokens[i].equals("*"))
				{
					a=Integer.valueOf(s.pop());
					b=Integer.valueOf(s.pop());
					s.push(String.valueOf(a*b));
				}
				else if(tokens[i].equals("/"))
				{
					a=Integer.valueOf(s.pop());
					b=Integer.valueOf(s.pop());
					try{
					s.push(String.valueOf(b/a));}catch(Exception e){
						System.out.println(e.getMessage());
					}
				}
				else{
					s.push(tokens[i]);
				}
			}
			return Integer.valueOf(s.peek());
			
	}
}