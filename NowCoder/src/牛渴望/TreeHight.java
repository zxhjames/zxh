package 牛渴望;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

//现在有一棵合法的二叉树，树的节点都是用数字表示，现在给定这棵树上所有的父子关系，求这棵树的高度
public class TreeHight 
{
	static int LEN=5;
	static int[][] arr={
		{0,1},{0,2},{1,3},{1,4}
	};
	
	public static void main(String[] args) {
		Tree[] tree=new Tree[5]; 
		for(int i=0;i<5;i++)
		{
			tree[i]=new Tree(i);
			tree[i].left=null;
			tree[i].right=null;
		}


		for(int i=0;i<LEN-1;i++)
		{
			if(tree[arr[i][0]].left==null)
			{
				tree[arr[i][0]].left=tree[arr[i][1]];
			}
			else if(tree[arr[i][0]].right==null)
			{
				tree[arr[i][0]].right=tree[arr[i][1]];
			}
		}


		//tree[0].getdeep(tree[0]);
		//System.out.println(tree[0].HighStack(tree[0]));
		//System.out.println(tree[0].TreeDepth(tree[0]));
		System.out.println(tree[0].getdeep(tree[0]));
		System.out.println(tree[0].treeDepth2(tree[0]));
	}
}


class Tree
{
	int data;
	Tree left;
	Tree right;
	Tree parent;
	boolean isMarked=false;
	Tree(int data)
	{
		this.data=data;
		this.isMarked=false;
	}
	
	//递归求树的高度
	int High(Tree root)
	{
		if(root==null)
			return 0;
		else
		{
			int left=High(root.left);
			int right=High(root.right);
			return 1+Math.max(left, right);
		}
	}
	
	
	public int getDeep(Tree root)
	{
		if(root != null)
		{
			Queue<Tree> Q=new LinkedList<Tree>();
			int deep=0;
			int high=0;
			int count=1;
			Q.add(root);
			while(!Q.isEmpty())
			{
				Tree peek=Q.poll();
				count--;
				if(peek.left!=null)
				{
					Q.add(peek.left);
					deep++;
				}
				if(peek.right!=null)
				{
					Q.add(peek.right);
					deep++;
				}
				if(count==0)
				{
					count=deep;
					deep=0;
					high+=1;
				}

			}
			return high;
			
		}
		return 0;
	}
	
	
	
	
	public int TreeDepth(Tree root)
	{
		if(root==null)
		{
			return 0;
		}
		
		int deep = 0,count=0;
		int nextCount=1;
		Queue<Tree> Q=new LinkedList<Tree>();
		Q.add(root);
		while(!Q.isEmpty())
		{
			Tree p=Q.poll();
			count++;
			if(p.left!=null)
			{
				Q.add(p.left);
			}
			if(p.right!=null)
			{
				Q.add(p.right);
			}
			if(count==nextCount)
			{
				nextCount=Q.size();
				count=0;
				deep++;
			}
		}
		return deep;
	}
	public int getdeep(Tree root)
	{
		if(root!=null)
		{
			Stack<Tree> S=new Stack<Tree>();
			S.push(root);
			root.isMarked=true;
			int deep=1;
			int max=0;
			while(!S.isEmpty())
			{
				if(S.size()>=max)
				{
					max=S.size();
					System.out.println(max);
				}
				
				Tree top=S.peek();
				S.pop();
				//deep--;
				if((top.left.isMarked==true && top.right.isMarked==true) || (top.left==null && top.right==null))
				{
					S.pop();
					System.out.println("pop");
					deep--;
				}
				if(top.left!=null && top.left.isMarked==false)
				{
					S.push(top.left);
					System.out.println("push left");
					top.left.isMarked=true;
					//deep++;
					continue;
				}
				if(top.right!=null && top.right.isMarked==false)
				{
					S.push(top.right);
					System.out.println("push right");
					top.right.isMarked=true;
					deep++;
					continue;
				}
			}
			return max;
		}
		return 0;
	}	
	
    /**
     * 非递归，借助栈来计算深度(层数)
     * 比如                root，先放入栈中
     *         5          当前栈的元素数量为1，len=1，取出栈中此时所有的元素，即5，然后将其子节点3和7放入栈中
     *     3       7      当前栈的元素数量为2，len=2，所以连续从栈中pop两次，使栈中不在含有该层元素，同时将下层节点2和4放入栈中
     * 2       4          当前栈的元素数量为2，len=2，所以连续从栈中pop两次
     *                    记录深度，所以每次pop出栈中所有元素(某层的所有节点)只需深度+1，即depth++
     * @param root
     * @return
     */
    public static int treeDepth2(Tree root) {
        if (root == null) {
            return 0;
        }
        // 初始化深度
        int depth =  0;
        // 存放每层树节点的栈
        Stack<Tree> stack = new Stack<>();
        // 将树的根(即第一层)放入栈中
        stack.push(root);
        while (!stack.isEmpty()) {
            // 当栈不为空时，层数+1，
            // 因为每次都会pop出当前层的所有节点，并将该层所有节点的子节点放入栈中
            depth++;
            // 当前栈中元素的数量
            int length = stack.size();
            while (length-- > 0) {
                // 取出栈中所有的节点，并将对应节点的子节点放入栈中
                Tree node = stack.pop();
                if (node.left != null) {
                    stack.push(node.left);
                }
                if (node.right != null) {
                    stack.push(node.right);
                }
            }
        }
        return depth;
    }
}