package 牛渴望;

import java.util.LinkedList;
import java.util.Queue;
public class Solution {
    public int run(TreeType root) {
        if(root == null)
            return 0;
        if(root.left == null && root.right == null)
            return 1;
         
        int depth = 0;
        Queue<TreeType> queue = new LinkedList<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            int len = queue.size();
            depth++;
            for(int i = 0; i < len; i++){
                TreeType cur = queue.poll();           
                if(cur.left == null && cur.right == null)
                    return depth;
                if(cur.left != null)
                    queue.offer(cur.left);
                if(cur.right != null)
                    queue.offer(cur.right);
            }              
        }
        return 0;
    }
}