package 牛渴望;

import java.util.Scanner;

public class 独立子图{
	static int[][] map;
	static int[][] book;
	static int n;
	static int sum=0;
	static int num=0;
	public static void main(String[] args) {
		int i,j,num=0;
		Scanner in = new Scanner(System.in);
		System.out.println("输入小岛宽度");
		n = in.nextInt();
		map = new int[n+1][n+1];
		book = new int[n+1][n+1];
		for(i=1;i<=n;i++)
			for(j=1;j<=n;j++)
				map[i][j]=in.nextInt();
		
		for(i=1;i<=n;i++) {
			for(j=1;j<=n;j++) {
				if(map[i][j]>0) {
					num--;
					book[i][j]=1;
					dfs(i,j,num);
				}
			}
		}
		
		System.out.println("小岛个数:"+(-num));
		
	}
	
	static void dfs(int x,int y,int color) {
		int dis[][] = {
				{0,1},
				{1,0},
				{0,-1},
				{-1,0}
		};
		map[x][y]=num;
		for(int i=0;i<4;i++) {
			int posx = x+dis[i][0];
			int posy = y+dis[i][1];
			if(posx<1 || posx>n || posy<1 ||posy >n || book[posx][posy]==1) {
				continue;
			}
			if(book[posx][posy]==0 && map[posx][posy]>0) {
				sum++;
				book[posx][posy]=1;
				dfs(posx,posy,color);
			}
		}
		return;
	}
}