package 牛渴望;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class TreeHigh
{
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		ArrayList<treeType> t=new ArrayList<treeType>();
		treeType[] array=new treeType[in.nextInt()];
		for(int i=0;i<array.length;i++)
		{
			array[i]=new treeType(i);
		}
		int LEN=array.length;
		while(LEN-->1)
		{
			int n=in.nextInt();
			int m=in.nextInt();
			array[n].createTree(array[m]);
		}
				//System.out.println(array[0].getdeep_1(array[0]));
				System.out.println(array[0].getdeep_2(array[0]));

	}
}

class treeType
{
	treeType left;
	treeType right;
	int value;
	public treeType(int nextInt) {
		this.value=nextInt;
	}

	public treeType() {
		// TODO Auto-generated constructor stub
	}

	//创建树
	void createTree(treeType root)
	{
		if(root!=null)
		{
			if(this.left==null)
			{
				this.left=root;
				return;
			}
			if(this.right==null)
			{
				this.right=root;
				return;
			}
			else if(this.left!=null && this.right!=null)
			{
				System.out.println("子结点已满，无法创建");
				return;
			}
		}
	}
	
	//递归实现求树的高度
	int getdeep_1(treeType root)
	{
		if(root!=null)
		{
			return 1+Math.max(getdeep_1(root.left), getdeep_1(root.right));
		}
		return 0;
	}
	
	//使用层次遍历求树的高度
	int getdeep_2(treeType root)
	{
		if(root!=null)
		{
			//借用队列先进先出的特性
			Queue<treeType> Q=new LinkedList<treeType>();
			Q.add(root);
			int deepth=0;
			int length=0;
			int count=1;
			while(!Q.isEmpty())
			{
				treeType head=Q.poll();
				count--;
				if(head.left!=null)
				{
					Q.add(head.left);
					length++;
					
				}
				if(head.right!=null)
				{
					Q.add(head.right);
					length++;
				}
				if(count==0)
				{
					count=Q.size();
					length=0;
					deepth++;
				}
			}
			return deepth;
		}
		return 0;
	}
}