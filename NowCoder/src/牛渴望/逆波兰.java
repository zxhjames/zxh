package 牛渴望;
import java.util.Stack;
//逆波兰后缀式
public class 逆波兰
{
	public static void main(String[] args) {
		String[] arr={
			"4",
			"3",
			"+",
			"6",
			"/",
			"9",
			"10",
			"32",
			"-",
			"/",
			"*",
			"+",
			"5",
			"6"
		};
		getSolution(arr);

	}

	public static void getSolution(String[] arr)
	{
		int i=0;
		Stack<String> S=new Stack<String>();
		if(!Character.isDigit(arr[0].charAt(0)) || !Character.isDigit(arr[1].charAt(0)))
		{
			System.out.println("不符合标准!");
			return;
		}
		S.push(arr[i]);	
		while(i!=arr.length-1)
		{
			S.push(arr[++i]);
			if(S.peek().equals("+"))
			{
				int a=0;
				S.pop();
				for(int k=0;k<2;k++)
				{
					if(S.isEmpty())
						return;
					a+=Integer.valueOf(S.peek());
					S.pop();
				}
				S.push(String.valueOf(a));
			}
			else if(S.peek().equals("-"))
			{
				S.pop();
				if(S.isEmpty())
					return;
				int d=Integer.valueOf(S.peek());
				S.pop();
				S.push(String.valueOf(Integer.valueOf(S.peek())-d));
			}
			else if(S.peek().equals("*"))
			{
				int b=1;
				S.pop();
				for(int k=0;k<2;k++)
				{
					if(S.isEmpty())
						return;
					b*=Integer.valueOf(S.peek());
					S.pop();
				}
				S.push(String.valueOf(b));
			}
			else if(S.peek().equals("/"))
			{
				S.pop();
				if(!S.peek().equals(0))
				{
					if(S.isEmpty())
						return;
					int c=Integer.valueOf(S.peek());
					S.pop();
					S.push(String.valueOf(Integer.valueOf(S.peek())/c));
				}
			}

		}
		System.out.println(Integer.valueOf(S.peek()));
	}
}
