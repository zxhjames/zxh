
package 牛渴望;
class BinaryTree
{
	class Node
	{
		//二叉排序树，中序遍历输出排序后的结点数
		private Comparable data;//Comparable类型，含有compareTo的方法
		private Node left; //左孩子节点
		private Node right; //右孩子节点
		
		public void addNode(Node newNode)//添加节点
		{
			if(newNode.data.compareTo(this.data)<0)//如果新节点的值<当前节点的值
			{
				if(this.left==null)
				{
					this.left=newNode;
				}else {
					this.left.addNode(newNode);//递归增加节点
				}
			}
			
			if(newNode.data.compareTo(this.data)>=0)
			{
				if(this.right==null) {
					this.right=newNode;
				}else {
					this.right.addNode(newNode);
				}
			}
		}
		
		
		public void printNode()//打印节点
		{
			if(this.left!=null)
			{
				this.left.printNode();
			}
			System.out.print(this.data+" ");
			if(this.right!=null)
			{
				this.right.printNode();
			}
		}

		public Node FindKey(Node findNode)//寻找节点
		{
			//寻找节点
			//Node findNode=new Node();
			//findNode=find;
			if(findNode.data.compareTo(this.data)<0)
			{
				if(this.left==null)
					return null;
				this.left.FindKey(findNode);
			}
			if(findNode.data.compareTo(this.data)>0)
			{
				if(this.right==null)
					return null;
				this.right.FindKey(findNode);
			}
			if(findNode.data.compareTo(this.data)==0)
			{
				return findNode;
			}
			//找到
			return null;
		}


		//删除结点
		public void delete(Comparable data)
		{

		}


		public void delete(Node n) {
			
			
		}
	
	};
		
	
	
	/********************************************************/
		private Node root=null;
		public void add(Comparable data)
		{
			Node newNode = new Node();
			newNode.data=data;
			if(root==null)
			{
				root=newNode;
			}else {
				root.addNode(newNode);
			}
		}
		
		//寻找节点
		@SuppressWarnings("unchecked")
		public boolean findkey(Comparable data)
		{
			Node findNode=new Node();
			findNode.data=data;
		//	boolean isFind=false;
			if(data.compareTo(root.data)==0)
			{
				System.out.println("find it!");
				return true;
			}
			if(data.compareTo(root.FindKey(findNode).data)==0)
			{
				System.out.println("find it");
				return true;
			}
			return false;
		}

		//删除结点
	
		public void deleteNode(Comparable val)
		{
			Node n=new Node();
			n.data=val;
			if(!findkey(val))
			{
				System.out.println("不能删除，找不到结点");
				return;
			}
			root.delete(n);

		}
		
		public void print()
		{
			this.root.printNode();
		}
	
}

public class binaryTree
{
	 public static void main(String[] args) {
		BinaryTree bt = new BinaryTree();
		bt.add(8);
		bt.add(4);
		bt.add(9);
		bt.add(2);
		bt.add(6);
		bt.add(1);
		bt.add(3);
		bt.add(5);
		bt.add(7);
		System.out.println("二叉排序树!");
		bt.print();
		bt.findkey(5);
		//System.out.println("删除一个节点后");
		bt.deleteNode(10);
		
	}
}

