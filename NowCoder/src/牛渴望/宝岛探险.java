package 牛渴望;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class 宝岛探险 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("输入小岛的边长");
		int n =in.nextInt();
		System.out.println("输入小岛的地理情况");
		int Map[][] = new int[n+1][n+1];
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=n;j++) {
				Map[i][j]=in.nextInt();
			}
		}
		System.out.println("输入降落的坐标");
		int posX = in.nextInt();
		int posY = in.nextInt();
		note begin = new note(posX,posY);
		System.out.println("所在的岛屿面积");
		System.out.println(get_bfs(begin, Map));
	}
	
	static int get_bfs(note begin,int[][] map) {
		int count = 0;
		int book[][]=new int[map.length][map[0].length];
		int[][] dis = {
				{0,1},
				{-1,0},
				{0,-1},
				{1,0}
		};
		if(map[begin.x][begin.y]==0) {
			return 0;
		}
		Queue<note> Q = new LinkedList<note>();
		Q.add(begin);
		count+=map[begin.x][begin.y];
		while(!Q.isEmpty()) {
			note head = Q.poll();
			book[head.x][head.y]=1;
			for(int i=0;i<4;i++) {
				int x = head.x+dis[i][0];
				int y = head.y+dis[i][1];
				if(x<0 || x>=map.length || y<0 || y>=map[0].length || book[x][y]==1)
				{
					continue;
				}
				if(map[x][y]!=0 && book[x][y]==0) {
					book[x][y]=1;
					count+=map[x][y];
					Q.add(new note(x,y));
				}
			}
		}
		return count;
	}
	
}
class note{
	int x;
	int y;
	note(int X,int Y){
		this.x=X;
		this.y=Y;
	}
}
	