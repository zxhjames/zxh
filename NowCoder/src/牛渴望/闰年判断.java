package 牛渴望;

import java.util.Scanner;

public class 闰年判断 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int year = in.nextInt();
		if(year<1990 || year>2050)
		{
			return;
		}
		if(isLunar(year)) {
			System.out.println("yes");
		}else {
			System.out.println("no");
		}
	}
	
	static boolean isLunar(int year)
	{
		if((year%4==0&&year%100!=0)||year%400==0)
		{
			return true;
		}
		return false;
	}
}
