//在一个给定的n个点的平面，找到最大数个百分点，要求在同一条直线上
//import java.util.ArrayList;
package 牛渴望;
import java.util.*;
public class Points 
{
	int x;
	int y;
	Points(){x=0;y=0;}
	Points(int a,int b){x=a;y=b;}

	public static int maxPoints(Points[] points)
	{
		/*int max=0;
		if(points.length==1){
			return 1;
		}
		for(int i=0;i<points.length-1;i++)
		{
			int aa=(points[i].y);
			int bb=(points[i].x);
			int sum=1;
			int k=i;
			for(int j=i+1;j<points.length;j++)
			{
				//if(points[i].y*points[j].x==points[i].x*points[j].y)
				if(j==k+1){
					sum+=1;
					continue;
				}

				int a=(points[i].y-points[j].y);
				int b=(points[i].x-points[j].x);
				if((a*bb)==(b*aa) || (a*bb)==0 || (b*aa)==0 ||a==0 ||b==0 ||aa==0 || bb==0){
					sum+=1;
				}
				aa=a;
				bb=b;
			}
			if(sum>=max){
				max=sum;
			}
		}
		return max;*/
	 int n = points.length;
        if(n < 2) return n;
         
        int ret = 0;
        for(int i = 0; i < n; i++) {
            // 分别统计与点i重合以及垂直的点的个数
            int dup = 1, vtl = 0;
            Map<Float, Integer> map = new HashMap<>();
            Points a = points[i];
             
            for(int j = 0; j < n; j++) {
                if(i == j) continue;
                Points b = points[j];
                if(a.x == b.x) {
                    if(a.y == b.y) dup++;
                    else vtl++;
                } else {
                    float k = (float)(a.y - b.y) / (a.x - b.x);
                    if(map.get(k) == null) map.put(k, 1);
                    else map.put(k, map.get(k) + 1);
                }
            }
             
            int max = vtl;
            for(float k: map.keySet()) {
                max = Math.max(max, map.get(k));
            }
            ret = Math.max(ret, max + dup);
        }
        return ret;
	}

	public static void main(String[] args) {
			Points[] p=new Points[4];
			int[][] arr=new int[][]
			{
				{3,10},
				{0,2},
				{0,2},
				{3,10}
			
			};
			for(int i=0;i<4;i++)
			{
				p[i]=new Points(arr[i][0],arr[i][1]);
			}
			System.out.println(maxPoints(p));
	}
} 
