package 牛渴望;// java 中类的执行顺序
class A {
    public A() {
        System.out.println("A的构造方法");
    }

    public static int j = print();

    public static int print() {
        System.out.println("A print");//父类静态方法
        return 521;
    }
}

public class Execute extends A {
    public Execute() {
        System.out.println("Test1的构造方法");
    }

    public static int k = print();

    public static int print() {
        System.out.println("Test print");//子类静态方法
        return 522;
    }

    public static void main(String[] args) {
        System.out.println("main start");
        //Execute t1 = new Execute();
    }
}