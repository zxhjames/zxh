package 牛渴望;
public class Int 
{

	public static void main(String[] args) {
		Integer a=new Integer(1000);
		Integer b=new Integer(1000);

		System.out.println(a.equals(b));
		System.out.println(a==b);
		System.out.println(a.equals(new Integer(100)));
		System.out.println(a==(new Integer(100)));
		System.out.println("**************************");
		
		Integer c=100;
		Integer d=100;
		System.out.println(c==d);
		System.out.println(c.equals(d));
		System.out.println("/***************/");
		Integer e=10000;
		Integer f=10000;
		System.out.println(e==f);
		System.out.println(e.equals(f));
	}
}