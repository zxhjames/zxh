package 牛渴望;
// 链表反转
public class ListNode
{
    int val;
    ListNode next = null;

    ListNode(int val)
    {
        this.val = val;
    }

	  
    public static ListNode ReverseList(ListNode head)
    {
    	ListNode prev = null;
        ListNode current = head;
        ListNode next = null;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }

       head = prev;
        return head; 
    }

}
