package 牛渴望;
public class javaString类的练习
{
	/**
	 * @java String类常用的方法   
	 */
	public static void main(String[] args) {
		/**
		 * 测试用句
		 */
		String str="hello! my name is james";

		//1.charAt()方法,返回指定索引的字符
		System.out.println(str.charAt(0));
		//output--->  'h'

		//2.compareTo()方法，比较两个字符串的字典排序
		//如果字符串小于字符串比较参数，会返回-1
		//如果字符串大于字符串比较参数，会返回1
		//如果相等的话，返回0
		System.out.println("a".compareTo("b"));

		//3.length()方法返回一个字符串的长度
		System.out.println(str.length());
		//4.toCharArray()方法返回一个字符数组
		char[] c=str.toCharArray();
		for(char cc:c){
			System.out.print(cc+" ");
		}
		System.out.println();

		//5.indexOf()方法，寻找字符串中某个字符的位置,如果不存在返回-1
		System.out.println(str.indexOf('j'));

		//6.concat()用以连接字符串
		System.out.println("james".concat("say: hahaha!"));

		//7.split()通过指定的字符来拆分整个字符串
		String[] temp=str.split("l");
		for(int i=0;i<temp.length;i++){
			System.out.println(temp[i]);
		}

		//8.去除字符串左右空格
		System.out.println("   hahaha   ".trim());
		

		//9.截取字符串substring(x,y)，截取位置为 from x to y-1
		System.out.println(str.substring(0,5));	
	
		//10.equalsIgnoreCase(String)忽略大小写比较两个字符串是否一模一样，返回一个布尔值
		System.out.println("HELLO".equalsIgnoreCase("hello"));

		//11.contains()方法判断一个字符里面是否包含指定的内容，同样返回一个布尔值
		System.out.println(str.contains("hello"));
		
		//12.startsWith()判断字符串是否以指定的字符串开头，返回一个布尔值
		System.out.println(str.startsWith("hello"));

		//13.replaceAll()方法,将某个字符串全部替换为新的字符串
		System.out.println(str.replaceAll("hello", "HELLO"));
	}
}