package 牛渴望;

import java.util.Scanner;
/*
 * Prim最小代价树
 */
public class Prim {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();//顶点个数
		int m = in.nextInt();//边的个数
		int e[][] = new int[n+1][n+1];//领接矩阵
		int dis[] = new int[n+1];//更新数组
		int book[] = new int[n+1];//标记数组
		int INF = 99999;
		int count=0;
		int sum=0;
		int k=0;
		int min=0;
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=n;j++)
			{
				if(i==j) {
					e[i][j]=0;
				}else {
					e[i][j]=INF;
				}
			}
		}
		
		for(int i=1;i<=m;i++)
		{
			int t1 = in.nextInt();
			int t2 = in.nextInt();
			int t3 = in.nextInt();
			e[t1][t2]=t3;
			e[t2][t1]=t3;
		}
		
		for(int i=1;i<=n;i++) {
			dis[i]=e[1][i];
		}
		
		//Prim
		book[1]=1;
		count++;
		while(count<n)
		{
			min = INF;
			for(int i=1;i<=n;i++)
			{
				if(book[i]==0 && dis[i]<min)
				{
					min=dis[i];
					k=i;
				}
			}
			book[k] = 1;
			count++;
			sum+=dis[k];
			for(int p=1;p<=n;p++)
			{
				if(book[p]==0 && dis[p]>e[k][p])
				{
					dis[p]=e[k][p];
				}
			}
		}
		System.out.println(sum);
	}
}
