package 表栈和队列;

import java.util.ArrayList;
/**
 * 给定一个表L和另一个表P，它们包含以升序排列的整数。操作printlots(L,P)将打印L中那些由P所指定的位置上的元素，
 * 例如，如果P=1,3,4,6,那么，L中位于第1,第3,第4和第6个位置上的元素将被打印出来，写出过程 printlots(L,P)，
 * 只可以使用public型的CollectionsAPI进行操作，该过程的运行时间是多长？
 */
public class P_3_1{
	public static void main(String[] args) {
		ArrayList<Integer> P=new ArrayList<Integer>();
		ArrayList<Integer> L=new ArrayList<Integer>();
		/**
		 * 测试数据
		 */
		P.add(1);P.add(3);P.add(4);P.add(6);
		L.add(10);L.add(20);L.add(30);L.add(40);L.add(50);L.add(60);L.add(70);
		long start=System.nanoTime();
		printlot(L,P);
		System.out.println("运行时间为"+(System.nanoTime()-start));
	}
	public static void printlot(ArrayList<Integer> L,ArrayList<Integer> P) {
		int m=0;
		int n=0;
		int k=0;
		for(int i=m;i<P.size();i++)
		{
			for(int j=n;j<L.size();j++) {
				k++;
				if(P.get(i)==k) {
					System.out.println(L.get(j));
					n=k;
					break;
				}
			}
		}
	}
}
