package 表栈和队列;

import java.util.LinkedList;

/*
 * 通过只调整链，而不是数据，来交换两个相邻的元素，使用a单链表
 */
public class P_3_2{
	public static void main(String[] args) {
		Singlelink a=new Singlelink();
		a.addNode(1);
		a.addNode(2);
		a.addNode(3);
		a.addNode(4);
		a.addNode(5);
		a.addNode(6);
		a.addNode(7);
		a.addNode(8);
		a.display();
//		System.out.println("交换后");
//		a.ChangeElem(7, 8);
//		a.display();
//		System.out.println(a.contains(3));
		System.out.println(a.get(1));
	}
}
class Singlelink
{
	//单链表
	Node head=new Node(0);
	public void addNode(int data) {
		Node newNode=new Node(data);
		if(head.Next==null) {
			head.Next=newNode;
		}
		else
		{
			Node p=head.Next;
			while(p.Next!=null) {
				p=p.Next;
			}
			p.Next=newNode;
		}
	}
	
	/**
	 * 交换两个相邻的元素
	 */
	public void ChangeElem(int pos1,int pos2) {
		//根据位置序号进行交换
		//先断开位置序号较大的链
		int edge=Math.max(pos1, pos2)<=Len() && Math.min(pos1, pos2)>=1?Math.min(pos1, pos2):-1;
		if(edge==-1) {
			return;
		}
			//相邻结点交换
			Node begin=head;
			Node p1 = begin.Next;
			int res=0;
			while(res!=edge-1) {
				res++;
				begin=begin.Next;
				p1=p1.Next;
			}
			Node p2=p1.Next;
			Node end=p2.Next;
			begin.Next=p2;
			p1.Next=end;
			p2.Next=p1;
	}
	
	/**
	 * 输出所有元素
	 */
	public void display() {
		Node p=head;
		while(p.Next!=null) {
			System.out.println(p.Next.Data);
			p=p.Next;
		}
	}
	
	/*
	 * 计算链表长度
	 */
	public int Len() {
		int size=0;
		Node p=head;
		while(p.Next!=null) {
			p=p.Next;
			size++;
		}
		return size;
	}
	
	
	/*
	 * contains方法，判断链表中是否含有某一个值
	 */
	public boolean contains(int data) {
		Node node=head;
		while(node.Next!=null) {
			node=node.Next;
			if(node.Data==data) {
				return true;
			}
		}
		return false;
	}
	
	/*
	 * get()方法，获得指定下表序号的元素
	 */
	public int get(int pos) {
		Node p=head;
		int k=-1;
		while(p.Next!=null) {
			p=p.Next;
			k++;
			if(k==pos) {
				return p.Data;
			}
		}
		return -1;
	}
	/*
	 * JiaoJi方法，求两个表的交集
	 */
	public Singlelink JiaoJi(Singlelink L1,Singlelink L2){
		Singlelink L = new Singlelink();
		int k=0;
		for(int i=0;i<L1.Len();i++)
		{
			for(int j=k;j<L2.Len();j++)
			{
				if(L1.get(i)==L2.get(j))
				{
					L.addNode(L1.get(i));
					k=j;
					break;
				}
			}
		}
		return L;
	}
	/*
	 * BingJi，求两个表的并集
	 */
	public Singlelink BingJi(Singlelink L1,Singlelink L2)
	{
		Singlelink L = new Singlelink();
		for(int i=0;i<L1.Len();i++)
		{
			if(!L2.contains(L1.get(i)))
			{
				L2.addNode(L1.get(i));
			}
		}
		return L2;
	}
}


class Node
{
	//结点类
	public Node Prev;
	public int Data;
	public Node Next;
	
	Node(){};
	Node(int data){
		this.Data=data;
	}
}





