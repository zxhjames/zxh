package 图论;

import java.util.Arrays;
import java.util.Scanner;

//堆排序--数组实现
/*
 * 堆的用法总结
 * 1.Dijistra算法中用来记录起点到其他结点的最短距离的数组，寻找最小元素可以用堆来优化
 * 2.寻找一个数列中第K大的元素
 */
public class 堆 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
//		System.out.println("待排序的数字数量:");
//		int n = in.nextInt();
//		int heap[] = new int[n+1];
//		System.out.println("请输入待排序的数字:");
//		//创建堆
//		for(int i = 1;i<=n;i++) {
//			heap[i] = in.nextInt();
//		}
		int heap[] = {4,3,2,10,6,7,8,2,0,9,44,54,23,-1,-100,32,-321,13,45,14,542,1,-1111,32,3,676,22,2321,1,98};
		int len = heap.length-1;
		long begin = System.nanoTime();
		alterheap(heap, len);
		long end = System.nanoTime();
		System.out.println("time"+(end-begin));
	}
	//调整堆
	static void alterheap(int[] heap,int l) {
		int len = l;
		while(len > 0) {
			int gap = len >> 1;
			//向上调整
			for(int i = gap;i>=1;i--) {
				if((i<<1)+1 > len)//表示只有左孩子
				{
					if(heap[i]>heap[i<<1]) {
						swap(i,i<<1,heap);
					}
				}else {
					//左右子孩子都存在
					boolean isLeft = heap[i<<1]<heap[(i<<1)+1]?true:false;
					if(isLeft && heap[i] > heap[i<<1]) {
						swap(i,i<<1,heap);
					}
					if(!isLeft && heap[i] > heap[(i<<1) +1]) {
						swap(i,(i<<1)+1,heap);
					}
				}
			}
			//调整完成，输出顶部元素
			System.out.println(heap[1]);
			//移除顶部元素
			swap(1,len,heap);
			len--;
		}
	}
	
	//交换元素
	static void swap(int a,int b,int[] heap) {
		heap[a] = heap[a]^heap[b];
		heap[b] = heap[a]^heap[b];
		heap[a] = heap[a]^heap[b];
	}
}
