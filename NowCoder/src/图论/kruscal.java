package 图论;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class kruscal
{
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();//结点数
		int m = in.nextInt();//道路数
		List<Nodes> arr = new ArrayList<Nodes>();
		int[] f = new int[n+1];//存储并查集
		//初始化并查集
		for(int i=1;i<=n;i++) {
			f[i] = i;
		}
		//初始化arr数组
		for(int i=1;i<=m;i++) {
			int u = in.nextInt();
			int v = in.nextInt();
			int w = in.nextInt();
			arr.add(new Nodes(u,v,w));
		}
		System.out.println(Kruscal(arr, m, n,f));
	}
	

	static int Kruscal(List<Nodes> arr,int m,int n,int[] f){
		int miniSum = 0;
		int times = 0;
		//根据w，对arr数组进行排序
		Collections.sort(arr, new Comparator<Nodes>() {
			public int compare(Nodes o1, Nodes o2) {
				return o1.w - o2.w;
			}});
		
		for(int i=0;i<m;i++)//有n个结点，表示生成树的边数最多为n-1
		{
			if(merge(arr.get(i).u, arr.get(i).v, f)) {
				times++;
				miniSum+=arr.get(i).w;
			}
			if(times == n-1) {//最多需要找出n-1条路，达到这个次数，就立即退出
				break;
			}
		}
		return miniSum;
	}

	//合并序列，靠左法，选出祖先结点
	static int getf(int x,int[] f){
		if(f[x]==x) {
			return x;
		}else {
			f[x] = getf(f[x],f);
			return f[x];
		}
	}


	//判断一条路的两个结点是否有同一个祖先，若有，该路径不能使用
	static boolean merge(int x1,int x2,int[] f){
		int t1 = getf(x1,f);
		int t2 = getf(x2,f);
		if(t1!=t2) {
			f[t2] = t1;
			return true;
		}
		return false;
	}

}

class Nodes {
	int u;//结点1序号
	int v;//结点2序号
	int w;//结点1与结点2之间的权
	Nodes(int u,int v,int w){
		this.u = u;
		this.v = v;
		this.w = w;
	}
}
