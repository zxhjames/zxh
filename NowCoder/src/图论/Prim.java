package 图论;

import java.util.Scanner;

public class Prim {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("输入城市数量和道路数");
		int n = in.nextInt();//城市数量
		int m = in.nextInt();//城市之间的道路数
		int[][] map = new int[n+1][n+1];
		int[] book = new int[n+1];
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=n;j++) {
				map[i][j] = i==j?0:Integer.MAX_VALUE;
			}
		}
		System.out.println("输入城市之间的信息");
		for(int i = 1;i<=m;i++) {
			int t1 = in.nextInt();
			int t2 = in.nextInt();
			int dis = in.nextInt();
			map[t1][t2] = dis;
			map[t2][t1] = dis;
		}
		System.out.println("输入起点");
		int root = in.nextInt();
		System.out.println(prim(root, map, book));
	}
	
	static int prim(int root,int[][] map,int[] book) {
		int ans = 0;
		int len = book.length;
		int[] dis = new int[len];
		//创建资源数组
		for(int i = 1;i<len;i++) {
			dis[i] = map[root][i];
			//默认初始结点为结点1,初始化结点1到其他各个结点的距离
		}
		book[root] = 1;//标记root已经访问
		for(int k=1;k<len;k++) {
			int pos = 0;
			int min = Integer.MAX_VALUE;
			for(int i=1;i<len;i++) {
				//每次选择没有访问的，并且距离最小生成树距离最短的点
				if(book[i]==0 && dis[i]<min) {
					min = dis[i];
					pos = i;//标记序号
				}
			}
			//开始调整
			book[pos] = 1;
			ans+=dis[pos];//加入最小生成树
			for(int i = 1;i<len;i++) {
				if(book[i]==0 && dis[i]>map[pos][i]) {
					dis[i] = map[pos][i];
				}
			}
		}
		return ans;
	}
}
