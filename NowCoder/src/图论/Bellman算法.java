package 图论;

import java.util.Arrays;
import java.util.Scanner;

public class Bellman算法 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int city = in.nextInt();//城市数目
		int num = in.nextInt();//边的数目
		int[] dis = new int[city+1];
		for(int i=1;i<=city;i++) {
			dis[i] = 100000;//初始化一个极大值
		}
		dis[1] = 0;
		int[] begin = new int[num+1];
		int[] end = new int[num+1];
		int distance[] = new int[num+1];
		for(int i=1;i<=num;i++) {
			begin[i] = in.nextInt();//起点
			end[i] = in.nextInt();//终点
			distance[i] = in.nextInt();//起点到重点的距离
		}
		int[] ans = getMiniPath(dis, begin, end, distance);
		for(int i=2;i<=city;i++) {
			System.out.println(dis[i]);
		}
	}
	
	static int[] getMiniPath(int[] dis,int[] begin,int[] end,int[] distance) {
		int city = dis.length-1;
		int path = distance.length-1;
		int[] dis_ = dis;
		for(int i=1;i<city;i++) {
			//可能出现中途已经更新完dis数组的情况，所以要提前退出
			dis_ = dis;
			for(int k=1;k<=path;k++) {
				dis[end[k]] = Math.min(dis[end[k]], dis[begin[k]]+distance[k]);
			}
			if(Arrays.equals(dis_, dis)) {
				break;
			}
		}
		return dis;
	}
}
