package 图论;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class 图的遍历 {
	//递归搜索法
	static void dfs(int begin,int[][] map,int[] book,int sum,int total) {
		System.out.println(begin);
		sum++;
		if(total == sum) {
			return;
		}
		int size = map.length;
		for(int i=1;i<size;i++) {
			if(map[begin][i]==1 && book[i]==0) {
				book[i] = 1;
				dfs(i,map,book,sum,total);
			}
		}
	}
	
	//调用局部栈
	static void DFS(int cur,int[][] map,int[] book,int n) {
		Stack<Integer> S = new Stack<Integer>();
		boolean flag = true;
		S.push(cur);
		System.out.println(S.peek());
		//begin here
		while(!S.isEmpty()) {
			if(!flag) {
				S.pop();
				if(S.isEmpty()) {
					break;
				}
			}
			int val = S.peek();
			flag = false;
			for(int i=1;i<=n;i++) {
				if(map[val][i]==1 && book[i]==0) {
					S.push(i);
					book[i] = 1;
					System.out.println(i);
					flag = true;
					break;
				}
			}
		}
	}
	
	
	//BFS广度优先搜索
	static void BFS(int cur,int[][] map,int[] book,int n) {
		Queue<Integer> Q = new LinkedList<Integer>();
		//begin here
		Q.add(cur);
		boolean flag = true;
		System.out.println(cur);
		while(!Q.isEmpty()) {
			if(!flag) {
				Q.poll();
				if(Q.isEmpty()) {
					break;
				}
			}
			int val = Q.peek();
			flag = false;
			for(int i = 1;i<=n;i++) {
				if(map[val][i]==1 && book[i] == 0) {
					Q.add(i);
					System.out.println(i);
					book[i] = 1;
					flag = true;
					break;
				}
			}
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("城市个数");
		int n = in.nextInt();
		int[][] map = new int[n+1][n+1];
		int[] book = new int[n+1];
		int sum = 0;
		//初始化地图
		for(int i=1;i<=n;i++) {
			for(int j=1;j<=n;j++) {
				map[i][j] = i==j?0:Integer.MAX_VALUE;
			}
		}
		System.out.println("互相连通的城市有多少组");
		int m = in.nextInt();
		System.out.println("输入连通城市序号");
		for(int i=1;i<=m;i++) {
			int t1 = in.nextInt();
			int t2 = in.nextInt();
			map[t1][t2] = 1;
			map[t2][t1] = 1;
		}
		System.out.println("起点序号:");
		int start = in.nextInt();
		book[start] = 1;
		long begin = System.nanoTime();
//	dfs(start,map,book,sum,n);
	//DFS(start,map,book,n);
		BFS(start,map,book,n);
		long end = System.nanoTime();
		System.out.println("遍历时间:"+(end-begin));
	}
	
}
