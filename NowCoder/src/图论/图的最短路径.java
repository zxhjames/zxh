package 图论;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class 图的最短路径 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("城市个数");
		int n = in.nextInt();
		int[][] map = new int[n+1][n+1];
		int[][] book = new int[n+1][n+1];
		int sum = 0;
		//初始化地图
//		for(int i=1;i<=n;i++) {
//			for(int j=1;j<=n;j++) {
//				map[i][j] = 0;
//				book[i][j] = 0;
//			}
//		}
		System.out.println("互相连通的城市有多少组");
		int m = in.nextInt();
		System.out.println("输入两个连通城市的序号和两个城市的距离");
		for(int i=1;i<=m;i++) {
			int t1 = in.nextInt();
			int t2 = in.nextInt();
			int t3 = in.nextInt();
			map[t1][t2] = t3;
			book[t1][t2] = 1;
		}
		System.out.println("输入起点序号和重点序号");
		int st = in.nextInt();
		int ed = in.nextInt();
		long begin = System.nanoTime();
//		int count = 0;//用来计算最短路的数量
		System.out.println(miniPath(st,ed, map, book, n));
		long end = System.nanoTime();
		System.out.println("遍历时间:"+(end-begin));
	}
	
	static List<List<Integer>> miniPath(int begin,int end,int[][] map,int[][] book,int size) {
		//递归
		List<List<Integer>> L = new ArrayList<List<Integer>>();//存取路径
		List<Integer> l = new ArrayList<Integer>();//存取路径信息
		l.add(begin);
		DFS(L,l,begin,end,map,book,size);
		System.out.println("所有路径");
		return L;
	}
	
	static void DFS(List<List<Integer>> L,List<Integer> l,int begin,int end,int[][] map,int[][] book,int size) {
		//begin here
		//保存每条路径
		//加入链表
		if(begin == end) {
			L.add(new ArrayList<Integer>(l));
			return;
		}
		
		for(int i=1;i<=size;i++) {
			if(map[begin][i]!=0 && book[begin][i] == 1) {
				book[begin][i] =0;
				l.add(i);
				DFS(L,l,i,end,map,book,size);
				l.remove(l.size()-1);
				book[begin][i] = 1;
			}
		}
	}
	
	//使用辅助栈存储结点
	static void DFS_(List<List<Integer>> L,List<Integer> l,int begin,int end,int[][] map,int[][] book,int size) {
		Stack<Integer> S = new Stack<Integer>();//辅助栈
		S.push(begin);
		System.out.println(S.peek());
	}
	
}
