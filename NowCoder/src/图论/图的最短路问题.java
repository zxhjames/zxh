package 图论;

import java.util.Scanner;

public class 图的最短路问题 {
	
	//foyed算法
	static void foyed(int[][] map) {
		int len = map.length;
		for(int k = 1;k<len;k++) {
			for(int i = 1;i<len;i++) {
				for(int j=1;j<len;j++) {
					map[i][j] = Math.min(map[i][j], map[i][k]+map[k][j]);
				}
			}
		}
		
		//output
		for(int i=1;i<len;i++) {
			for(int j=1;j<len;j++) {
				if(map[i][j] == 0 || map[i][j] == 100) {
					continue;
				}
				System.out.println(i+"到"+j+"的最短路是"+map[i][j]);
			}
		}
	}
	
	//Dijikstra算法
	//需要一个标记数组book
	static void Dijkstra(int[][] map,int[] book,int begin) {
		int[] dis = new int[map.length];
		int len = map.length;
		//初始化资源数组
		for(int i=1;i<len;i++) {
			dis[i] = map[begin][i];
		}
		book[begin] = 1;
		for(int k = 1;k<len-1;k++) {
			int min = Integer.MAX_VALUE;//注意这里不要设置太大，不然int型数据会越界
			int pos = 0;
			//找到距离一号顶点最短的序号
			for(int i = 1;i<len;i++) {
				if(book[i] == 0 && dis[i]<min) {
					min = dis[i];
					pos = i;
				}
			}
			//标记当前序号已经访问
			book[pos] = 1;
			//更新数组
			for(int i = 1;i<len;i++) {
				dis[i] = Math.min(dis[i],dis[pos] + map[pos][i]);
			}
		}
		
		for(int i = 1;i<len;i++) {
			if(i==begin) {
				continue;
			}
			if(dis[i]==100) {
				System.out.println(begin+"到"+i+"不可达");
			}else {
				System.out.println(begin+"到"+i+"的最短路是"+dis[i]);
			}
			}
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int city = in.nextInt();
		int[][] map = new int[city+1][city+1];
		for(int i=1;i<=city;i++) {
			for(int j=1;j<=city;j++) {
				map[i][j] = i==j?0:100;
			}
		}
		int path = in.nextInt();
		//读入边
		for(int i=1;i<=path;i++) {
			int t1 = in.nextInt();
			int t2 = in.nextInt();
			int distance = in.nextInt();
			map[t1][t2] = distance;
		}
		foyed(map);
//		int[] book = new int[city+1];
//		System.out.println("输入起点城市");
//		int begin = in.nextInt();
//		Dijkstra(map, book, begin);
	}
}
