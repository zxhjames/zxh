package 图论;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class 九宫格遍历 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int edge = in.nextInt();
		int[][] map = new int[edge+1][edge+1];
		int[][] book = new int[edge+1][edge+1];
		int count = 0;
		for(int i=1;i<=edge;i++)
		{
			for(int j=1;j<=edge;j++) {
				map[i][j] = ++count;
			}
		}
		getall(map, book);
	}
	
	static void getall(int[][] map,int[][] book){
		List<List<Integer>> L = new ArrayList<List<Integer>>();
		List<Integer> l = new ArrayList<Integer>();
		l.add(map[1][1]);
		int cur = map[1][1];
		int row = 1;
		int col = 1;
		book[1][1] = 1;
		dfs(cur,map, book, L, l,row,col);
		for (List<Integer> integer : L) {
			System.out.println(integer);
		}
	}
	
	static void dfs(int cur,int[][] map,int[][] book,List<List<Integer>> L,List<Integer> l,int row,int col) {
		int[][] dir = {
				{0,1},
				{-1,0},
				{0,-1},
				{1,0}
		};
		if(cur==map[map.length-1][map.length-1]) {
			L.add(new ArrayList<Integer>(l));
			return;
		}
		for(int i = 0;i<4;i++) {
			int r = row+dir[i][0];
			int c = col+dir[i][1];
			int edge = map.length;
			if(r>=edge ||r<1 || c>=edge ||c<1 ||book[r][c] == 1) {
				continue;
			}
			if(book[r][c] == 0) {
				l.add(map[r][c]);
				book[r][c] = 1;
				dfs(map[r][c],map,book,L,l,r,c);
				l.remove(l.size()-1);
				book[r][c] = 0;
			}
		}
	}
}
