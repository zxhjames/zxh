package 图论;

import java.util.Scanner;
/*
 * 独立子图问题
 */
public class 并查集 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int[] f = new int[100];
		init(f,n);
		for(int i=1;i<=m;i++) {
			int x = in.nextInt();
			int y = in.nextInt();
			merge(x,y,f);
		}
		int sum = 0;
		for(int i=1;i<=n;i++) {
			if(f[i]==i) {
				sum++;
			}
		}
		System.out.println(sum);
		
	}
	
	static void init(int[] f,int n) {
		for(int i=1;i<=n;i++) {
			f[i] = i;//初始化为相应的下标值
		}
	}
	
	static int getf(int v,int[] f) {
		if(f[v] == v) {
			return v;
		}else {
			f[v]=getf(f[v],f);
			return f[v];
		}
	}
	
	static void merge(int v,int u,int[] f) {
		int t1 = getf(v,f);
		int t2 = getf(u,f);
		if(t1!=t2) {
			f[t2] = t1;
		}
	}
}
