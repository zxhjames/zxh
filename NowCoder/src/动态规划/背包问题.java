package 动态规划;

import java.util.Scanner;

/*
 * 0-1背包问题
 */
public class 背包问题 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();//货品数量
		int total = in.nextInt();//背包体积
		int[] weight = new int[n+1];
		int[] value = new int[n+1];
		for(int i=1;i<=n;i++) {
			weight[i] = in.nextInt();
			value[i] = in.nextInt();
		}
//		System.out.println(getsolution(n,weight,value,total));
		System.out.println(Solution(n,weight,value,total));
	}
	
	/*
	 * 二维数组实现
	 */
	static int Solution(int nums,int[] w,int[] v,int t) {
		int[][] dp = new int[nums+1][t+1];
		for(int k=1;k<=nums;k++) {
			for(int p=1;p<=t;p++) {
				if(p>=w[k]) {//如果当前背包总容量大于当前物品的重量
					dp[k][p] = Math.max(dp[k-1][p-w[k]]+v[k], dp[k-1][p]);
				}else {//如果当前背包总量小于当前物品的重量，那这个物品就不能放
					dp[k][p] = dp[k-1][p];
				}
			}
		}
		return dp[nums][t];
	}
	
	/*
	 * 一位数组解法
	 */
	static int getsolution(int nums,int[] w,int[] v,int t) {
		int[] dp = new int[t+1];
		for(int i=1;i<=t;i++) {
			dp[i] = i>=w[1]?w[1]:0;
		}
		for(int k=2;k<=nums;k++) {
			for(int p=1;p<=t;p++) {
				if(p>=w[k]) {
					dp[p] = Math.max(dp[p-w[k]]+v[k], dp[p]);
				}
			}
		}
		return dp[t];
	}
}
