package 字符串问题;

import java.util.HashMap;
import java.util.Map;

public class 压缩字符串 {
	public static void main(String[] args) {
		String text = "你好我我的名字字字是站站向";
		char[] chars = text.toCharArray();
//		char[] chars = {'a','a','a','b','b','b','b','b','b','b','b','b','b','b','a','a','a','a'};
//		System.out.println(compress(chars));
		System.out.println(_Compress(chars));
	}
	
	public static int Compress(char[] chars) {
		//失败的算法
		if(chars == null) {
			return -1;
		}
		int len = chars.length;
		Map<Character,Integer> map = new HashMap<Character, Integer>();
		for(char c : chars) {
			if(!map.containsKey(c)) {
				map.put(c, 1);
			}else {
				map.put(c, map.get(c)+1);
			}
		}
		String ans = "";
		for(Map.Entry<Character,Integer> entry:map.entrySet()) {
			ans+=entry.getKey();
			ans+=entry.getValue()==1?"":entry.getValue();
		}
//		System.out.println(ans);
		char res[] = ans.toCharArray();
		int len_ = res.length;
		for(int i =0;i<len_;i++) {
			chars[i] = res[i];
		}
		return len_;
	}
	
	 public static int compress(char[] chars) {
		 //AC
		 if(chars == null) {
			 return -1;
		 }
		 int len = chars.length;
		 if(len == 1) {
			 return 1;
		 }
		 int k = 0;
		 String flag = String.valueOf(chars[0]);
		 int count = 0;
		 //构建第二条字符串
		 for(int i =0;i<len;i++) {
			 if(chars[i] != chars[k]) {
				 flag+=(count==1?"":count);
				 flag+=String.valueOf(chars[i]);
				 count = 1;
				 k = i;
				 continue;
			 }count++;
		 }
		 flag+=(count==1?"":count);
		 char ans[] = flag.toCharArray();
		 int newlen = ans.length;
		 for(int i=0;i<newlen;i++) {
			 chars[i] = ans[i];
		 }
//		 System.out.println(flag);
		 return newlen;
	 }
	 
	 public static int _Compress(char[] chars) {
		 //考虑双指针法
		 //AC
		 if(chars == null) { return -1; }
		 int len = chars.length;
		 if(len <= 1) { return 1;}
		 int count = 1;
		 int i = 0;
		 int j = 1;
		 char temp[];
		 do {
			 if(chars[j] == chars[i]) {++count;}
			 else {
				 if(count!=1) {
					 temp = String.valueOf(count).toCharArray();
					 int length = temp.length;
					 for(int t = 0;t<length;++t) {
						 chars[++i] = temp[t];
					 }
				 }
				 chars[++i] = chars[j];
				 count = 1;	 
			 }
		 }while(++j<len);
		 
		 if(count!=1) {
			 temp = String.valueOf(count).toCharArray();
			 int length = temp.length;
			 for(int t = 0;t<length;++t) {
				 chars[++i] = temp[t];
			 }
		 }
		 
		 for(int k = 0;k<len;k++) {
			 System.out.print(chars[k]);
		 } 
		 return 1+i;
	 }
	 
}
