package LeetCode;

public class 接雨水 {
	public static void main(String[] args) {
		int[] arr = {0,1,0,2,1,0,1,3,2,1,2,1};//answer is 6
		int[] brr = {2,0,2};//answer is 2;
		System.out.println(trap(arr));
	}
	
	public static int trap(int[] height) {
		//初步思想，数组第一个非0的元素开始，找到大于等与他的元素，计算中间的雨水容积，否则从其后面的开始寻找
		boolean begin = false;
		int sum = 0;
		int m = 0;
		int k=0;
		for(int i=m;i<height.length;i++) {
			if(!begin) {
				//没有找到第一个元素
				if(height[i]!=0) {
					begin = true;
					if(i==0) {
						i--;
					}
					continue;
				}else {
					continue;
				}
			}else if(begin) {
				//找到第一个元素
				for(int j=i+1;j<height.length;j++) {
					if(height[j]>=height[i]) {
						for(k=i;k<j;k++) {
							sum+=(height[i]-height[k]);
						}
						m=k-1;
						break;
					}
				}
			}
		}
		return sum;
	}
}
