package LeetCode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class 二叉树的右视图 {
	public static void main(String[] args) {
		TreeNode n1 = new TreeNode(1);
		TreeNode n2 = new TreeNode(2);
		TreeNode n3 = new TreeNode(3);
		TreeNode n4 = new TreeNode(4);
		TreeNode n5 = new TreeNode(5);
		TreeNode n6 = new TreeNode(6);
		TreeNode n7 = new TreeNode(7);
		TreeNode n8 = new TreeNode(8);
		n1.left = n2;
		n1.right = n3;
		n2.left=n4;
		n2.right = n5;
		n3.left = n6;
		n3.right = n7;
		System.out.println(rightSideView(n1));
	}
	//层次遍历，求每层的最后一个值
	 public static List<Integer> rightSideView(TreeNode root) {
		 List<Integer> L = new ArrayList<Integer>();
		 List<Integer> LL = new ArrayList<Integer>();
		 if(root == null) {
			 return L;
		 }
		 Queue<TreeNode> Q = new LinkedList<TreeNode>();
		 Q.offer(root);
		 L.add(root.val);
		 int s = Q.size();
		 int count = s;
		 while(!Q.isEmpty()) {
			 TreeNode head = Q.poll();
			 if(--s == 0) {
				 s = count;
				 count = 0;
				 LL.add(L.get(L.size()-1));
			 }
			 if(head.left!=null) {
				 Q.add(head.left);
				 L.add(head.left.val);
				 count++;
			 }
			 if(head.right!=null) {
				 Q.add(head.right);
				 L.add(head.right.val);
				 count++;
			 }
		 }
		 return LL;
	 }
}
