package LeetCode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class 二叉树的层平均值 {
	public static void main(String[] args) {
		TreeNode n1 = new TreeNode(1);
		TreeNode n2 = new TreeNode(2);
		TreeNode n3 = new TreeNode(3);
		TreeNode n4 = new TreeNode(4);
		TreeNode n5 = new TreeNode(5);
		TreeNode n6 = new TreeNode(6);
		TreeNode n7 = new TreeNode(7);
		TreeNode n8 = new TreeNode(8);
		n1.right = n3;
		n3.left = n6;
		n3.right = n7;
		System.out.println(averageOfLevels(n1));
	}
	
    public static  List<Double> averageOfLevels(TreeNode root) {
    	List<Double> L = new ArrayList<Double>();
        TreeNode r = root==null?null:root;
        if(r==null) {return L;}
        Queue<TreeNode> Q = new LinkedList<TreeNode>();
        Double avg = (double) r.val;
        Q.offer(r);
        int s = Q.size();
        int count = s;
        while(!Q.isEmpty())
        {
        	TreeNode head = Q.poll();
        	if(--s ==  0) {
        		s = count;
        		L.add(avg/count);
        		count = 0;
        		avg = 0.0;
        	}
	    	 if(head.left!=null) {
	    		 Q.add(head.left);
	    		 avg+=head.left.val;
	    		 count++;
	    	 }
	    	 if(head.right!=null) {
	    		 Q.add(head.right);
	    		 avg+=head.right.val;
	    		 count++;
	    	 }
        }
        return L;
        
    }
}
