package LeetCode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class 二叉树的层次遍历 {
	 public static List<List<Integer>> levelOrder(TreeNode root) {
	     List<List<Integer>> L = new ArrayList<List<Integer>>();
	     TreeNode r = root;
	     if(r==null) {
	    	 return L;
	     }
	     Queue<TreeNode> Q = new LinkedList<>();
	     Q.offer(r);
	     List<Integer> l = new ArrayList<Integer>();
	     l.add(r.val);
	     int t = Q.size();
	     int count = 1;
	     while(!Q.isEmpty())
	     {
	    	 TreeNode head = Q.poll();
	    	 t--;
	    	 if(t==0) {
	    		 t = count;
	    		 count = 0;
	    		 L.add(l);
	    		 l = new ArrayList<Integer>();
	    	 }
	    	 if(head.left!=null) {
	    		 Q.add(head.left);
	    		 l.add(head.left.val);
	    		 count++;
	    	 }
	    	 if(head.right!=null) {
	    		 Q.add(head.right);
	    		 l.add(head.right.val);
	    		 count++;
	    	 }

	     }
	     return L;
	     
	 }
	 
	 public static List<List<Integer>> levelOrder2(TreeNode root) {
		 TreeNode r = root==null?null:root;
		 List<List<Integer>> L = new ArrayList<List<Integer>>();
		 if(r==null) {
			 return L;
		 }else {
			 Queue<TreeNode> Q = new LinkedList<TreeNode>();
			 List<Integer> l = new ArrayList<Integer>();
			 Q.add(r);
			 l.add(r.val);
			 int size = Q.size();
			 int count = 1;
			 while(!Q.isEmpty()) {
				 TreeNode head = Q.poll();
				if(--size == 0) {
					size = count;
					count = 0;
					L.add(l);
					l = new ArrayList<Integer>();
				}
				
				if(head.left!=null) {
					Q.add(head.left);
					l.add(head.left.val);
					count++;
				}
				if(head.right!=null) {
					Q.add(head.right);
					l.add(head.right.val);
					count++;
				}
			 }
			 List<List<Integer>> LL = new ArrayList<List<Integer>>();
			 int len = L.size();
			 for(int i=len-1;i>=0;i--) {
				 LL.add(L.get(i));
			 }
			 return LL;
		 }
		 
	 }
	 
	 public static void main(String[] args) {
		TreeNode n1 = new TreeNode(1);
		TreeNode n2 = new TreeNode(2);
		TreeNode n3 = new TreeNode(3);
		TreeNode n4 = new TreeNode(4);
		TreeNode n5 = new TreeNode(5);
		TreeNode n6 = new TreeNode(6);
		TreeNode n7 = new TreeNode(7);
		TreeNode n8 = new TreeNode(8);
		n1.right = n3;
		n3.left = n6;
		n3.right = n7;
		System.out.println(levelOrder2(n1));
		
	}
}


