package LeetCode;

public class 二叉搜索树的最小子树 {
	public static void main(String[] args) {
		TreeNode n1 = new TreeNode(1);
		TreeNode n2 = new TreeNode(2);
		TreeNode n3 = new TreeNode(3);
		TreeNode n4 = new TreeNode(4);
		TreeNode n5 = new TreeNode(5);
		TreeNode n6 = new TreeNode(6);
		TreeNode n7 = new TreeNode(7);
		TreeNode n8 = new TreeNode(8);
		n1.left = n2;
		n1.right = n3;
		n2.left=n4;
		n2.right = n5;
		n3.left = n6;
		n3.right = n7;
		insertIntoBST(n1, 8);
		searchBST(n1, 7);
		System.out.println(n7.right.val);
	}
	
	public static TreeNode searchBST(TreeNode root, int val) {
		if(root!=null) {
			if(root.val==val) {
				return root;
			}
			if(val<root.val) {return searchBST(root.left, val);}
			else {return searchBST(root.right, val);}
		}
		return null;
	}
	
	/*
	 * 二叉树的插入操作
	 */
	 public static TreeNode insertIntoBST(TreeNode root, int val) {
		 //递归法
		 if(root!=null) {
		 if(val<root.val) {
			 if(root.left==null) {root.left = new TreeNode(val);return root;}
			 insertIntoBST(root.left, val);
		 }
		 else {
			 if(root.right==null) {root.right = new TreeNode(val);return root;}
			 insertIntoBST(root.right, val);
		 }
		 }
		 return new TreeNode(val);
		 //非递归法
//		 if(root==null) {
//			 return new TreeNode(val);
//		 }
//		 TreeNode top = root;
//		 while(top!=null) {
//			 if(val<top.val) {
//				 if(top.left==null) {
//					 top.left = new TreeNode(val);
//					 break;
//				 }
//				 top = top.left;
//			 }
//			 else {
//				 if(top.right==null) {
//					 top.right = new TreeNode(val);
//					 break;
//				 }
//				 top = top.right;
//			 }
//		 }
//		 return root;
	 }
}
