package LeetCode;

import java.util.Arrays;
import java.util.List;

public class 整数划分 {
	public static void main(String[] args) {
		System.out.println(divide(2,1));
	}
	
	public static int  divide(int n,int m) {
		if(n<1 || m<1) {
			return 0;
		}
		if(n==1 || m==1) {
			return 1;
		}
		if(n<m) {
			return divide(n,n);
		}
		if(n==m) {
			return divide(n,m-1)+1;
		}
		return divide(n,m-1)+divide(n-m,m);
	}
}
