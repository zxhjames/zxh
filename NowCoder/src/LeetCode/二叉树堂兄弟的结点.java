package LeetCode;

import java.util.LinkedList;
import java.util.Queue;

public class 二叉树堂兄弟的结点 {
	public static boolean isCousins(TreeNode root, int x, int y) {
		TreeNode Root = root;
		Queue<TreeNode> Q = new LinkedList<TreeNode>();
		if(Root==null || Root.val==x ||Root.val==y) {
			return false;
		}
		Q.add(Root);
		int k=1;
		int r1 = 0,r2=0;
		boolean flag1 = false,flag2 = false;
		while(!Q.isEmpty()) {
			TreeNode head = Q.peek();
			int n1 = k;
			int n2 = k;
			if(head.left!=null) {
				Q.add(head.left);
			}else if(head.right!=null) {
				Q.add(head.right);
			}
			if(head.left!=null) {
				if(head.left.val==x || head.left.val==y) {
					n1 = n1*2;
					r1 = n1;
					flag1=true;
				}
			}
			if(head.right!=null) {
				if(head.right.val==x || head.right.val==y) {
					n2 = n2*2+1;
					r2 = n2;
					flag2=true;
				}
			}
			if(flag1==true&&flag2==true) {
				if(n1<=3 || n2<=3) {
					return false;
				}
				break;
			}
			Q.poll();
			k++;
			System.out.println(k);
		}
		//开始判断是否堂兄弟
		int res = Math.abs(r1/2 - r2/2);
		if(res>0 && res<=Math.pow(2, k-1)-1) {
			return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		TreeNode n1 = new TreeNode(1);
		TreeNode n2 = new TreeNode(2);
		TreeNode n3 = new TreeNode(3);
		TreeNode n4 = new TreeNode(4);
		TreeNode n5 = new TreeNode(5);
		TreeNode n6 = new TreeNode(6);
//		n1.left=n2;n1.right=n3;
//		n2.left=n4;n2.right=n5;
//		n3.left=n6;
		n1.left=n2;
		n2.right = n3;
		n3.right = n4;
		n4.left = n5;
		System.out.println(isCousins(n1,3,4));
	}
}

