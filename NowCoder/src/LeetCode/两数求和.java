package LeetCode;

public class 两数求和 {
	public static void main(String[] args) {
		
	}
	
	/*
	 * 两数相加
	 */
	public static ListNode addTwoNumbers(ListNode l1,ListNode l2) {
		ListNode h1 = l1;
		ListNode h2 = l2;
		ListNode head = new ListNode(0);
		ListNode tmp = head;
		int res = 0;
		while(h1!=null && h2!=null) {
			int a1 = h1==null?0:h1.val;
			int a2 = h2==null?0:h2.val;
			int ans = a1+a2+res;
			res = ans>9?(ans/10):0;
			ans = res>0?(ans-res*10):ans;
			head.next = new ListNode(ans);
			head = head.next;
		}
//		while(tmp!=null) {
//			System.out.println(tmp.val);
//			tmp = tmp.next;
//		}
		return tmp.next;
	}
	
	
	/*
	 * 删除链表的倒数第N个结点
	 */
	public static ListNode removeNthFromEnd1(ListNode head, int n) {
       /*
        * 参考题解，一次遍历方法
        */
		ListNode l1 = head;
		ListNode l2 = head;
		int L = 1;
		while(l1!=null) {
			l1 = l1.next;
			L++;
		}
		int pos = L - n + 1;
		if(pos == 1) {
			return head.next;
		}
		int flag = 1;
		while(flag!=pos) {
			l2 = l2.next;
		}
		l2.next = l2.next.next;
		return head;
    }
	
	/*
	 * 两次遍历方法
	 */
	public static ListNode removeNthFromEnd2(ListNode head,int n) {
		ListNode HEAD = new ListNode(-1);
		HEAD.next = head;
		ListNode left = HEAD;
		ListNode right = HEAD;
        ListNode tmp = left;
        while(n>=0){
            right = tmp.next;
            tmp = tmp.next;
            n--;
        }
		while(right!=null) {
			left = left.next;
			right = right.next;
		}
		left.next = left.next.next;
		return HEAD.next;
	}
	
	/*
	 * 删除排序列表中的重复元素
	 */
	public static ListNode deleteDuplicates(ListNode head) {
        ListNode tmp = head;
        while(tmp.next!=null) {
        	while(tmp.next.val == tmp.val && tmp.next.next!=null) {
        		tmp.next = tmp.next.next;
        	}
        	if(tmp.next.next == null && tmp.next.val == tmp.val) {
        		tmp.next = null;
        	}
        	tmp = tmp.next;
        }
        return tmp;
    }
	
	/*
	 * 删除排序链表中重复的元素2
	 */
	public static ListNode deleteDuplicates(ListNode head) {
		/*
		 * 思路，采用双指针
		 */
		ListNode tmp = new ListNode(0);
		ListNode ans = tmp;
		tmp.next = head;
		ListNode cur = tmp.next;
		int res = -1;
		while(cur.next!=null) {
			if(cur.val!=res && cur.val!=tmp.val && cur.next.val!=cur.val) {
				tmp = tmp.next;
				cur = cur.next;
				continue;
			}
			if(cur.next.val == cur.val) {
				res = cur.val;
				cur = cur.next;
				continue;
			}
			cur = cur.next;
		}
		return ans.next;
	}
}

class ListNode{
	int val;
	ListNode next;
	ListNode(int x){
		this.val = x;
	}
}
