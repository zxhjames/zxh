package LeetCode;
//求树的最小高度
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class miniTreeDeepth
{
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		ArrayList<treeType> t=new ArrayList<treeType>();
		treeType[] array=new treeType[in.nextInt()];
		for(int i=0;i<array.length;i++)
		{
			array[i]=new treeType(i);
		}
		int LEN=array.length;
		while(LEN-->1)
		{
			int n=in.nextInt();
			int m=in.nextInt();
			array[n].createTree(array[m]);
		}
				//System.out.println(array[0].getdeep_1(array[0]));
		System.out.println(array[0].getminiHeight(array[0]));
	}
}

class treeType
{
	treeType left;
	treeType right;
	int value;
	public treeType(int nextInt) {
		this.value=nextInt;
	}

	public treeType() {
		// TODO Auto-generated constructor stub
	}

	//创建树
	void createTree(treeType root)
	{
		if(root!=null)
		{
			if(this.left==null)
			{
				this.left=root;
				return;
			}
			if(this.right==null)
			{
				this.right=root;
				return;
			}
			else if(this.left!=null && this.right!=null)
			{
				System.out.println("子结点已满，无法创建");
				return;
			}
		}
	}
	
	//求解树的最小高度
	int getminiHeight(treeType root)
	{
		if(root!=null)
		{
			Queue<treeType> Q=new LinkedList<>();
			Q.add(root);
			int deep=0;
			while(!Q.isEmpty())
			{
				int len=Q.size();
				
				deep++;
				for(int i=0;i<Q.size();i++)
				{	
					treeType node=Q.poll();
					if(node.left==null && node.right==null)
					{
						return deep+1;
					}	
					if(node.left!=null)
					{
						Q.add(node.left);
					}
					if(node.right!=null)
					{
						Q.add(node.right);
					}
				}	
		
			}	
		}
		
		return 0;
	}
			
	
}