package LeetCode;

import java.util.ArrayList;
import java.util.List;

public class 二叉树的所有路径 {
	public static List<String> binaryTreePaths(TreeNode root){
		List<String> L = new ArrayList<String>();
		seek(root,L,new String(""));
		return L;
	}
	
	public static void seek(TreeNode root,List<String> L,String s) {
		if(root == null) {
			return;
		}
		if(root.left==null && root.right==null) {
			String val = root.val+"";
			s+=val;
			L.add(new String(s));
			return;
		}
		String v = root.val+"->";
		s+=v;
		seek(root.left,L,s);
		seek(root.right,L,s);
		
	}
	
	public static void main(String[] args) {
		TreeNode root = new TreeNode(0);
		TreeNode a1 = new TreeNode(1);
		TreeNode a2 = new TreeNode(2);
		TreeNode a3 = new TreeNode(3);
		TreeNode a4 = new TreeNode(4);
		TreeNode a5 = new TreeNode(2);
		TreeNode a6 = new TreeNode(1);
		root.left = a1;
		root.right = a2;
		a1.left = a3;
		a1.right = a4;
		a2.left = a5;
		a5.left = a6;
		System.out.println(binaryTreePaths(root));
	}
}
