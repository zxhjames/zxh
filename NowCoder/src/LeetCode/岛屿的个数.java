package LeetCode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class 岛屿的个数 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
//		int n = in.nextInt();
//		int m = in.nextInt();
//		int[][] map = new int[n][m];
//		for(int i=0;i<n;i++)
//			for(int j=0;j<m;j++)
//				map[i][j] = in.nextInt();
		int map[][] = {
				{1,1,0,1},
				{1,0,0,0},
				{0,0,1,0},
				{0,1,1,1}
		};
		System.out.println(numIslands(map));
		
		
		
		
		
	}
	public static int numIslands(int[][] grid) {
		int xlen = grid.length;
		int ylen = grid.length;
		int sum = 0;
		int[][] book = new int[xlen][ylen];
		Queue<Position> Q = new LinkedList<Position>();
		int[][] dir = {
				{0,1},//up
				{1,0},//right
				{0,-1},//down
				{-1,0}//left
		};
		for(int i=0;i<xlen;i++) {
			for(int j=0;j<ylen;j++) {
				if(book[i][j]==0 && grid[i][j]==1) {
					Position pos = new Position(i,j);
					Q.add(pos);
					book[i][j]=1;
					while(!Q.isEmpty()) {
						Position head = Q.poll();
						for(int k=0;k<3;k++) {
							int posX = head.x+dir[k][0];
							int posY = head.y+dir[k][1];
							if(posX<0 || posX>xlen-1 || posY<0 || posY>ylen-1 || book[posX][posY]==1 || grid[posX][posY]==0) {
								continue;
							}
							if(book[posX][posY]==0 && grid[posX][posY]==1) {
								book[posX][posY]=1;
								grid[posX][posY] = 0;
								pos = new Position(posX, posX);
								Q.add(pos);
							}
						}
					}
					sum++;
				}
			}
		}
		return sum;
	}
}

class Position{
	int x;
	int y;
	public Position(int X,int Y) {
		// TODO Auto-generated constructor stub
		this.x = X;
		this.y = Y;
	}
}