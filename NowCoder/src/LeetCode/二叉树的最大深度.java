package LeetCode;

import java.util.LinkedList;
import java.util.Queue;

public class 二叉树的最大深度 {
	public static void main(String[] args) {
		
	}
	
	static int  maxDepth(TreeNode root) {
			TreeNode r = root;
			if(r==null) {
				return 0;
			}
			Queue<TreeNode> Q = new LinkedList<TreeNode>();
			Q.add(r);
			int high = 1;
			int count = 1;
			while(!Q.isEmpty()) {
				TreeNode head = Q.poll();
				count--;
				if(head.left!=null) {
					Q.add(head.left);
				}else if(head.right!=null) {
					Q.add(head.right);
				}
				if(head.left==null && head.right==null) {
					return high;
				}
				if(count==0) {
					count = Q.size();
					high++;
				}
			}
			return high;
	}
}

