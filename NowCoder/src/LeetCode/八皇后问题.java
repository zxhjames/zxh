package LeetCode;

import java.util.Stack;

public class 八皇后问题 {
	public static void main(String[] args) {
		int size = 10;//N
		int[][] map = new int[size][size];
		long st = System.currentTimeMillis();
		Queen(map, 0, 0, size);
//		Queen_(map, size);
		System.out.println("解法:"+count);
		long ed =System.currentTimeMillis();
		System.out.println("耗时:"+(ed - st)+"毫秒");
	}
	
	static int count  =0;
	
	public static void Queen(int[][] arr,int row,int column,int size) {
		if(row>size-1) {
			count++;
			printMap(arr);
			System.out.println();
			return;
		}
		for(int k = 0;k<size;k++) {//每一行进行遍历
			if(!isAttack(arr, row, k, size)) {
				arr[row][k] = 1;
				Queen(arr,row+1,k,size);
				arr[row][k] = 0;
			}
		}
	} 
	






	
	public static void Queen_(int[][] arr,int size) {
		//使用栈来求解八皇后问题
		Stack<Integer> SaveX = new Stack<Integer>();//存储x的坐标
		Stack<Integer> SaveY = new Stack<Integer>();//存储y的坐标
		int posx = 0;
		int posy = 0;
		while(posx>0 || posy<size) {
			if(SaveX.size()==size) {
				count++;
				printMap(arr);
				arr[posx][posy] = 0;
				SaveX.pop();
				SaveY.pop();
				posy--;
				posx++;
			}
			SaveX.push(posx);
			SaveY.push(posy);
			//当前行判断
			for(int i = 0;i<size;i++) {
				if(i==posy){
					continue;
				}
				//如果满足
				if(!isAttack(arr, posx, posy, size)) {
					arr[posx][posy] = 1;
					posx = i;
					posy++;
					break;
				}
			}
			arr[posx][posy]=0;
			SaveX.pop();
			SaveY.pop();
		}
		
	}
	static boolean isAttack(int[][] arr,int row,int column,int size) {
		for(int i=0;i<size;i++) {
			for(int j=0;j<size;j++) {
//				if(i==row && j==column) {
//					continue;
//				}
				if((i==row && arr[i][j]==1) || (j==column && arr[i][j]==1) || ((Math.abs(i-row)==Math.abs(j-column))&&arr[i][j]==1)) {
					return true;
				}
			}
		}
		return false;
	}
	static void printMap(int[][] arr) {
		int size = arr.length;
		for(int i=0;i<size;i++) {
			for(int j = 0;j<size;j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	
}
