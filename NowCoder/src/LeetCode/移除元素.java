package LeetCode;

public class 移除元素 {
	  public static int removeElement(int[] nums, int val) {
		  	if(nums.length==1 && nums[0]!=val) {
		  		return 1;
		  	}
	        int l = 0;
	        int r = nums.length-1;
	        while(l<=r ){
	            if(nums[l] == val){
	                while(nums[r]==val && r>0){
	                    r--;
	                }
	                if(r==0 || r<l)
	                	break;
	                int temp = nums[l];
	                nums[l] = nums[r];
	                nums[r] = temp;
	                l++;
		            r--;
	            }else {
	            	l++;
	            }
	           
	        }
	        return l;
	    }
	  
	 public static void main(String[] args) {
		int[] nums = {3,4,0,0,2};
		int pos = removeElement(nums, 0);
		for(int i:nums) {
			System.out.print(i+" ");
		}
		System.out.println();
		System.out.println(pos);
	}
}
