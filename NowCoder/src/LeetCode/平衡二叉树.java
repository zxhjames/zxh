package LeetCode;

public class 平衡二叉树 {
		public static boolean isBalanced(TreeNode root) {
	        if(root == null){
	            return false;
	        }
	        int h = getHight(root);
	        if(h>1){
	            return false;
	        }
	        return isBalanced(root.left) || isBalanced(root.right);
	    }
	    
	    public static int getHight(TreeNode root){
	    	if(root == null) {
	    		return 0;
	    	}
	    	return Math.abs(Math.max(getHight(root.left), getHight(root.right)) - Math.min(getHight(root.left), getHight(root.right)));
	    }
	
	public static void main(String[] args) {
		TreeNode root = new TreeNode(0);
		TreeNode a1 = new TreeNode(1);
		TreeNode a2 = new TreeNode(2);
		TreeNode a3 = new TreeNode(3);
		TreeNode a4 = new TreeNode(4);
		TreeNode a5 = new TreeNode(2);
		TreeNode a6 = new TreeNode(1);
		root.left = a1;
		root.right = a2;
		a1.left = a3;
		a1.right = a4;
		a2.left = a5;
		a5.left = a6;
		System.out.println(isBalanced(root));
	}
}
