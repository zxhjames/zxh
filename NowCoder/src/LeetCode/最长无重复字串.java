package LeetCode;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class 最长无重复字串 {
	public static void main(String[] args) {
		System.out.println(lengthOfLongSubString("abcabcbb"));
	}
	 static int lengthOfLongSubString(String s) {
		   int size = 0;
	       int LEN = s.length();
	       Map<Character,Integer> m = new HashMap<Character,Integer>();
	        int j=0;
	       while(true){
	           while(j<LEN && !m.containsKey(s.charAt(j))){
	               m.put(s.charAt(j),1);
	               j++;
	           }
	           size = Math.max(m.size(),size);
	           if(j==LEN) {
	        	   break;
	           }
	           m.clear();
	       }
	        return size;
	 }
}
