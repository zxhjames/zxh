package LeetCode;

public class 两数中位数 {
	public static void main(String[] args) {
//		System.out.println(findMedianSortedArrays([1],[2]));Integer.valueOf(1)
	}
	
	
	  public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
	        int arr[] = new int[nums1.length+nums2.length];
	        for(int i=0,j=0;i<nums1.length&&j<nums2.length;i++,j++){
	            arr[i]=nums1[i];
	            arr[j+nums1.length]=nums2[j];
	        }
	        Sort(arr);
	        int k = arr.length/2;
	        if(arr.length%2==0){
	            return (arr[k]+arr[k-1])/2;
	        }
	        return arr[k];
	        
	    }
	    
	    	public static void Sort(int[] arr) {
			int[] temp=new int[arr.length];
			/**
			 * 在排序前，建立一个长度等于原数组长度的临时数组，
			 * 避免递归过程中频繁开辟空间
			 */
			sort(arr,0,arr.length-1,temp);//下标从0～length-1
		}
		
		public static void sort(int[] arr,int left,int right,int[] temp) {
			if(left<right) {
				int mid=(left+right)/2;
				sort(arr,left,mid,temp);//左边归并排序
				sort(arr,mid+1,right,temp);//右边归并爬序
				merge(arr,left,mid,right,temp);//将两个有序子数组合排序操作
			}
		}
		public static void merge(int[] arr,int left,int mid,int right,int[] temp) {
			int i=left;
			int j=mid+1;
			int t=0;
			while(i<=mid && j<=right) {
				if(arr[i]<=arr[j]) {
					temp[t++]=arr[i++];
				}else {
					temp[t++]=arr[j++];
				}
			}
			while(i<=mid) {
				temp[t++]=arr[i++];
			}
			while(j<=right) {
				temp[t++]=arr[j++];
			}
			t=0;
			while(left<=right) {
				arr[left++]=temp[t++];
			}
		}
}
