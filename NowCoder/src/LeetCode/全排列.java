package LeetCode;

import java.util.ArrayList;
import java.util.List;

public class 全排列 {
	public static void permute(int[] nums,int k,int[] copy,int[] book) {
		if(k==nums.length) {
			for(int i=0;i<copy.length;i++) {
				System.out.print(copy[i]+" ");
			}
			System.out.println();
			return;
		}
		for(int i = 0;i<nums.length;i++) {
			if(book[i]==0) {
				copy[i]=nums[k];
				book[i]=1;
				permute(nums,k+1,copy,book);
				book[i]=0;
			}
		}
	}
	
	public static List<List<Integer>> permute(int[] nums) {
        List<Integer> ll = new ArrayList<Integer>();
        ll.add(1);
        ll.add(2);
        List<List<Integer>> lll = new ArrayList<List<Integer>>();
        lll.add(ll);
        lll.add(ll);
        return lll;
    }
	public static void main(String[] args) {
		int[] nums = {1,2,6,7};
//		ArrayList<Integer> copy = new ArrayList<Integer>();
		int[] copy = new int[nums.length];
		int[] book = new int[nums.length];
		//permute(nums, 0, copy, book);
		System.out.println(permute(nums));
	}
}
