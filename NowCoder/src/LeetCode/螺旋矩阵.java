package LeetCode;

import java.util.Scanner;

public class 螺旋矩阵 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[][] demo = getMatrix(n);
		for(int i=0;i<n;i++) {
			for(int j=0;j<n;j++) {
				System.out.print(demo[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	static int[][] getMatrix(int n){
		int[][] map = new int[n][n];
		int begin = 0;
		int end = n-1;
		int count = 1;
		while(begin<=end) {
//			boolean right = false;
//			boolean down = false;
//			boolean left = false;
//			boolean up = false;
			//right
			for(int i=begin;i<=end;i++) {
				map[begin][i] = count++;
//				right = true;
			}
//			if(!right)break;
			//down
			for(int i=begin+1;i<=end;i++) {
				map[i][end] = count++;
//				down = true;
			}
//			if(!down)break;
			//left
			for(int i=end-1;i>=begin;i--) {
				map[end][i] = count++;
//				left = true;
			}
//			if(!left)break;
			//up
			for(int i=end-1;i>begin;i--) {
				map[i][begin] = count++;
//				up = true;
			}
//			if(!up)break;
			begin++;
			end--;
		}
		return map;
	}
}
