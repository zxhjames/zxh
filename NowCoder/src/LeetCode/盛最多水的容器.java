package LeetCode;

public class 盛最多水的容器 {
	public static void main(String[] args) {
		int arr[] = {2,3,4,5,18,17,6};
		System.out.println(MaxVolume(arr));
	}
	
	public static int MaxVolume(int[] height) {
		int max = 0;
		int area= 0;
		for(int i=0;i<height.length-1;i++) {
			for(int j=i+1;j<height.length;j++) {
				area = height[i]>=height[j]?height[j]*(j-i):height[i]*(j-i);
				max = Math.max(max, area);
			}
		}
		return max;
	}
}
