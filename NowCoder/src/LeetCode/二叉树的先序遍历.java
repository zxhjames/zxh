package LeetCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class 二叉树的先序遍历 {
//	public static List<List<Integer>> prevtrace(TreeNode root,int sum) {
//		List<List<Integer>> L = new ArrayList<List<Integer>>();
// 		List<Integer> l = new ArrayList<Integer>();
//		Stack<TreeNode> S = new Stack<TreeNode>();
//		S.push(root);
//		while(!S.isEmpty()) {
//			TreeNode head = S.pop();
//			l.add(head.val);
//			if(head.left==null && head.right==null) {
//				int s = 0;
//				List<Integer> ll = new ArrayList<Integer>();
//				for (Integer treeNode : l) {
//					s+=treeNode;
//					ll.add(treeNode);
//				}
//				if(s == sum) {
//					L.add(ll);
//				}
//				l.remove(l.size()-1);
//			}
//			if(head.right!=null) {
//				S.push(head.right);
//			}
//			if(head.left!=null) {
//				S.push(head.left);
//			}
//		}
//		return L;
//	}
	
	//递归解法
	public static List<List<Integer>> pathNum(TreeNode root,int sum){
		//递归法
		List<List<Integer>> L = new ArrayList<List<Integer>>();
		preTraceTree(root,sum,L,new ArrayList<Integer>());
		return L;
	}
	public static void preTraceTree(TreeNode root,int sum,List<List<Integer>> L,List<Integer> l) {
		if(root == null) {
			return;
		}
		if(root.left == null && root.right == null && sum - root.val == 0) {
			l.add(root.val);
			L.add(new ArrayList<Integer>(l));
			l.remove(l.size() - 1);
			return;
		}
		l.add(root.val);
		preTraceTree(root.left,sum-root.val,L,l);
		preTraceTree(root.right,sum-root.val,L,l);
		l.remove(l.size()-1);
	}
	
	public static void main(String[] args) {
		//begin here
		TreeNode root = new TreeNode(0);
		TreeNode a1 = new TreeNode(1);
		TreeNode a2 = new TreeNode(2);
		TreeNode a3 = new TreeNode(3);
		TreeNode a4 = new TreeNode(4);
		TreeNode a5 = new TreeNode(2);
		TreeNode a6 = new TreeNode(1);
		root.left = a1;
		root.right = a2;
		a1.left = a3;
		a1.right = a4;
		a2.left = a5;
		a5.left = a6;
		//System.out.println(prevtrace(root, 5));
		System.out.println(pathNum(root, 5));
	}
}
