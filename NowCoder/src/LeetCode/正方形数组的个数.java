package LeetCode;

import java.util.ArrayList;
import java.util.Map;

public class 正方形数组的个数 {
	public static void main(String[] args) {
		int[] A = {2,2,2};
		System.out.println(numSquarefulPerms(A));
	}
	
	public static int numSquarefulPerms(int[] A) {
		int[] arr = new int[12];
		int count = 0;
		for(int i=0;i<A.length-1;i++) {
			for(int j=0;j<A.length;j++) {
				boolean flag = true;
				if(Math.pow(A[i]+A[j], 0.5)==(int)Math.pow(A[i]+A[j], 0.5)) {
					int res = (int) (Math.pow(A[i]+A[j], 0.5));
					for(int k=0;k<arr.length;k++) {
						if(arr[k]==res) {
							flag = false;
							break;
						}
					}
					if(flag) {
						arr[count++]=res;
					}
				}
			}
		}
		return count;
	}
}
