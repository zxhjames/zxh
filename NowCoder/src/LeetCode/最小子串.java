package LeetCode;

public class 最小子串 {
	public static void main(String[] args) {
		System.out.println(lengthOfLongestSubstring("pwwkew"));
	}
	
	public static int lengthOfLongestSubstring(String s) {
		int max = -1;
		int count =1;
		boolean flag = true;
		int k=0;
		for(int i=1;i<s.length();i++) {
			flag=true;
			for(int j=i-1;j>=k;j--) {
				if(s.charAt(j)==s.charAt(i)) {
					flag = false;
					k=j+1;
					i-=1;
					count=1;
					break;
				}
			}
			if(flag==true) {
				count++;
			}
			max = Math.max(max, count);
		}
		return max;
	}
}
