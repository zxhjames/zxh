package LeetCode;


public class 整数反转 {
	public static void main(String[] args) {
		int x = 1534;
		System.out.println(reverse(x));
		test();
	}
	public static int reverse(int x) {
        int xx = x>0?x:-x;
        int sum = 0;
        String s = String.valueOf(xx);
        StringBuffer ss = new StringBuffer().append(s).reverse();
        if(ss.charAt(0)=='0') {
        	ss=ss.deleteCharAt(0);
        }
        sum = Integer.valueOf(ss.toString().trim());
        if(sum>=Integer.MAX_VALUE || sum<=Integer.MIN_VALUE) {
        	return 0;
        }
        if(x>0) {
        	return sum;
        }
        return -sum;
        
	}
	
	public static void test() {
		StringBuffer ss = new StringBuffer().append("abc").reverse();
		System.out.println(ss);
	}
}
