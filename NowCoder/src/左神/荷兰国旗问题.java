package 左神;

import java.util.Scanner;

//给定一个数组，一个数，将大于这个数的数据放数组右边，将小于这个数的数据放在左边，将等于的数放在数组左边
public class 荷兰国旗问题 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
//		int[] arr = {1,9,2,8,3,4,7,6,5};
//		int len =arr.length;
		int[] arr = {1,1,1,2,2,2,3,4,4,5,5,5,5,6,7,7,8};
		unique(arr);
		for(int i:arr) {
			System.out.print(i);
		}
	}
	
	
	static void unique(int[] arr) {
		int len = arr.length;
		if(len<=2) {
			return;
		}
		int i = 1;
		int u = 0;
		while(i<len) {
			if(arr[i++] != arr[u]) {
				swap(arr,i-1,++u);
			}
		}
	}
	
	static void swap(int[] arr,int a,int b) {
//		arr[a] = arr[a]^arr[b];
//		arr[b] = arr[b]^arr[a];
//		arr[a] = arr[a]^arr[b];
		int temp = 0;
		temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}
}
