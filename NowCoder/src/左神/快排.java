package 左神;

public class 快排 {
	public static void main(String[] args) {
		int[] arr = {4,3,2,10,6,7,8,2,0,9,44,54,23,-1,-100,32,-321,13,45,14,542,1,-1111,32,3,67654,22,2321,1,98};
		long begin = System.nanoTime();
		quicksort(arr, 0, arr.length-1);
		long end  =System.nanoTime();
		for(int i:arr) {
			System.out.println(i);
		}
		System.out.println("time"+(end-begin));
	}
	
	static void quicksort(int[] arr,int L,int R) {
		if(L<R) {
			int[] p = partition(arr,L,R);
			quicksort(arr, L, p[0]-1);
			quicksort(arr, p[1]+1, R);
		}
	}
	
	static int[] partition(int[] arr,int L,int R) {
		int less = L-1;
		int more = R;
		while(L<more) {
			if(arr[L]<arr[R]) {
				swap(arr,++less,L);
				L++;
			}else if(arr[L]>arr[R]) {
				swap(arr,--more,L);
			}else {
				L++;
			}
		}
		swap(arr,more,R);
		return new int[] {less+1,more};
	}
	static void swap(int[] arr,int a,int b) {
		int temp = 0;
		temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}
}
