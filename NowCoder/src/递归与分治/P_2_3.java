package 递归与分治;
/*
 * 算法分析2-3，二分法改写
 */
public class P_2_3 {
	public static void getKey_(int[] arr ,int n) {
		int get = Getkey(arr,n);
		if(get==-1) {
			//找出i,j
			int pos1 = 0,pos2 =0;
			for(int i=n-1;i>0;i--) {
				int get1 = Getkey(arr, i);
				if(get1!=-1) {
					pos1 = get1;
					break;
				}
			}
			for(int j=n+1;j<arr[arr.length-1];j++) {
				int get2 = Getkey(arr, j);
				if(get2!=-1) {
					pos2 = get2;
					break;
				}
			}
			System.out.println(pos1+"  "+pos2);
		}
	}
	
	public static int Getkey(int[] arr,int n)
	{
		int left=0;
		int right=arr.length-1;
		while(left<=right)
		{
			int mid = ( left + right ) / 2;
			if(n<arr[mid])
			{
				right=mid-1;
			}
			else if(n>arr[mid])
			{
				left=mid+1;
			}
			else if(n==arr[mid])
			{
				return mid;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int[] arr = {1,3,4,6,7,9,10};
		getKey_(arr, 8);
	}
}


