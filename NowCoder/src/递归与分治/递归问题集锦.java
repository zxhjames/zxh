package 递归与分治;

public class 递归问题集锦 {
	/**
	 * 1.阶乘
	 */
	public static long f(int n) {
		if(n==1) {
			return 1;
		}
		return n*f(n-1);
	}
	public static long f_loop(int n) {
		long result = n;
		while(n>1) {
			n--;
			result*=n;
		}
		return result;
	}
	
	/**
	 * 2.fibonacci
	 */
	public static int fibonacci(int n) {
		if(n==1 || n==2) {
			return 1;
		}
		return fibonacci(n-1)+fibonacci(n-2);
	}
	
	public static void getN(int n) {
		if(n == 1) {
			return;
		}
		System.out.println(n);
		getN(n-1);
		System.out.println(n);
	}
}
