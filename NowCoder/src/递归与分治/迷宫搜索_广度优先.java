package 递归与分治;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class 迷宫搜索_广度优先 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("输入地图的长和宽");
		int m = in.nextInt();
		int n = in.nextInt();
		int Map[][] = new int[m][n];
		System.out.println("输入地图的矩阵");
		for(int i = 0;i<m;i++)
		{
			for(int j = 0;j<n;j++)
			{
				Map[i][j]=in.nextInt();
			}
		}
		System.out.println("输入地图起点和终点坐标");
		note begin = new note(in.nextInt(),in.nextInt(),0,null);
		note end = new note(in.nextInt(),in.nextInt(),0,null);
		System.out.println(BFS(begin,end,Map));
	}
	
	public static int BFS(note BEGIN,note END,int[][] MAP) {
		note begin = BEGIN;//初始起点位
		note end = END;//终止结点位
		int[][] isVisit = new int[MAP.length][MAP[0].length];//标记数组，是否访问?1:0
		int step = 0;//最短步数
		int[][] dir = {
				{0,1},//向下走
				{-1,0},//向左走
				{0,-1},//向上走
				{1,0}//向右走
		};//方向
		Queue<note> Q = new LinkedList<note>();
		Q.add(begin);
		note fina = null;
		while(!Q.isEmpty()) {
			note head = Q.poll();
			note son = null;
			isVisit[head.x][head.y]=1;
			for(int i = 0;i<4;i++) {
				int x = head.x+dir[i][0];
				int y = head.y+dir[i][1];
				if(x<0 || x>=MAP.length || y>=MAP[0].length || y<0 || MAP[x][y]==1 || isVisit[x][y]==1) {
					continue;
				}
				if(MAP[x][y]==0 && isVisit[x][y]==0) {
					if(x==end.x && y==end.y){
						step = head.step+1;
						fina = new note(end.x,end.y,step,head);
						Stack<note> S = new Stack<note>();
						note temp = fina;
						while(temp!=null)
						{
							S.push(temp);
							temp = temp.pre;
						}
						System.out.println("输出轨迹");
						while(!S.isEmpty())
						{
							System.out.println(S.peek().x+","+S.peek().y);
							S.pop();
						}
						break;
					}
					isVisit[x][y]=1;
					son = new note(x,y,head.step+1,head);
					Q.add(son);
				}
			}
		}
		System.out.println("最短步数");
		return step;
	}
	
	
	
	//DFS
	public static int DFS(note BEGIN,note END,int[][] MAP)
	{
		note begin = BEGIN;//初始起点位
		note end = END;//终止结点位
		int[][] isVisit = new int[MAP.length][MAP[0].length];//标记数组，是否访问?1:0
		int step = 999999;//最短步数
		int[][] dir = {
				{0,1},//向下走
				{-1,0},//向左走
				{0,-1},//向上走
				{1,0}//向右走
		};//方向
		Stack<note> S = new Stack<note>();
		S.push(begin);
		while(!S.isEmpty())
		{
			note head = S.peek();
			isVisit[head.x][head.y]=1;
			for(int i=0;i<4;i++)
			{
				int x = head.x+dir[i][0];
				int y = head.y+dir[i][1];
				if(x<0 || x>=MAP.length || y>=MAP[0].length || y<0 || MAP[x][y]==1 || isVisit[x][y]==1) {
					continue;
				}
				if(MAP[x][y]==0 && isVisit[x][y]==0)
				{
					if(x==end.x && y==end.y) {
						int mini=S.size();
						if(mini<=step)
						{
							step = mini;
						}
						S.pop();
					}
					note son = new note(x,y,0,head);
					S.push(son);
					isVisit[x][y]=1;
					break;
				}
			}
		}
		return step;
	}
}

class note{
	int x;
	int y;
	int step;//结点步数
	note pre;//父结点
	note(int x,int y,int step,note p){
		this.x=x;
		this.y=y;
		this.step = step;
		this.pre = p;
	}
}
