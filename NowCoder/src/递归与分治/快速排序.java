package 递归与分治;
/*
 * 快速排序法和冒泡排序相似，都是基于交换排序的思想，快速排序对冒泡排序作出了改进
 */
public class 快速排序 {
	//基准点为数组的中间元素
	static void QuickSort(int[] arr,int left,int right) {
		int t;
		int ltemp = left;
		int rtemp = right;
		int f = arr[(left+right)/2];
		while(ltemp<rtemp) {
			while(arr[ltemp]<f) {
				++ltemp;
			}
			while(arr[rtemp]>f) {
				--rtemp;
			}
			if(ltemp<=rtemp) {
				t=arr[ltemp];
				arr[ltemp]=arr[rtemp];
				arr[rtemp]=t;
				--rtemp;
				++ltemp;
				printArr(arr);
			}
		}
		if(ltemp == rtemp) {
			ltemp++;
		}
		if(left<rtemp) {
			QuickSort(arr, left, ltemp-1);
			
		}
		if(ltemp<right) {
			QuickSort(arr, rtemp+1,right);
		}
		if(left<=right) {
			t = arr[left];
			arr[left] = arr[right];
			arr[right] = t;
			
		}
	}
	
	static void printArr(int[] arr) {
		for (int i : arr) {
			System.out.print(i+" ");
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		int[] shuzu = {4,3,2,10,6,7,8,2,0,9,44,54,23,-1,-100,32,-321,13,45,14,542,1,-1111,32,3,67654,22,2321,1,98};
		long begin = System.nanoTime();
		QuickSort(shuzu, 0, shuzu.length-1);
		long end = System.nanoTime();
		printArr(shuzu);
		System.out.println(end-begin);
	}
}
