package 递归与分治;

import java.util.Scanner;

/*
 * 最短路径
 */
public class floyed {
	public static void main(String[] args) {
		int[][] e = new int[10][10];
		int k,i,j,n,m,t1,t2,t3=0;
		int INF = 99999999;
		System.out.println("输入顶点个数，边的条数");
		Scanner in = new Scanner(System.in);
	    n = in.nextInt();
		m = in.nextInt();
		for(i=1;i<=n;i++) {
			for(j=1;j<=n;j++) {
				if(i==j) {
					e[i][j]=0;
				}else {
					e[i][j]=INF;
				}
			}
		}
		
		//读入边
		for(i=1;i<=m;i++) {
			t1 = in.nextInt();
			t2 = in.nextInt();
			t3 = in.nextInt();
			e[t1][t2] = t3;
		}
		
		//floyed-warshall
		for(k=1;k<=n;k++) {
			for(i=1;i<=n;i++) {
				for(j=1;j<=n;j++) {
					if(e[i][j]>e[i][k]+e[k][j]) {
						e[i][j] = e[i][k] + e[k][j];
					}
				}
			}
		}
		
		
		//output
		for(i=1;i<=n;i++)
		{
			for(j=1;j<=n;j++) {
				System.out.print(e[i][j]+" ");
			}
			System.out.println();
		}
	}
}
