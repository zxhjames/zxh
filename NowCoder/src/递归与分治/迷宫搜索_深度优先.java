package 递归与分治;

import java.util.Scanner;

/*
 * 迷宫由n行m列单元格组成，(n和m都小于等于50)
 * 每个单元格要么是空地，要么是障碍物，你的任务是找到一条从迷宫起点到被救援者的最短路径
 * 注意障碍物是不能走的，当然寻找者也不能走到迷宫外
 */
public class 迷宫搜索_深度优先{
		static int n=99999999;
		static int m,p,q,min=n;
	
	static int[][] a=new int[51][51];
	static boolean[][] book = new boolean[51][51]; 
	/*
	 * 深度优先策略
	 */
	static void dfs(int x,int y,int step) {
		//方向数组
		int next[][]= {
				{0,1},
				{1,0},
				{0,-1},
				{-1,0}
		};
		int tx,ty,k;
		//判断是否达到被救者位置
		if(x==p && y==q) {
			//更新最小值
			if(step<min) {
				min = step;
			}
			return;
		}
		
		//枚举四种走法
		for(k=0;k<=3;k++) {
			tx=x+next[k][0];
			ty=y+next[k][1];
			if(tx<1 ||tx>n ||ty<1 ||ty>m) {
				continue;
			}
			if(a[tx][ty]==0 && book[tx][ty]==false) {
				book[tx][ty]=true;
				dfs(tx,ty,step+1);
				book[tx][ty]=false;
			}
		}
		return;
	}
	/*
	 * 广度优先策略
	 */
	
	
	public static void main(String[] args) {
		int i,j,startx,starty;
		Scanner in = new Scanner(System.in);
		//输入迷宫的长与宽
		n = in.nextInt();
		m = in.nextInt();
		//输入迷宫的每个位置的点  1为障碍物   0为空地
		for(i=1;i<=n;i++) {
			for(j=1;j<=m;j++) {
				a[i][j] = in.nextInt();
			}
		}
		//输入起点坐标
		startx = in.nextInt();
		starty = in.nextInt();
		//输入终点坐标
		p = in.nextInt();
		q = in.nextInt();
		//起点位置已经访问
		book[startx][starty]=true;
		dfs(startx,starty,0);
		System.out.println("最小步数:"+min);
	}
}
