package 递归与分治;

public class 二分搜索 {
	public static void main(String[] args) {
		int[] arr = new int[6];
		for(int i=0;i<6;i++) {
			arr[i]=i+2;
		}
		//System.out.println(getkey(arr,0,arr.length-1,3));
		System.out.println(Getkey(arr, 6));
		
	}
	/*
	 * 递归求解
	 */
	public static int getkey(int[] arr,int left,int right,int n) {
		if(left<=right) {
			int mid = (right+left)/2;
			if(n==arr[mid])
			{
				return arr[mid];
			}
			if(n>arr[mid])
			{
				System.out.println(arr[mid]);
				return getkey(arr,mid+1,right,n);
			}
			else if(n<arr[mid]) {
				System.out.println(arr[mid]);
				return getkey(arr,left,mid-1,n);
			}
		}
		return -1;
	}
	
	/*
	 * 非递归求解
	 */
	public static int Getkey(int[] arr,int n)
	{
		int left=0;
		int right=arr.length-1;
		while(left<=right)
		{
			int mid = ( left + right ) / 2;
			if(n<arr[mid])
			{
				right=mid-1;
			}
			else if(n>arr[mid])
			{
				left=mid+1;
			}
			else if(n==arr[mid])
			{
				return mid;
			}
		}
		return -1;
	}
}
