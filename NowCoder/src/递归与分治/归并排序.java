package 递归与分治;

import java.util.Arrays;

public class 归并排序 {
	public static void main(String[] args) {
		int[] arr= {9,8,7,6,5,4,3,2,1};
		Sort(arr);
		System.out.println(Arrays.toString(arr));
	}
	
	public static void Sort(int[] arr) {
		int[] temp=new int[arr.length];
		/**
		 * 在排序前，建立一个长度等于原数组长度的临时数组，
		 * 避免递归过程中频繁开辟空间
		 */
		sort(arr,0,arr.length-1,temp);//下标从0～length-1
		
	}
	
	public static void sort(int[] arr,int left,int right,int[] temp) {
		if(left<right) {
			int mid=(left+right)/2;
			sort(arr,left,mid,temp);//左边归并排序
			sort(arr,mid+1,right,temp);//右边归并爬序
			merge(arr,left,mid,right,temp);//将两个有序子数组合排序操作
		}
	}
	public static void merge(int[] arr,int left,int mid,int right,int[] temp) {
		int i=left;
		int j=mid+1;
		int t=0;
		while(i<=mid && j<=right) {
			if(arr[i]<=arr[j]) {
				temp[t++]=arr[i++];
			}else {
				temp[t++]=arr[j++];
			}
		}
		while(i<=mid) {
			temp[t++]=arr[i++];
		}
		while(j<=right) {
			temp[t++]=arr[j++];
		}
		t=0;
		while(left<=right) {
			arr[left++]=temp[t++];
		}
	}
}
