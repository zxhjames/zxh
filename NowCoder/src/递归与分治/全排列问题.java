package 递归与分治;

public class 全排列问题{
	static int[] a = new int[10];
	static boolean[] book = new boolean[10];//判断数组是否已经访问
	static void dfs(int step,int n) {
		int i;
		if(step==n+1) {
			/*
			 * 如果超过了排列数的总数，则输出一种结果。回溯
			 */
			for(i=1;i<=n;i++) {
				System.out.print(a[i]+" ");
			}
			System.out.println();
			return;
		}
		
		for(i=1;i<=n;i++) {
			if(book[i]==false) {
				/*
				 * 如果没有访问（book[i]=false) 则加入序列，递归
				 */
				a[step]=i;
				book[i]=true;//递
				dfs(step+1,n);
				book[i]=false;//归
			}
		}
		return;
	}
	public static void main(String[] args) {
		System.out.println("输入排列数的个数");
		//Scanner in = new Scanner(System.in);
		dfs(1,6);
	}
}
