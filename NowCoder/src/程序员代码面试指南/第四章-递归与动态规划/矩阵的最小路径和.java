import java.util.Scanner;
/**
 * 第二题
 */
public class 矩阵的最小路径和{
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int m = input.nextInt();
		int n = input.nextInt();
		int[][] arr = new int[m][n];//初始化m行n列的矩阵数组
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				arr[i][j] = input.nextInt();
			}
		}
		System.out.println(getMiniPath(arr,m,n));
		System.out.println(getMiniPath_(arr,m,n));
	}


	/*
	空间复杂度为O(MN)
	 */
	static int getMiniPath(int[][] map,int m,int n){
		int[][] dp = new int[m][n];
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				if(i==0 && j==0){
					dp[i][j] = map[i][j];
				}else if(i==0 && j!=0){
					dp[i][j] = dp[i][j-1] + map[i][j];
				}else if(i!=0 && j==0){
					dp[i][j] = dp[i-1][j] + map[i][j];
				}else{
					dp[i][j] = Math.min(dp[i][j-1], dp[i-1][j]) + map[i][j];
				}
			}
		}
		return dp[m-1][n-1];
	}

	/*
	空间压缩
	 */
	static int getMiniPath_(int[][] map,int m,int n){
		int[] dp = new int[n];//长度等于列数即可
		for(int i=0;i<m;i++){
			//一共枚举m次
			for(int j=0;j<n;j++){
				if(i == 0 && j==0){
					dp[0] = map[0][0]; 
				}else if(i == 0 && j != 0){
					dp[j] = dp[j-1] + map[0][j];
				}else if(i !=0 && j == 0){
					dp[0] = dp[0] + map[i][0];
				}else{
					dp[j] = Math.min(dp[j-1], dp[j]) + map[i][j];
				}
			}
		}
		return dp[n-1];
	}

}