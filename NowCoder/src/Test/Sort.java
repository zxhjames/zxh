package Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Sort {
	public static void main(String[] args) {
		ArrayList<Integer> l = new ArrayList<Integer>();
		l.add(10);
		l.add(1);
		l.add(124);
		l.add(-1);
		l.add(23);
		l.add(12);
		for(Integer i:l) {
			System.out.println(i);
		}
		Collections.sort(l,new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return o1-o2;
			}
		});
		for(Integer i:l) {
			System.out.println(i);
		}
	}
}
