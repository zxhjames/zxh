package Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

public class maptest {
	public static void main(String[] args) {
		/*
		 * hashcode sort
		 */
		HashSet<Integer> hashset = new HashSet<Integer>();
		hashset.add(1);
		hashset.add(5);
		hashset.add(1);
		hashset.add(8);
		hashset.add(-1);
		for(Integer i:hashset) {
			System.out.println(i);
		}
		
		
		/*
		 * natrual sort
		 */
		TreeSet<Integer> treeset = new TreeSet<Integer>();
		treeset.add(1);
		treeset.add(10);
		treeset.add(-1);
		treeset.add(1);
		treeset.add(-4);
		for(Integer i: treeset) {
			System.out.println(i);
		}
		
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		map.put(1, 1);
		map.put(2, 2);
		map.put(3, 3);
		map.put(4, 4);
		for(Map.Entry<Integer, Integer> m:map.entrySet()) {
			System.out.println(m.getKey()+"  "+m.getValue());
		}
		
		Set<Integer> i = map.keySet();
		for(Integer ii:i) {
			System.out.println(ii);
		}
		
	}
}
