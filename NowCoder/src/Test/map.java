package Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class map {
	public static void main(String[] args) {
//		TreeMap<String, Integer> V  =new TreeMap<String, Integer>();
//		V.put("c", 4);
//		V.put("a", 2);
//		V.put("d",2);
//		Set<Entry<String, Integer>> m = V.entrySet();
//		for (Entry<String, Integer> entry : m) {
//			System.out.println(entry.getKey()+" "+entry.getValue());
//		}
//		
//		String a = "eafd";
//		int[] arr = new int[a.length()];
//		for(int i=0;i<a.length();i++) {
//			arr[i] = Integer.valueOf(a.charAt(i));
//		}
//		Arrays.sort(arr);
//		for(int i:arr) {
//			System.out.println(i);
//		}
//		String A = "aaa";
//		char[] aa = A.toCharArray();
//		String B= "aaa";
//		char[] bb = B.toCharArray();
//		System.out.println(Arrays.equals(aa, bb));
		System.out.println(countAndSay(3));
		
	}
	
	
	 public static String countAndSay(int n) {
	       String num = String.valueOf(1);
	        String str = "1";
	       if(n==1){
	           return num;
	       }
	       Map<String,String> m = new HashMap<String,String>();
	       m.put(num,"1");
	       for(int i=0;i<n;i++){
	            Set<Entry<String, String>> mm = m.entrySet();
	            str = "";
	            for (Entry<String,String> entry : mm) {
	                str+=(entry.getValue().concat(entry.getKey()));
	            }
	            m.clear();
	            for(int j=0;j<str.length();j++){
	                String res = str.substring(j, j+1);
	                if(!m.containsKey(res)){
	                    m.put(res,"1");
	                    continue;
	                }
	                else{
	                    int change = Integer.valueOf(m.get(res))+1;
	                    m.put(res,String.valueOf(change));
	                }
	            }
	       }
	        return str;
	    }
}
