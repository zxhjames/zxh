package Test;
//redis操作
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import redis.clients.jedis.Jedis;

public class redis {
	public static void main(String[] args) {
		Jedis js = new Jedis("127.0.0.1");
		System.out.println(js.ping());
		//存储string类型
		js.set("james", "23");
		System.out.println("存储的字符串为"+js.get("james"));
		//存储list
		js.lpush("1", "james");
		js.lpush("1", "redis");
		js.lpush("1", "mysql");
		List<String> list = js.lrange("1", 0, 2);
		for(String i:list) {
			System.out.println(i);
		}
//		//存储set
//		js = new Jedis("127.0.0.1");
//		js.sadd("2", "a");
//		js.sadd("2", "b");
//		js.sadd("2", "c");
//		Set<String> keys = js.smembers("2");
//		Iterator<String> s = keys.iterator();
//		while(s.hasNext()) {
//			String ss = s.next();
//			System.out.println(ss);
//		}
		Map<String, String> map = new HashMap<String, String>();
		map.put("1", "11");
		map.put("2", "22");
		js.hmset("1", map);
		
	}
}
