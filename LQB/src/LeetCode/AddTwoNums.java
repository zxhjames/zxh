public class AddTwoNums
{
	 public class ListNode
	  {
	     int val;
	     ListNode next;
	     ListNode(int x) { val = x; }
 	  }
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = new ListNode(0);
        ListNode ptr = result;
        int carry = 0, val = 0;
        while (l1 != null || l2 != null || carry > 0) {
            if (l1 != null) {
                val += l1.val;
                l1 = l1.next;
            } 
            if (l2 != null) {
                val += l2.val;
                l2 = l2.next;
            }
            val += carry;
            ptr.next = new ListNode(val % 10);
            carry = val / 10;
            val = 0;
            ptr = ptr.next;
        }
        return result.next;
    }


	public static void main(String[] args) {
		
		System.out.println(10<<2);
	}

}