package JAVA_B;

import java.math.BigInteger;
import java.util.Scanner;
//�������
public class Complex
{
	static BigInteger real=BigInteger.ZERO;
	static BigInteger imag=BigInteger.ZERO;
	public String show()
	{
		
		if(imag.compareTo(BigInteger.ZERO)==-1)
			{
				return real+""+imag+"j";
			}
			else
			{
				return real+"+"+imag+"j";
			}
	}
	public static void main(String[] args) {
		Complex c=new Complex();
		Scanner input=new Scanner(System.in);
		real=input.nextBigInteger();
		imag=input.nextBigInteger();
		for(int i=0;i<5;i++)
		{
			real=real.multiply(real).subtract(imag.multiply(imag));
			imag=imag.multiply(real).add(real.multiply(imag));
		}
		System.out.println(c.show());
	}

}
