//现在有一棵合法的二叉树，树的节点都是用数字表示，现在给定这棵树上所有的父子关系，求这棵树的高度
public class TreeHight 
{
	static int LEN=5;
	static int[][] arr={
		{0,1},{0,2},{1,3},{1,4}
	};

	public static void main(String[] args) {
		Tree[] tree=new Tree[5]; 
		for(int i=0;i<4;i++)
		{
			tree[i].data=i;
		}


		for(int i=0;i<LEN-1;i++)
		{
			if(tree[arr[i][0]].left!=null)
			{
				tree[arr[i][0]].left=tree[arr[i][1]];
			}
			else
			{
				tree[arr[i][0]].right=tree[arr[i][1]];
			}
		}


		tree[0].High();

	}
}


class Tree
{
	int data;
	Tree prev;
	Tree left;
	Tree right;
	Tree()
	{

	}
	Tree(int data)
	{
		this.data=data;
	}
	void High()
	{
		int i=0;
		while((this.left==null && this.right==null))
		{
			if(this.left==null && this.right!=null)
			{
				this.right=this;
				i++;
				continue;
			}
			else
			{
				this.left=this;
				i++;
				continue;
			}

		}
		System.out.println(i);
	}
}