package JAVA练习;

class Test
{
	public static void main(String[] args) {
		
	}
	public void f1()
	{
		System.out.println("f1");
	}
	
	public final void f2()
	{
		System.out.println("f2");
	}
	
	public void f3()
	{
		System.out.println("f3");
	}

	private void f4()
	{
		System.out.println("f4");
	}
}

public class Test1 extends Test
{
	public void f1()
	{
		System.out.println("父类的方法被覆盖!");
	}
	
	public static void main(String[] args) {
		Test1 t=new Test1();
		t.f1();
		t.f2();
		t.f3();
		//t.f4();
	}
}
