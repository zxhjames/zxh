public class test4
{
	public static int maxSubSum(int[] a)
	{
		int maxSum=0;
		for(int i=0;i<a.length;i++)
		{
			for(int j=i;j<a.length;j++)
			{
				int thissum=0;
				for(int k=i;k<=j;k++)
				{
					thissum+=a[k];
				}
				if(thissum>maxSum)
				{
					maxSum=thissum;
				}
			}
		}
		return maxSum;
	}
	public static void main(String[] args) {
		test4 test=new test4();
		int[] arr={2,3,4,5,6,1,10};
	//	test.maxSubSum(arr);
		System.out.println(test.maxSubSum(arr));
	}

}