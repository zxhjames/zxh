package JAVA算法练习;
//给出一个包含n个整数的数列，问整数a在数列中的第一次出现是第几个。


import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main_6
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		int COUNT=0;
		Map<Integer, Integer> arr =new HashMap<Integer, Integer>();
		int n=input.nextInt();
		for(int i=1;i<n+1;i++)
		{
			arr.put(i, input.nextInt());
		}
		int a=input.nextInt();
		for(Map.Entry<Integer, Integer> entry:arr.entrySet())
		{
			int value=entry.getValue();
			int key=entry.getKey();
			if(value==a)
			{
				System.out.println(key);
				break;
			}
			COUNT++;
		}
		if(COUNT==n)
		{
			System.out.println(-1);
		}
	}
	
	
	

}
