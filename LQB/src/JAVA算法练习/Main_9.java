package JAVA算法练习;

import java.util.ArrayList;
import java.util.Scanner;
//输出DNA序列
public class Main_9
{

	public static void main(String[] args)
	{
		ArrayList<Integer> arr=new ArrayList<>();
		int k;
		Scanner input=new Scanner(System.in);
		int N=input.nextInt();
		if(N>15 || N<0)
		{
			return;
		}
		while(N!=0)
		{
			int a=input.nextInt();
			int b=input.nextInt();
			if(a<3 || a>39 || a%2==0 || b<1 || b>20)
			{
				System.out.println("error");
				return;
			}
			arr.add(a);
			arr.add(b);
			--N;	
		}
		
		//下面开始输出
		for(int i=0;i<arr.size();i=i+2)
		{
			for(int j=0;j<arr.get(i+1);j++)
			{
				k=0;
				while(k<arr.get(i))
				{
					for(int p=0;p<arr.get(i);p++)
					{
						if(p==k || p==arr.get(i)-k-1)
						{
							System.out.print("X");
						}
						else
						{
							System.out.print(" ");
						}
					}
					k++;
					System.out.println();
				}
			}
			System.out.println();
		}
	}
}

		
		
			