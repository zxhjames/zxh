package JAVA算法练习;
//1221是一个非常特殊的数，它从左边读和从右边读是一样的，编程求所有这样的四位十进制数。
import java.util.ArrayList;
import java.util.Collections;

public class Main_2
{
	public static void main(String[] args)
	{
		ArrayList<Integer> arr=new ArrayList<>();
		int a,b,c,d=0;
		for(int i=1001;i<=9999;i++)
		{
			a=i/1000;
			b=(i-a*1000)/100;
			c=(i-a*1000-b*100)/10;
			d=(i-a*1000-b*100-c*10);
			if((a==d) && (b==c))
			{
				arr.add(i);
			}
		}
		
		Collections.sort(arr);
		for(int i=0;i<arr.size();i++)
		{
			System.out.println(arr.get(i));
		}
	}

}
