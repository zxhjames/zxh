package java集合;

import java.util.ArrayList;
import java.util.Collection;

import java.util.List;
//List的基本性质，基本操作
public class Collection_List
{
	public static void main(String[] args) {
		List<String> allList=null;
		Collection<String> allCollection=null;
		allList = new ArrayList<String>();
		allCollection = new ArrayList<String>();
		allList.add("HELLO");
		allList.add(0,"HELLO");
		System.out.println(allList);
		allCollection.add("LXH");
		allCollection.add("www");
		allList.addAll(allCollection);
		allList.addAll(0, allCollection);
		System.out.println(allList);		
		allList.remove(1);
		System.out.println(allList);
		allList.removeAll(allList);
		System.out.println(allList);
	}

}
