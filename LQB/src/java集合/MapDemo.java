package java集合;

import java.util.*;

public class MapDemo {
	public static void main(String[] args) {
		long start=System.currentTimeMillis();
		//输出所有的key和values，foreach输出
		long end=0;
		Map<String, String> map=null;
		map= new HashMap<String, String>();
		map.put("A", "aaa");
		map.put("B", "bbb");
		map.put("C", "ccc");
		map.put("D", "ddd");
		String val=map.get("D");
		System.out.println(val);
		
		//输出所有的keys
		Set<String> keys= map.keySet();
		Iterator<String> iter = keys.iterator();
		while(iter.hasNext())
		{
			String str=iter.next();
			System.out.println(str+"　");
		}
		
		//输出所有的values
		Collection<String> values = map.values();
		Iterator<String> iter1 = values.iterator();
		while(iter1.hasNext())
		{
			String val1 = iter1.next();
			System.out.println(val1+"　");
		}
		
		//输出所有的keys和values，迭代器输出
		Set<Map.Entry<String, String>> allSet=null;
		allSet=map.entrySet();
		Iterator<Map.Entry<String, String>> iter11 = null;
		iter11=allSet.iterator();
		
		while(iter11.hasNext())
		{
			Map.Entry<String, String> me = iter11.next();
			System.out.println(me.getKey()+" "+me.getValue());
		}
		
	;
		start=System.currentTimeMillis();
		for(Map.Entry<String, String> me:map.entrySet())
		{
			System.out.println(me.getKey()+" "+me.getValue());
		}
		end=System.currentTimeMillis();
		System.out.println(end-start);
	}
}
