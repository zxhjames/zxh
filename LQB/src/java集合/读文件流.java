package java集合;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
public class 读文件流 
{
	public static void main(String[] args) throws IOException
	{
		HashMap<Integer, Integer> map =new HashMap<Integer,Integer>();;
		Collection<Integer> count =null;
		ArrayList<Integer> ARR=new ArrayList<>();
		Map<Character, Integer> MAP = new TreeMap<>();
		char Aa='A';
		int n = 6;
		int m=10;
		int[][] arr = new int[m][n]; 
		File file = new File("d:\\array.txt");
		BufferedReader in = new BufferedReader(new FileReader(file)); 
		String line;
		int row=0;
		while((line = in.readLine())!= null)
		{
			String[] temp = line.split(" ");
			for(int j=0;j<temp.length;j++)
			{
				arr[row][j] = Integer.parseInt(temp[j]);
			}
			row++;
		}	
		in.close();	
		System.out.println("票数统计开始！");
		System.out.println("A "+"B "+"C "+"D "+"E "+"F");
		for(int i=0;i<m;i++)
		{
			//System.out.print("同学"+m+":");
			for(int j=0;j<n;j++)
			{
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
		
		for(int i=0;i<arr[i].length;i++)
		{
			
			
			for(int k=0;k<arr.length;k++)
			{
				//arr[k][i]=ARR.get(k);
				ARR.add(arr[k][i]);
			}
			//System.out.println(arr.length);
			//System.out.println(ARR.size());
			for(int j=0;j<ARR.size();j++)
			{
				if(map.containsKey(ARR.get(j)))
				{
					int temp=map.get(ARR.get(j));
					map.put(ARR.get(j), temp+1);
				}
				else
				{
					map.put(ARR.get(j), 1);
				}
			}
			count=map.values(); 
			int maxCount=Collections.max(count);
			int maxNum=0;
			for(Map.Entry<Integer, Integer> entry : map.entrySet())
			{
				if(maxCount==entry.getValue())
				{
					maxNum=entry.getKey();
				}
			}
		//	System.out.println();
			System.out.println(Aa+"同学最后的排名:"+maxNum+" ");
		
			MAP.put(Aa, maxNum);
			Aa=(char) (Aa+1);
			map.clear();
			ARR.clear();
		}
		
		List<Map.Entry<Character, Integer>> list = new ArrayList<Map.Entry<Character,Integer>>(MAP.entrySet());
		Collections.sort(list,new Comparator<Map.Entry<Character, Integer>>()
		{

			@Override
			public int compare(Entry<Character, Integer> o1, Entry<Character, Integer> o2) {
				return o2.getValue().compareTo(o1.getValue());
			
			}
		});
		for(Entry<Character, Integer> mapping:list)
		{
			System.out.println(mapping.getKey()+" "+mapping.getValue());
		}
		
	}
}
