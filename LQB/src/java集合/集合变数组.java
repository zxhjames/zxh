package java集合;

import java.util.ArrayList;
import java.util.List;

public class 集合变数组 {
	public static void main(String[] args) {
		List<String> allList = null;
		allList = new ArrayList<String>();
		allList.add("Hello");
		allList.add("0,World");
		allList.add("MLDN");
		allList.add("www");
		String str[] = allList.toArray(new String[] {});
		System.out.println("指定数组的类型");
		for(int i=0;i<str.length;i++)
		{
			System.out.println(str[i]+",");
		}
		System.out.println("返回的数据类型");
		Object obj[]=allList.toArray();
		for(int i=0;i<obj.length;i++)
		{
			String temp=(String) obj[i];
			System.out.println(temp+",");
		}
		
		List<String> allSub=allList.subList(0, 3);
		for(int i=0;i<allSub.size();i++)
		{
			System.out.println(allList.get(i)+" ");
		}
	}
}
