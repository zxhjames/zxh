package com.sp.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * 
 * @author james
 *@serial用于处理购物相关的业务逻辑
 */
public class MyCartBO {
	private ResultSet rs=null;
	private Connection ct=null;
	private PreparedStatement ps=null;
	HashMap<String, String> hm=new HashMap<String,String>();
	/**
	 * 返回购物车总价格
	 */
	private float allPrice=0.0f;
	public float returnAllPrice(){
		return this.allPrice;
	}
	/**
	 *根据 ID号返回货物的数量
	 * @param goodsId
	 * @param goodsNum
	 */
	public String getGoodsNumById(String goodsId){
		return (String)hm.get(goodsId);
	}
	
	
	//增加货物
	public void addGoods(String goodsId,String goodsNum)
	{
		hm.put(goodsId,goodsNum);
	}
	//减少货物
	public void delGoods(String goodsId)
	{
		hm.remove(goodsId);
	}
	//清空货物
	public void clear()
	{
		hm.clear();
	}
	//更新货物信息
	public void upGoods(String goodsId,String newNum)
	{
		hm.put(goodsId,newNum);
	}
	
	//显示购物车
	public ArrayList showMycart() throws SQLException
	{
		ArrayList<GoodsBean> al=new ArrayList<GoodsBean>();
		try {
			//组织SQl语句
			String sql="select * from goods where goodsId in";//(1,4)
			Iterator it=hm.keySet().iterator();
			String sub="(";
			while (it.hasNext())
			{
				String goodsId=(String) it.next();
				//判断goodsId是否最后一个
				if(it.hasNext())
				{
					sub+=goodsId+",";
				}else{
					sub+=goodsId+")";
				}
			}
			sql+=sub;
			ct=new ConnDB().getConn();
			ps=ct.prepareStatement(sql);
			rs=ps.executeQuery();
			this.allPrice=0.0f;
			while(rs.next())
			{
				GoodsBean gb=new GoodsBean();
				gb.setGoodsId(rs.getLong(1));
				int goodsId=rs.getInt(1);
				gb.setGoodsName(rs.getString(2));
				gb.setGoodsIntro(rs.getString(3));
				gb.setGoodsPrice(rs.getFloat(4));
				float unit=rs.getFloat(4);
				gb.setGoodsNum(rs.getInt(5));
				gb.setPublisher(rs.getString(6));
				gb.setPhoto(rs.getString(7));
				gb.setType(rs.getString(8));
				al.add(gb);
				//算总价
				this.allPrice+=unit*Integer.parseInt(this.getGoodsNumById(goodsId+""));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}finally{
			this.close();
		}
		return al;
	}
	
	public void close() throws SQLException
	{
		if(rs!=null)
		{
			rs.close();
			rs=null;
		}
		if(ps!=null)
		{
			ps.close();
			ps=null;
		}
		if(ct!=null)
		{
			ct.close();
			ct=null;
		}
	}
}
