package com.sp.model;
import java.sql.Connection;
import java.sql.DriverManager;
/*
 * 连接数据库的类
 */
public class ConnDB {
	/**
	 * 加载驱动
	 * 得到链接
	 * 获得url,user,password
	 */
	private Connection ct=null;
	public Connection getConn()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/Users?useSSL=false";
			String user="root";
			String password="";
			ct=DriverManager.getConnection(url,user,password);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return ct;
	}	
}
