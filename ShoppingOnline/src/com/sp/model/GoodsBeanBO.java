package com.sp.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * 
 * @author james
 *@serial  这是model类，处理与goods表相关的业务
 */
public class GoodsBeanBO {
	/**
	 * 根据一个货物货物id，得到货物具体信息的函数
	 */
	private ResultSet rs=null;
	private Connection ct=null;
	private PreparedStatement ps=null;
	public GoodsBean getGoodsBean(String id)
	{
		GoodsBean gb=new GoodsBean();
		try {
			ct=new ConnDB().getConn();
			ps=ct.prepareStatement("select * from goods where goodsId=?");
			ps.setString(1,id);//将第一个参数赋给id
			rs=ps.executeQuery();
			if(rs.next())
			{
				gb.setGoodsId(rs.getLong(1));
				gb.setGoodsName(rs.getString(2));
				gb.setGoodsIntro(rs.getString(3));
				gb.setGoodsPrice(rs.getFloat(4));
				gb.setGoodsNum(rs.getInt(5));
				gb.setPublisher(rs.getString(6));
				gb.setPhoto(rs.getString(7));
				gb.setType(rs.getString(8));

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}finally{
			this.close();
		}
		return gb;
	}
	
	public void close()
	{
		try {
			if(rs!=null)
			{
				rs.close();
				rs=null;
			}
			if(ps!=null)
			{
				ps.close();
				ps=null;
			}
			if(ct!=null)
			{
				ct.close();
				ct=null;
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * 分页显示用户的信息
	 *
	 */
	@SuppressWarnings("unchecked")
	public ArrayList getGoodsByPage(int pageSize,int pageNow)
	{
		ArrayList al=new ArrayList();
		try {
			ct=new ConnDB().getConn();
			/*
			 * 显示分页的记录
			 */
			ps=ct.prepareStatement("select * from goods limit "+(pageNow-1)*pageSize+","+pageSize);
			rs=ps.executeQuery();
			while(rs.next()){
				GoodsBean gb=new GoodsBean();
				gb.setGoodsId(rs.getInt(1));
				gb.setGoodsName(rs.getString(2));
				gb.setGoodsIntro(rs.getString(3));
				gb.setGoodsPrice(rs.getFloat(4));
				gb.setGoodsNum(rs.getInt(5));
				gb.setPublisher(rs.getString(6));
				gb.setPhoto(rs.getString(7));
				gb.setType(rs.getString(8));
				//加入al
				al.add(gb);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}finally{
			this.close();
		}
		return al;
	}
	
	/**
	 * 返回有多少页
	 */

	/**
	 * 返回有多少页
	 */
	public int getPageCount(int pageSize){
		int pageCount=0;
		int rowCount=0;
		try	{
			ct=new ConnDB().getConn();
			ps=ct.prepareStatement("select count(*) from goods");
			rs=ps.executeQuery();
			if(rs.next()){
				 rowCount=rs.getInt(1);
			}
			/**
			 * 分页算法
			 */
			if(rowCount%pageSize==0){
				pageCount=rowCount/pageSize;
			}else{
				pageCount=rowCount/pageSize+1;
			}
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		finally{
			this.close();
		}
		return pageCount;
	}
}
