package com.sp.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ChangePage
 */
@WebServlet("/ChangePage")
public class ChangePage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChangePage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		String pageCount_=request.getParameter("pageCount");
		String flag=request.getParameter("flag");
		/*if(pageCount_.equals("null") && flag.equals("true")) {
			request.getRequestDispatcher("index.jsp?pageCount=1").forward(request, response);
		}else if(pageCount_.equals("null") && flag.equals("false")) {
			request.getRequestDispatcher("index.jsp?pageCount=2").forward(request, response);
		}*/
		int pageCount=Integer.valueOf(request.getParameter("pageCount"));
		if(flag.equals("true"))
		{
			if(pageCount>1)
			{
				request.getRequestDispatcher("index.jsp?pageCount="+(pageCount-1)).forward(request, response);
			}else{
				request.getRequestDispatcher("index.jsp?pageCount=1").forward(request, response);
			}
		}	
		else if(flag.equals("false"))
		{
			if(pageCount<3)
			{
				request.getRequestDispatcher("index.jsp?pageCount="+(pageCount+1)).forward(request, response);
			}else{
				request.getRequestDispatcher("index.jsp?pageCount=3").forward(request, response);
			}		
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

}
