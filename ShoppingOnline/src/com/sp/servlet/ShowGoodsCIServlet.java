package com.sp.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sp.model.GoodsBean;
import com.sp.model.GoodsBeanBO;

/**
 * Servlet implementation class ShowGoodsCIServlet
 */
@WebServlet("/ShowGoodsCIServlet")
public class ShowGoodsCIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowGoodsCIServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 得到要显示的id
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		//PrintWriter out = response.getWriter();
		String goodsId=request.getParameter("id");
		
		//调用GoodsBeanBO
		GoodsBeanBO gbb=new GoodsBeanBO();
		GoodsBean gb=gbb.getGoodsBean(goodsId);
		// 把gb放入request
		request.setAttribute("goodsinfo", gb);
		request.getRequestDispatcher("showDetail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

}
