package com.sp.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sp.model.MyCartBO;

/**
 * Servlet implementation class ShoppingCIServlet
 */
@WebServlet("/ShoppingCIServlet")
public class ShoppingCIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShoppingCIServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		PrintWriter out=response.getWriter();
		/*
		 * 删除购物车,得到type值，判断用户是否删除货品
		 */
		String type=request.getParameter("type");
		//MyCartBO mbo=new MyCartBO();
		//默认为每次购买一本书，以后可以修改
		//mbo.addGoods(goodsId, "1");//这样会出现多个购物车
		//解决多个购物车的问题
		//1.先试图从session中取出一个购物车
		MyCartBO mbo=(MyCartBO)request.getSession().getAttribute("mycart");
		 if(mbo==null)
		 {
			 //说明该用户第一次购物，并且放入Session
			 //创建一个购物车，放到Session中
			 mbo=new MyCartBO();
			 request.getSession().setAttribute("mycart", mbo);
		 }
		if(type.equals("addGoods")){
		//获得要购买的货物的ID 
		String goodsId=request.getParameter("goodsId");
		
		/**
		 * 把货物取出，准备下一个页面显示
		 */
		mbo.addGoods(goodsId, "1");
	
		}else if(type.equals("delGoods")){
			String goodsId=request.getParameter("goodsId");
			mbo.delGoods(goodsId);
		}else if(type.equals("show")){
			//donothing
		}else if(type.equals("delAllgoods")){
			//删除全部书籍
			mbo.clear();
		}else if(type.equals("updateGoods")){
			//用户希望修改数量
			//怎样在servlet中获得货物的id
			//接受货物ID
			String goodsId[]=request.getParameterValues("goodsId");
			String newNums[]=request.getParameterValues("newNums");
			//测试是否得到新的修改值
			for(int i=0;i<goodsId.length;i++){
				System.out.println("id---"+goodsId[i]+"  数量---"+newNums[i]);
				mbo.upGoods(goodsId[i], newNums[i]);
			}
		}
		ArrayList al = null;
		try {
			al = mbo.showMycart();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		request.setAttribute("mycartinfo", al);
		//放入到request中即可
		request.getRequestDispatcher("showMycart.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

}
