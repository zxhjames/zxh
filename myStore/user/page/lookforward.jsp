<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title></title>
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
<link href="../css/animate.css" rel="stylesheet" />
<style>
#forward {
	width: 1000px;
	height: 500px;
	margin: 0px auto;
	padding-top: 30px;
	text-align: center;
	vertical-align: middle;
	/*background: #f00;*/
}

#forward img {
	position: relative;
	top: 193px;
}

#forward p {
	font-size: 20px;
	position: relative;
	top: 220px;
	font-weight: inherit;
	opacity: 0.3;
}
</style>
</head>
<body>
	<!-- 页面顶部-->
	<jsp:include page="head.jsp"></jsp:include>
	<!-- nav主导航-->

	<!--敬请期待页面-->
	<div id="forward">
		<img src="../img/lookforward/lookforward_img1.png" alt=""
			class="animated bounce" />
		<p class="animated slideInDown">服务暂未上线 敬请期待!!</p>
	</div>
	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
	<script src="../js/jquery-3.1.1.min.js"></script>
	<script src="../js/index.js"></script>
</body>
</html>
