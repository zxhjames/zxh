<%@page import="web.BookDao"%>
<%@page import="web.UserDao"%>
<%@page import="entity.Book"%>
<%@page import="java.util.ArrayList"%>
<%@page import="web.MyCartDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	//取出购物车中的信息
	MyCartDao mcd = (MyCartDao)request.getSession().getAttribute("mycart")==null?null:(MyCartDao)request.getSession().getAttribute("mycart");
	if(mcd==null){
		request.getRequestDispatcher("index.jsp").forward(request, response);
		return;
	}
	ArrayList<Book> book = (ArrayList<Book>)request.getSession().getAttribute("mybooks");
%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>达内商城购物车</title>
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
<link href="../css/cart.css" rel="stylesheet" />
</head>
<body>
	<!-- 页面顶部-->
	<jsp:include page="head.jsp"></jsp:include>
	<!-- nav主导航-->

	<div class="modal" style="display: none">
		<div class="modal_dialog">
			<div class="modal_header">删除提醒</div>
			<div class="modal_information">
				<img src="../img/model/model_img2.png" alt="" /> <span>确定删除您的这个宝贝吗？</span>
			</div>
			<div class="yes">
				<span>删除</span>
			</div>
			<div class="no">
				<span>不删除</span>
			</div>
		</div>
	</div>
	<div class="modalNo" style="display: none">
		<div class="modal_dialog">
			<div class="modal_header">
				删除提示 <img src="../img/model/model_img1.png" alt="" class="rt close" />
			</div>
			<div class="modal_information">
				<img src="../img/model/model_img2.png" alt="" /> <span>请选择商品</span>
			</div>
		</div>
	</div>
	<div class="big">
		<form name="" action="" method="post">
			<section id="section">
				<div id="title">
					<b>购物车</b>
					<p>
						已选<span class="total color"><%=mcd.totalnum()%></span>件商品<span
							class="interval"></span>合计(不含运费):<span
							class="totalPrices color susum"><%=mcd.totalprice() %></span><span
							class="unit color">元</span>
					</p>
				</div>
				<div id="box">
					<div id="content_box">
						<div class="imfor_top">
							<div class="check_top">
								<div class="all">
									<span class="normal"> <img
										src="../img/cart/product_normal.png" alt="" />
									</span> <input type="hidden" name="" value="">全选
								</div>
							</div>
							<div class="pudc_top">商品</div>
							<div class="pices_top">单价(元)</div>
							<div class="num_top">数量</div>
							<div class="totle_top">金额</div>
							<div class="del_top">操作</div>
						</div>


						<!-- 在这里显示购物车书籍 -->
						<%for(int i=0;i<mcd.totalnum();i++){ 
                			Book b = book.get(i);
                			int num = Integer.valueOf(mcd.getBookNumByIsbn(b.getIsbn()));
             			  %>
						<div class="imfor">
							<div class="check">
								<div class="Each">
									<span class="normal"> <img
										src="../img/cart/product_normal.png" alt="" />
									</span> <input type="hidden" name="" value="">
								</div>
							</div>
							<div class="pudc">
								<div class="pudc_information" id="<%=b.getIsbn()%>">
									<a href="../page/detail.jsp?isbn=<%=b.getIsbn()%>"><img src="../img/goods/<%=b.getIsbn()%>/detail1.jpg"
										class="lf" /> </a>
										<input type="hidden" name="" value=""> <span
										class="des lf"> <%=b.getTitle()%> <input
										type="hidden" name="" value="">
									</span>
									<p class="col lf">
										<span>作者：</span><span class="color_des"><%=b.getAuthor()%><input
											type="hidden" name="" value=""></span>
									</p>
								</div>
							</div>
							<div class="pices">
								<p class="pices_des"></p>
								<p class="pices_information">
									<b>￥</b><span><%=b.getPrice()%><input
										type="hidden" name="" value=""></span>
								</p>
							</div>
							<div class="num">
								<span class="reduc" name="red">&nbsp;-&nbsp;</span><input type="text"
									value="<%=num%>" name="buyNum"><span class="add" name="add">&nbsp;+&nbsp;</span>
							</div>
							<div class="totle">
								<span>￥</span> <span class="totle_information"><%=num*b.getPrice()%></span>
							</div>
							<div class="del">
								<a href="#" class="del_d" name="del">删除</a>
							</div>
						</div>
						<%} %>





						<div class="foot">
							<div class="foot_check">
								<div class="all">
									<span class="normal"> <img
										src="../img/cart/product_normal.png" alt="" />
									</span> <input type="hidden" name="" value="">全选
									
									
									<a href="#" class="del_all" name="del">&nbsp;&nbsp;<font color="red">删除全部</font></a>
								</div>
							</div>
							<a href="../page/order-confirm.jsp"><div class="foot_cash" id="go-buy">提交订单</div></a>
							<div class="foot_tol">
								<span>合计(不含运费):</span><span class="foot_pices susumOne"><%=mcd.totalprice() %></span><span
									class='foot_des'>元</span>
							</div>
							<div class="foot_selected">
								已选<span class="totalOne color"><%=mcd.totalnum() %></span>件商品
							</div>
						</div>
					</div>
			</section>
		</form>
		<div class="none" style="display: none">
			<p class="none_title">购物车</p>
			<div class="none_top"></div>
			<div class="none_content">
				<img src="../img/30.png" alt="" class="lf" />
				<p class="lf">您的购物车竟然还是空哒( ⊙ o ⊙ )</p>
				<span class="lf">赶快去下单吧！</span> <a href="#" class="lf">去购物>></a>
			</div>

		</div>
	</div>
	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
	<script src="../js/jquery-3.1.1.min.js"></script>
	<script src="../js/cart.js"></script>
	<script src="../js/index.js"></script>
<script>
    <!-- 显示空购物车页面-->
    $(function(){
        if(!$(".imfor")) {
            $('#section').hide();
            $('.none').show();
        }
    })
    
    
    //删除货品
    $("#del").click(function (){
       	console.log("collect");
       	$.ajax({
            type: "post",
            url: 'cancelCollect.htm',
            data: "userId=12345678901&product=9787115435101",//params,
            success: function (data) {
                if (data == 'yes') {
                	alert("操作完成！");
                	window.location.href = "detail.html?isbn=9787115435101";
                } else {
                    alert("操作失败！");
                }
            },
            error: function (data) {
                alert("系统异常！");
            }
        });
    });
    
    //增加数量

</script>
</body>
</html>
