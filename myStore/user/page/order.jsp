<%@page import="web.BookDao"%>
<%@page import="web.OrderItemDao"%>
<%@page import="entity.OrderItem"%>
<%@page import="entity.Book"%>
<%@page import="java.util.ArrayList"%>
<%@page import="web.MyCartDao"%>
<%@page import="entity.Order"%>
<%@page import="web.OrderDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
	//显示详细用户信息
	String uname = (String)request.getSession().getAttribute("uname");
	//显示商品
	ArrayList<Order> order = new OrderDao().getFinalOrder(uname);
	
%>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>我的订单 - 达内学子商城</title>
<link href="../css/my.order.css" rel="stylesheet" />
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
</head>
<body>
	<!-- 页面顶部-->
	<jsp:include page="head.jsp"></jsp:include>
	<!-- 主导航-->

	<!-- 我的订单导航栏-->
	<div id="nav_order">
		<ul>
			<li></li>
		</ul>
	</div>
	<!--我的订单内容区域 #container-->
	<div id="container" class="clearfix">
		<!-- 左边栏-->
		<div id="leftsidebar_box" class="lf">
			<div class="line"></div>
			<dl class="my_order">
				<dt onClick="changeImage()">
					我的订单 <img src="../img/myOrder/myOrder2.png">
				</dt>

			</dl>

		</div>
		<!-- 右边栏-->
		<div class="rightsidebar_box rt">
			<!-- 商品信息标题-->
			<table id="order_list_title" cellpadding="0" cellspacing="0">
				<tr>
					<th width="345">商品</th>
					<th width="82">单价（元）</th>
					<th width="50">数量</th>
					<th width="82">售后</th>
					<th width="100">实付款（元）</th>
					<th width="90">交易状态</th>
					<th width="92">操作</th>
				</tr>
			</table>
			<!-- 订单列表项 -->
			<%for(int i = 0;i<order.size();i++){
			%>
			<div id="orderItem">
				<p class="orderItem_title">
					<span id="order_id"> &nbsp;&nbsp;订单编号:<a href="#"><%=order.get(i).getOrderid() %></a>
					</span> &nbsp;&nbsp;成交时间<%=order.get(i).getHandover()%>&nbsp;&nbsp; <span> <a
						href="#" class="servie"> 联系客服<img
							src="../img/myOrder/kefuf.gif" />
					</a>
					</span>
				</p>
			</div>
			
			<% ArrayList<OrderItem> ods = new OrderItemDao().getUserOrderInfo(order.get(i).getOrderid());
			for(int j=0;j<ods.size();j++){
				Book b = new BookDao().getBookinfoByisbn(ods.get(j).getProduct());
			%>
			<div id="orderItem_detail">
				<ul>
					<li class="product"><b><a href="#"><img
								src="../img/goods/<%=ods.get(j).getProduct()%>/detail1.jpg" /></a></b> <b
						class="product_name lf"> <a href=""><%=b.getTitle() %></a> <br />
					</b> <b class="product_color "> 出版社：<%=b.getPress() %> </b></li>
					<li class="unit_price">专属价 <br /> ￥<%=ods.get(j).getItemprice()%>
					</li>
					<li class="count"><%=ods.get(j).getCount()%>件</li>
					<li class="sale_support">退款/退货 <br /> 我要维权
					</li>
					<li class=" payments_received">￥<%=ods.get(j).getCount()*ods.get(j).getItemprice() %></li>
					<li class="trading_status"><img src="../img/myOrder/car.png"
						alt="" />已发货 <br /> <a href="../page/order-info.jsp?orderid=<%=order.get(i).getOrderid()%>">订单详情</a></li>
					<li class="operate"><a href="confirmReceipt.html">确认收货</a></li>
				</ul>
			</div>
			<%}} %>			
			<!--分页器-->
			<div class="tcdPageCode"></div>
		</div>
		<!-- 右边栏(没有订单时) -->
		<!-- 右边栏(没有订单时)开始 -->
		<!--
		<div class="rightsidebar_box rt" >
		      <div class="order_empty">
		          <img src="../img/myOrder/myOrder3.png" alt=""/>
		         <p>你可能还没有订单(⊙o⊙)</p>
		         <span>赶紧去下单吧 <b>去购物</b></span>
		     </div>
	    </div>
    -->
		<!-- 右边栏(没有订单时)结束 -->
	</div>

	<!-- 品质保障，私人定制等-->
<jsp:include page="tail.jsp"></jsp:include>
</body>
<script src="../js/jquery-3.1.1.min.js"></script>
<script src="../js/index.js"></script>
<script src="../js/jquery.page.js"></script>
<script src="../js/order.js"></script>
<script type="text/javascript">
//分页部分
$(".tcdPageCode").createPage({
    pageCount:6,
    current:1,
    backFn:function(p){
    	
    }
});
/*
$(".tcdPageCode").createPage({
    pageCount: ${requestScope.pageCount },
    current: ${requestScope.current },
    backFn:function(pageIndex){
        var start = (pageIndex-1)*${requestScope.length };
        window.location.href='../servlet/show-order?pageIndex='+pageIndex+'&start='+start+'&length='+${requestScope.length };
    }
});
*/
</script>
</html>
