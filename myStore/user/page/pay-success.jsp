<%@page import="entity.Order"%>
<%@page import="web.OrderDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	Order odd = (Order)request.getSession().getAttribute("order");
	double payment = odd.getPayment();
	String orderid = odd.getOrderid();
%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>达内学子商城——支付页面</title>
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
<link href="../css/payment.css" rel="stylesheet" />
</head>
<body>
	<!-- 页面顶部-->
	<jsp:include page="head.jsp"></jsp:include>

	<div id="navlist">
		<ul>
			<li class="navlist_gray_left"></li>
			<li class="navlist_gray">确认订单信息</li>
			<li class="navlist_gray_arrow"></li>
			<li class="navlist_gray">支付订单<b></b></li>
			<li class="navlist_gray_arrow"></li>
			<li class="navlist_blue">支付完成<b></b></li>
			<li class="navlist_blue_right"></li>
		</ul>
	</div>
	<div id="container">
		<div>
			<h1>
				<i>支付结果</i><span class="rt">支付订单：<%=orderid %> &nbsp; 支付金额：<b><%=payment %></b></span>
			</h1>
		</div>
		<div class="rightsidebar_box rt">
			<div class="pay_result">
				<img src="../img/pay/pay_succ.png" alt="" />
				<p>支付成功</p>
				<span><!-- <a href="../page/order-info.jsp">查看订单状态？ </a> --><b><a href="../page/order.jsp">查看订单&gt;&gt;</a></b></span>
				<br /> 达内学子商城不会以系统异常、订单升级为由，要求你点击任何链接进行退款操作！
			</div>
		</div>
	</div>
	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
	<script src="../js/jquery-3.1.1.min.js"></script>
	<script src="../js/index.js"></script>
</body>
</html>
