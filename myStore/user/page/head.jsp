<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
	String uname =(String) request.getSession().getAttribute("uname");
	String localuser = (uname==null)?"未登录":uname;
%>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- 页面顶部-->
	<header id="top">
		<div id="logo" class="lf">
			<img class="animated jello" src="../img/header/logo.png" alt="logo" />
		</div>
		<div id="top_input" class="lf">
			<input id="input" type="text" placeholder="请输入您要搜索的内容" /> <a href=""
				class="rt"><img id="search" src="../img/header/search.png"
				alt="搜索" /></a>
				
		</div>
				<h3 align="right">当前用户:<%=localuser %></h3>
		<div class="rt">
			<ul class="lf">
				<li><a href="../page/index.jsp">首页</a><b>|</b></li>
				<li><a href="../page/collect.jsp">收藏</a><b>|</b></li>
				<li><a href="../page/order.jsp">订单</a><b>|</b></li>
				<li><a href="../page/cart.jsp">购物车</a><b>|</b></li>
				<li><a href="../page/password-change.jsp">设置</a><b>|</b></li>
				<li><a href="../controller/userLogout.do">退出</a><b>|</b></li>
				<li><a href="../page/lookforward.jsp">帮助</a></li>
			</ul>
		</div>
	</header>
</body>
</html>