<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>达内商城学子登陆页面</title>
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
<link href="../css/animate.css" rel="stylesheet" />
<link href="../css/login.css" rel="stylesheet" />
</head>
<body>
	<!-- 页面顶部-->
<header id="top">
    <div class="top">
        <img src="../img/header/logo.png" alt=""/>
        <span>欢迎登录</span>
    </div>
</header>
<div id="container">
    <div id="cover" class="rt">
        <form id="fm-login" name="form1" method="post" action="" >
            <div class="txt">
                <p>登录学子商城
                    <span>
                        <a href="../page/regist.jsp">新用户注册</a>
                    </span>
                </p>
                
                <!--  <form action="UserLoginServlet" method="post"> -->
                 <div class="text">
                    <input type="text" placeholder="请输入您的用户名" name="uname" id="uname" required>
                    <span></span>
                </div>
                <!--  </form>  -->

                <div class="text">
                    <input type="password" id="upwd" placeholder="请输入您的密码" name="upwd" required minlength="6" maxlength="15">
                    <span></span>
                </div>
                <div class="chose">

                </div>
                <input class="button_login" type="button" value="登录" id="bt-login" />
            </div>
        </form>
    </div>
</div>
<!--错误提示-->
	<div id="showResult"></div>
	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
	<script src="../js/jquery-3.1.1.min.js"></script>
	<script>
    $("#uname").blur(function(){
        var data = $("#uname").val();
        if (data == null || data == "") {
            $("#showResult").text("用户名不能为空！");
            $("#showResult").css("color","red");
            return false;
        }
        $.ajax({
            type:"POST",
            url:"../controller/checkUname.do",
            data:"uname="+data,
            beforeSend:function(XMLHttpRequest)
            {
                $("#showResult").text("正在查询");

            },
            success:function(msg)
            {
                if(msg ==="YES"){
                    $("#showResult").text("");
                }else {
                    $("#showResult").text("该用户不存在");
                    $("#showResult").css("color","red");
                    $('#uname').focus(); //自动把光标（焦点）放在用户名文本框中
                }
            },
            error:function(msg)
            {
            	console.log(msg.responseText);
                //错误处理
            	$("#showResult").text("服务器系统异常！");
                $("#showResult").css("color","red");
            }
        });
    });
    $('#bt-login').click(function(){
        //window.location.href =  "index.html";
        //读取用户的输入——表单序列化
        //将用户的输入变成能够传输给服务器的请求参数字符串
        var inputData = $('#fm-login').serialize();
        
        // alert(inputData);
        //异步提交请求，进行验证
        //async，表示同步还是异步，若值为true表示异步，为false表示同步
        //默认不指定，为true。
         $.ajax({
        	async: true,
            type: 'POST',
            url: '../controller/userLogin.do',
            data: inputData,
            success: function(result){
            	
                if(result=='YES'){  //登录成功
                    //window.location.href =  "index.jsp"; //跳转到首页
                    window.location.href="../page/index.jsp";
                }else{ //登录失败
                    $('#showResult').html('错误的用户名或密码！');
                    $('#showResult').css('color','red');
                    $('#uname').focus(); //自动把光标（焦点）放在用户名文本框中
                }
            }
        }); 
    });
    
    //实现在表单中输入用户名和密码后，按回车键，可直接登录
    //声明当在表单中按下键盘按键时，执行里面的函数
    $('#fm-login').keydown(
    		function(event){
    			if(event.keyCode==13){ //若按下回车键
    				$('#bt-login').click(); //调用执行登录按钮单击事件中的代码
    			}
    		}
    );
    
</script>
</body>
</html>
