<%@page import="web.BookDao"%>
<%@page import="entity.Book"%>
<%@page import="web.UserDao"%>
<%@page import="entity.Collect"%>
<%@page import="java.util.ArrayList"%>
<%@page import="web.CollectDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%
 	ArrayList<Collect> collect = new CollectDao().ShowMyCollect(new UserDao().getUserPhone((String)request.getSession().getAttribute("uname")));
 %>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>达内商城收藏夹</title>
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/my.collect.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
</head>
<body>
	<!-- 页面顶部-->
	<jsp:include page="head.jsp"></jsp:include>
	<!-- nav主导航-->

	<div class="modal" style="display: none">
		<div class="modal_dialog">
			<div class="modal_header">删除提醒</div>
			<div class="modal_information">
				<img src="../img/model/model_img2.png" alt="" /> <span>确定删除您的这个宝贝吗？</span>

			</div>
			<div class="yes">
				<span>删除</span>
			</div>
			<div class="no">
				<span>不删除</span>
			</div>
		</div>
	</div>
	<div class="modalNo" style="display: none">
		<div class="modal_dialog">
			<div class="modal_header">
				删除提示 <img src="../img/model/model_img1.png" alt="" class="rt close" />
			</div>
			<div class="modal_information">
				<img src="../img/model/model_img2.png" alt="" /> <span>请选择商品</span>

			</div>

		</div>
	</div>

	<div class="modalAdd" style="display: none">
		<div class="modal_dialog">
			<div class="modal_header">
				添加提示 <img src="../img/model/model_img1.png" alt="" class="rt close" />
			</div>
			<div class="modal_information">
				<img src="../img/model/model_img2.png" alt="" /> <span>请选择商品</span>

			</div>

		</div>
	</div>

	<div class="big">
		<form name="" action="" method="post">
			<section id="section">
				<div id="title">
					<b>收藏商品</b>
				</div>
				<div id="box">

					<div id="content_box">

					<!-- insert here -->
					<%for(int i=0; i<collect.size();i++)
					{  Collect c = collect.get(i);
					%>
						<div class="lf" id="d1">
							<div class="img">
								<a href="detail.jsp?isbn=<%=c.getCollectid() %>"> <img
									src="../img/goods/<%=c.getCollectid() %>/collect.jpg" alt="" />
								</a>
							</div>
							<div class="describe">
								<p><%=new BookDao().getBookinfoByisbn(c.getCollectid()).getTitle() %></p>
								<span class="price"><b>￥</b><span class="priceContent"><%=new BookDao().getBookinfoByisbn(c.getCollectid()).getPrice()%></span></span>
								<span class="addCart"><a href="detail.jsp?isbn=<%=c.getCollectid() %>">查看</a>
								 &nbsp;<a href="../controller/DelMyCollect.do?isbn=<%=c.getCollectid()%>&userid=<%=c.getUserid()%>">删除</a></span>
								
								
								<span
									class="succee" style="display: none"> <img
									src="../img/myCollect/product_true.png" alt="" /> <span>已移入购物车</span>
								</span>
							</div>
								
							<div class="mask" style="display: none">
								<div class="maskNormal">
									<img src="../img/myCollect/product_normal_big.png" alt="" />
								</div>
							</div>
							
						</div>
						
						
					<%
					}
					%>


					</div>
				</div>
			</section>

		</form>
		<div class="none" style="display: none">
			<div class="none_content">
				<img src="../img/model/model_img3.png" alt="" class="lf" />
				<p class="lf">您的收藏夹竟然还是空哒( ⊙ o ⊙ )</p>
				<span class="lf">赶快去收藏商品吧！</span> <a href="#" class="lf">去购物>></a>
			</div>

		</div>
	</div>
	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>

	<script src="../js/jquery-3.1.1.min.js"></script>
	<script src="../js/collect.js"></script>
	<script>
    $(function(){
        if(!$(".imfor")) {
            $('#section').hide();
            $('.none').show();
        }
    })
    //搜索下拉
    $('.seek').focus(function(){

        if($(this).hasClass('clickhover')){

            $(this).removeClass('clickhover');
            $(this).find('.seek_content').hide();
            $(this).find('img').attr('src','../img/header/header_normal.png');

        }else{
            $(this).addClass('clickhover');
            $(this).find('.seek_content').show();
            $(this).find('img').attr('src','../img/header/header_true.png');
        }
    })
    $('.seek_content>div').click(function(){
        $('.seek').removeClass('clickhover');
        var text=$(this).html();
        $('.seek span').html(text);
        $(this).parent().hide();
        $('.seek').find('img').attr('src','../img/header/header_normal.png');
        $('.seek').blur();

    })

    $('.seek').blur(function(){

        $('.seek').removeClass('clickhover');
        $('.seek_content').hide();

        $('.seek').find('img').attr('src','../img/header/header_normal.png');
        console.log(1);
    })
    $(".care").hover(function(){
        $(this).attr('src',"../img/header/care1.png");
    },function(){
        $(this).attr('src',"../img/header/care.png");
    });
    $(".order").hover(function(){
        $(this).attr('src',"../img/header/order1.png");
    },function(){
        $(this).attr('src',"../img/header/order.png");
    });
    $(".shopcar").hover(function(){
        $(this).attr('src',"../img/header/shop_car1.png");
    },function(){
        $(this).attr('src',"../img/header/shop_car.png");
    });
</script>
</body>
</html>
