<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>我的订单 - 达内学子商城</title>
<link href="../css/my.order.css" rel="stylesheet" />
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
<link href="../css/set.css" rel="stylesheet" />
</head>
<body>
	<div id="header">
		<!-- 头部-->
	<jsp:include page="head.jsp"></jsp:include>
		<!-- 主导航-->

		<!-- 主导航-->
		<nav id="nav1">
			<!-- 我的订单导航栏-->
			<div id="nav_order" class="lf">
				<ul>
					<li></li>
				</ul>
			</div>
		</nav>
	</div>
	</div>
	<!--我的订单内容区域 #container-->
	<div id="container" class="clearfix">
		<!-- 左边栏-->
		<div id="leftsidebar_box" class="lf">
			<div class="line"></div>
			<dl class="my_order">
				<dt onClick="changeImage()">
					帐号管理<img src="../img/myOrder/myOrder1.png">
				</dt>
				<dd class="first_dd">
					<a href="password-change.jsp">修改密码</a>
				</dd>
				<dd>
					<a href="address-add.jsp">添加地址</a>
				</dd>
			</dl>

		</div>
		<!-- 右边栏-->
		<div class="rightsidebar_box rt">
			<div class="order_empty">
				<div id="cover" class="rt">
					<form id="fm-add" method="post" name="form1">
						<div class="txt">
							<p>
								添加地址 <span> <a href="index.jsp">继续购物</a>
								</span>
							</p>
							<div class="text">
								<input type="text" placeholder="收件人" name="receiver"
									id="receiver" required> <span></span>
							</div>
							<div class="text">
								<input type="text" placeholder="地址" id="address" name="address"
									required minlength="6" maxlength="25"> <span></span>
							</div>
							<div class="text">
								<input type="text" placeholder="联系电话" id="receiverPhone"
									name="receiverPhone" required minlength="11" maxlength="11">
								<span></span>
							</div>
							<input class="button_login" type="button" value="添加" id="bt-add" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
</body>
<script src="../js/jquery-3.1.1.min.js"></script>
<script src="../js/index.js"></script>
<script src="../js/jquery.page.js"></script>
<script src="../js/order.js"></script>
<script type="text/javascript">
	$('#bt-add').click(function(){
    //读取用户的输入——表单序列化
    var inputData = $('#fm-add').serialize();
	console.log(">>>"+inputData);
    //异步提交请求
    $.ajax({
    	async: true,
        type: 'POST',
        url: '../controller/AddAddress.do',
        data: inputData,
        success: function(txt){
        	// alert("*"+txt+"*");
            if(txt=='YES'){
                alert("添加成功！");
                $('#fm-add').get(0).reset();
                window.location.href = "order-confirm.jsp";
            }else{ //修改失败
            	alert("添加失败!请确认手机号是否是用户本人");
        
            }
        },
    });
});
</script>
</html>
