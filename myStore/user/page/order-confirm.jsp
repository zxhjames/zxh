<%@page import="entity.Book"%>
<%@page import="web.MyCartDao"%>
<%@page import="entity.Address"%>
<%@page import="java.util.ArrayList"%>
<%@page import="web.AddressDao"%>
<%@page import="entity.User"%>
<%@page import="web.UserDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
	String uname = (String)request.getSession().getAttribute("uname");
	UserDao usd = new UserDao();
	User user = usd.selectUserByUname(uname);
	AddressDao ard = new AddressDao();
	ArrayList<Address> addr = ard.getUserAddress(user.getPhone());
	System.out.print(addr.size());
	MyCartDao mcd = (MyCartDao)request.getSession().getAttribute("mycart");
	ArrayList<Book> book = (ArrayList<Book>)request.getSession().getAttribute("mybooks");
%>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>确认订单</title>
<link href="../css/order.confirm.css" rel="stylesheet" />
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
</head>
<body>
	<!-- 页面顶部-->
	<jsp:include page="head.jsp"></jsp:include>
	<div id="navlist">
		<ul>
			<li class="navlist_blue_left"></li>
			<li class="navlist_blue">确认订单信息</li>
			<li class="navlist_blue_arro"><canvas id="canvas_blue"
					width="20" height="38"></canvas></li>
			<li class="navlist_gray">支付订单<b></b></li>
			<li class="navlist_gray_arro"><canvas id="canvas_gray"
					width="20" height="38"></canvas></li>
			<li class="navlist_gray">支付完成<b></b></li>
			<li class="navlist_gray_right"></li>
		</ul>
	</div>
	<!--订单确认-->
	<div id="container" class="clear">
		<!--收货地址-->
		<div class="adress_choice">
			<p>选择收货地址</p>
			<%for(int i=0;i<addr.size();i++){ %>
			<div id="addresId1">
				<i class="user_choice"> <input type="radio" name="address"
					value="<%=addr.get(i).getAddress()%>"/>
				</i> 
				
				<i class="address_name"> <%=addr.get(i).getReceiver() %> </i> <i class="user_address"> 
				    <%=addr.get(i).getAddress() %>&nbsp;<%=addr.get(i).getReceiver_phone() %> </i>
			</div>
			<%} %>
			<a href="address-add.jsp"> 添加地址 &gt;&gt; </a>
		</div>
		<!-- 商品信息-->
		<form name="" method="post" action="#">
			<div class="product_confirm">
				<p>确认商品信息</p>
				<ul class="item_title">
					<li class="p_info">商品信息</li>
					<li class="p_price">单价(元)</li>
					<li class="p_count">数量</li>
					<li class="p_tPrice">金额</li>
				</ul>
				
				<%for(int i = 0;i<mcd.totalnum();i++)
					{
						Book b = book.get(i);
	        			int num = Integer.valueOf(mcd.getBookNumByIsbn(b.getIsbn()));
					%>
				<ul class="item_detail">
					<li class="p_info"><b><img
							src="../img/goods/<%=b.getIsbn() %>/detail1.jpg" /></b> <b
						class="product_name lf"><%=b.getTitle() %></b> <br />
						<span class="product_color "><%=b.getPress() %></span></li>
					<li class="p_price"><i>达内专属价</i> <br /> <span
						class="pro_price">￥<span class="ppp_price"><%=b.getPrice() %></span></span></li>
					<li class="p_count">X<span><%=num %></span></li>
					<li class="p_tPrice">￥<span><%=num*b.getPrice() %></span></li>
				</ul>
				<%} %>
				
			</div>
		</form>
		<!-- 结算条-->
		<div id="count_bar">
			<span class="go_cart"><a href="../page/cart.jsp">&lt;&lt;返回购物车</a></span> <span
				class="count_bar_info">已选<b id="count"><%=mcd.totalnum() %></b>件商品&nbsp;&nbsp;合计(不含运费):<b
				class="zj"></b> <input type="hidden" name="Payment" value="<%=mcd.totalprice() %>" />元
			</span>
			
			<span class="go_pay" name="payit">确认并付款</span> 
		</div>
	</div>
	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
	<script src="../js/jquery-3.1.1.min.js"></script>
	<script src="../js/index.js"></script>
	<script>
    var html=0;
    var total=0;
    $(function(){
        $(".item_detail").each(function() {
            html+=1;
            var p=parseFloat($(this).children('.p_price').children('.pro_price').children('.ppp_price').html());
            console.log(p);
            var sl=parseFloat($(this).children('.p_count').children('span').html());
            var xj=parseFloat(p*sl).toFixed(2);
            console.log("xiaoji"+xj);
            $(this).children('.p_tPrice').children('span').html(xj);
            total+=xj-0;
        })
        console.log("zongji"+total);
        $("#count").html(html)-0;
        $('.zj').html(total.toFixed(2))-0;
        var input_zj=parseFloat($('.zj').html()).toFixed(2);
        $('#payment').val(input_zj);
    })
</script>
<script>
    $(".go_pay").click(function () {
    	var address = $('input:radio[name="address"]:checked').val();
        $.ajax({
        	async: true,
            type: 'POST',
            url: '../controller/OrderMaker.do',
            data: "address="+address,
            success: function(result){
            	
                if(result=='YES'){  //订单生成成功
                	alert("订单生成成功！")
                    window.location.href="../page/payment.jsp";
                }else{ //登录失败
                   alert('支付失败！');
                }
            }
        }); 
    })
</script>
	<script>
    var canvas=document.getElementById("canvas_gray");
    var cxt=canvas.getContext("2d");
    var gray = cxt.createLinearGradient (10, 0, 10, 38);
    gray.addColorStop(0, '#f5f4f5');
    gray.addColorStop(1, '#e6e6e5');
    cxt.beginPath();
    cxt.fillStyle = gray;
    cxt.moveTo(20,19);
    cxt.lineTo(0,38);
    cxt.lineTo(0,0);
    cxt.fill();
    cxt.closePath();
</script>
	<script>
    var canvas=document.getElementById("canvas_blue");
    var cxt=canvas.getContext("2d");
    var blue = cxt.createLinearGradient (10, 0, 10, 38);
    blue.addColorStop(0, '#27b0f6');
    blue.addColorStop(1, '#0aa1ed');
    cxt.beginPath();
    cxt.fillStyle = blue;
    cxt.moveTo(20,19);
    cxt.lineTo(0,38);
    cxt.lineTo(0,0);
    cxt.fill();
    cxt.closePath();
</script>

</body>
</html>
