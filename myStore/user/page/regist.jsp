<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>达内商城学子注册页面</title>
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
<link href="../css/animate.css" rel="stylesheet" />
<link href="../css/login.css" rel="stylesheet" />
</head>
<body>
	<!--模态框-->
	<div class="modal">
		<div class="modal_header">提示信息</div>
		<div class="modal_content">正在注册学子商城...</div>
	</div>
	<!-- 页面顶部-->
	<header id="top">
		<div class="top">
			<img src="../img/header/logo.png" alt="" /> <span>欢迎注册</span>
		</div>
	</header>
	<div class="parent">
		<div class="container">
			<div class="panel rt">
				<form id="fm-register" method="post"
					action="../controller/userRegist.do">
					<div class="txt">
						<p id="newuser">
							新用户注册 <span> <a href="login.jsp" id="dlogin">直接登录</a>
							</span>
						</p>
					</div>
					<div class="form-group">
						<label for="uname">用户名：</label> <input autocomplete required
							minlength="6" maxlength="9" type="text" placeholder="请输入用户名"
							autofocus name="uname" id="uname" /> <span class="msg-default">用户名长度在6到9位之间</span>
					</div>
					<div class="form-group">
						<label for="upwd">登录密码：</label> <input required type="password"
							minlength="6" maxlength="12" placeholder="请输入密码" name="upwd"
							autofocus id="upwd" /> <span class="msg-default hidden">密码长度在6到12位之间</span>
					</div>
					<div class="form-group">
						<label for="email">邮箱：</label> <input autocomplete required
							type="email" placeholder="请输入邮箱地址" name="email" id="email" /> <span
							class="msg-default hidden">请输入合法的邮箱地址</span>
					</div>
					<div class="form-group">
						<label for="phone">手机号：</label> <input id="phone" name="phone"
							placeholder="请输入您的手机号"
							pattern="(\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$"
							required type="text" /> <span class="msg-default hidden">请输入合法的手机号</span>
					</div>
					<div class="form-group">
						<label></label> <input type="button" value="提交注册信息"
							id="bt-register" />
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
	<!--弹出的小广告-->
	<script src="../js/jquery-3.1.1.min.js"></script>
	<script>
  $('#bt-register').click(function(){
    var lengths=0;
    $('.form-group').each(function(){
      if($(this).find('span').hasClass('msg-success')){
        lengths++;
      }
    });
    if(lengths==4){
    	setTimeout(function(){
    		$('#fm-register').submit();
    	},2000);
    	$('.modal').fadeIn();
    }
  });
</script>
	<script>
  /*1.对用户名进行验证*/
  $('#uname').blur(function(){
    if(this.validity.valueMissing){
      this.nextElementSibling.innerHTML = '用户名不能为空';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('用户名不能为空');
    }else if(this.validity.tooShort){
      this.nextElementSibling.innerHTML = '用户名不能少于6位';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('用户名不能少于6位');
    }else {
      this.nextElementSibling.innerHTML = '用户名格式正确';
      this.nextElementSibling.className = 'msg-success';
      this.setCustomValidity('');
  //  var data =document.getElementById("uname").value;
      var data = $("#uname").val();  //取用户名文本框中的内容
      if(!data){   //用户没有输入任何内容
        return;
      }
      /*发起异步GET请求，询问服务器用户名是否已经存在*/
      var xhr=new XMLHttpRequest();  //建立发送ajax请求的对象
      xhr.open('get','../controller/checkUname.do?uname='+data); //准备连接服务器发送请求的方式和url
      //预先告诉浏览器，当发送请求过程中，状态发生改变时，执行后面的函数
      xhr.onreadystatechange=function(){
    	  //当ajax请求状态已完成（readyState==4）并且服务器返回响应的状态为200（即OK）
    	  if(xhr.readyState==4 && xhr.status==200){
    		  if(xhr.responseText=="YES"){//假设约定，服务器响应内容为YES,表示该用户名已被占用
    			  alert('该用户名已被占用');
    		  }
    	  }
      };
      xhr.send(); //向服务器发送请求
      
      
      
     /* $.ajax({
		  type:'post',
		  data:'uname='+data,
		  url:'../controller/checkUname.do',
		  success:function(data){
	          console.log('开始处理响应数据');
	          // alert("*"+data+"*");
	          if(data=='YES'){
	            alert('该用户名已被占用');
	          }
		  }
	  });*/
    }
  });
  $('#uname').focus(function(){
    this.nextElementSibling.innerHTML = '用户名长度在6到9位之间';
    this.nextElementSibling.className = 'msg-default';
  });
  /*2.对密码进行验证*/
  $('#upwd').blur(function(){
    if(this.validity.valueMissing){
      this.nextElementSibling.innerHTML = '密码不能为空';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('密码不能为空');
    }else if(this.validity.tooShort){
      this.nextElementSibling.innerHTML = '密码长度在尽量别少于6位';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('密码长度在尽量别少于6位');
    }else {
      this.nextElementSibling.innerHTML = '密码格式正确';
      this.nextElementSibling.className = 'msg-success';
      this.setCustomValidity('');
    }
  });
  $('#upwd').focus(function(){
    this.nextElementSibling.innerHTML = '密码长度在6到12位之间';
    this.nextElementSibling.className = 'msg-default';
  });
  /*3.对邮箱地址进行验证*/
  $('#email').blur(function(){
    if(this.validity.valueMissing){
      this.nextElementSibling.innerHTML = '邮箱不能为空';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('邮箱不能为空');
    }else if(this.validity.typeMismatch){
      this.nextElementSibling.innerHTML = '邮箱格式不正确';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('邮箱格式不正确');
    }else {
      this.nextElementSibling.innerHTML = '邮箱格式正确';
      this.nextElementSibling.className = 'msg-success';
      this.setCustomValidity('');
    //  var data =document.getElementById("email").value;
      var data = $("#email").val();
      if(!data){   //用户没有输入任何内容
        return;
      }
      /*利用jquery封装好的ajax，发起异步请求，询问服务器邮箱是否已经存在
      	ajax的参数是一个js对象，对象中各参数说明如下 ：
      		type：请求的类型，get或post
      		data: 发送给服务器的请求参数
      		url：请求的服务器地址路径
      		success:服务器成功是要执行的函数，
      		该函数后面的变量参数(如下例中的result)会自动接收服务器返回的数据
      */
      
      //下面这句话，发送ajax请求，里面的js对象{...}为
      //ajax请求所需要参数
      $.ajax({
    	  type:"post",
    	  data:"email="+data,
    	  url:"../controller/checkEmail.do",
    	  success:function(result){
    		  //进行业务处理
    		  console.log(result);
    		  if(result=="YES"){
    			  alert("该邮箱已被占用，请换一个！")
    		  }
    	  }
      });
    }
  });
  $('#email').focus(function(){
    this.nextElementSibling.innerHTML = '请输入合法的邮箱地址';
    this.nextElementSibling.className = 'msg-default';
  });
  /*4.对手机号进行验证*/
  $('#phone').blur(function(){
    if(this.validity.valueMissing){
      this.nextElementSibling.innerHTML = '手机号不能为空';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('手机号不能为空');
    }else if(this.validity.patternMismatch){
      this.nextElementSibling.innerHTML = '手机号格式不正确';
      this.nextElementSibling.className = 'msg-error';
      this.setCustomValidity('手机号格式不正确');
    }else {
      this.nextElementSibling.innerHTML = '手机号格式正确';
      this.nextElementSibling.className = 'msg-success';
      this.setCustomValidity('');
    //  var data =document.getElementById("phone").value;
      var data = $("#phone").val();
      if(!data){   //用户没有输入任何内容
        return;
      }
      /*发起异步GET请求，询问服务器手机号是否已经存在*/
      $.ajax({
		  type:'post',
		  data:'phone='+data,
		  url:'../controller/checkPhone.do',
		  success:function(data){
	          console.log('开始处理响应数据');
	          // alert("*"+data+"*");
	          if(data=='YES'){
	        	  alert("手机号已经占用!");
	          }
		  }
	  });
    }
  });
  $('#phone').focus(function(){
    this.nextElementSibling.innerHTML = '请输入合法的手机号';
    this.nextElementSibling.className = 'msg-default';
  });
</script>
</body>
</html>
