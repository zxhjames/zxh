<%@page import="web.BookDao"%>
<%@page import="web.OrderItemDao"%>
<%@page import="entity.OrderItem"%>
<%@page import="web.OrderDao"%>
<%@page import="entity.Book"%>
<%@page import="java.util.ArrayList"%>
<%@page import="web.MyCartDao"%>
<%@page import="entity.Order"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String uname = (String)request.getSession().getAttribute("uname");
String address =(String) request.getSession().getAttribute("address");
String orderid = request.getParameter("orderid");
//获得订单信息
Order o = new OrderDao().get_a_Order(orderid);
//获得订单中的物品
ArrayList<OrderItem> otm = new OrderItemDao().getUserOrderInfo(orderid);
%>
<!DOCTYPE html>
<html>
<head lang="en">
<meta charset="UTF-8">
<title>达内学子商城-订单详情页</title>
<link href="../css/header.css" rel="stylesheet" />
<link href="../css/footer.css" rel="stylesheet" />
<link href="../css/order.info.css" rel="stylesheet" />
</head>
<body>
	<!-- 页面顶部-->
	<jsp:include page="head.jsp"></jsp:include>
	<!-- nav主导航-->

	<!--详细信息-->
	<div id="container">
		<!-- 导航 -->
		<div class="container_nav">
			首页&gt;订单管理&gt;订单<span><%=orderid%></span>
		</div>
		<div class="orderInfo_icon">
			<p>
				订单<span class="order-num"><%=orderid %></span>&nbsp;&nbsp;&nbsp;当前状态：<span
					class="state"><%=o.getStatue() %></span>
			</p>
		</div>
		<!-- 订单状态流程图-->
		<div id="orderStatusChart">
			<dl>
				<dt>
					<img src="../img/orderinfo/orderinfo_img1_2.png" alt="" />
				</dt>
				<dd>
					<p>提交订单</p>
					<span><%=o.getPlaced() %></span>
				</dd>
			</dl>
			<dl>
				<dt class="point">
					<img src="../img/orderinfo/orderinfo_img6_2.png" alt="" />
				</dt>
			</dl>

			<dl>
				<dt>
					<img src="../img/orderinfo/orderinfo_img3_1.png" alt="" />
				</dt>
				<dd>
					<p style="display: none">配送中</p>
					<span style="display: none">2016.01.01 13:00</span>
				</dd>
			</dl>
			<dl>
				<dt class="point">
					<img src="../img/orderinfo/orderinfo_img6_1.png" alt="" />
				</dt>
			</dl>

			<dl>
				<dt>
					<img src="../img/orderinfo/orderinfo_img4_1.png" alt="" />
				</dt>
				<dd>
					<p style="display: none">确认收货</p>
					<span style="display: none">2016.01.01 13:00</span>
				</dd>
			</dl>
			<dl>
				<dt class="point">
					<img src="../img/orderinfo/orderinfo_img6_1.png" alt="" />
				</dt>
			</dl>

			<dl>
				<dt>
					<img src="../img/orderinfo/orderinfo_img5_1.png" alt="" />
				</dt>
				<dd>
					<p style="display: none">评价</p>
					<span style="display: none">2016.01.01 13:00</span>
				</dd>
			</dl>

		</div>
		<div class="clear">

			<!-- 详细信息-->
			<h1>详细信息</h1>
			收货人：<span class="consignee"><%=uname %></span>&nbsp;&nbsp;&nbsp;&nbsp;邮编：<span
				class="postcode">333300</span>&nbsp;&nbsp;&nbsp;&nbsp;联系电话：<span
				class="tel"><%=o.getUserid() %></span> <br />
			<p>
				收货地址：<span><%= address%></span>
			</p>
		</div>

		<!-- 商品信息-->
		<div style="width: 1000px; margin: 0px auto">
			<h1 class="commodity">商品信息</h1>
			<div class="product_confirm">
				<ul class="item_title">
					<li class="p_info">商品信息</li>
					<li class="p_price">单价(元)</li>
					<li class="p_count">数量</li>
					<li class="p_tPrice">实付款</li>
				</ul>
				<div>
					订单编号：<span><%=o.getOrderid() %></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;成交时间：<%=o.getHandover() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;联系客服：<img
						src="../img/orderinfo/kefuf.gif" alt="" />
				</div>
				<%for(int i = 0;i<otm.size();i++)
				{
					Book b = new BookDao().getBookinfoByisbn(otm.get(i).getProduct());
					%>
				
				<ul class="item_detail">
					<li class="p_info"><b><img
							src="../img/goods/<%=b.getIsbn() %>/detail1.jpg" /></b>
						<p class="product_name lf"><%=b.getTitle() %></p> <br /> <span class="product_color ">
							作者：<%=b.getAuthor() %></span></li>
					<li class="p_price"><i>专属价</i> <br /> <span class="pro_price">￥<%=b.getPrice() %></span>
					</li>
					<li class="p_count"><%=otm.get(i).getCount()%>件</li>
					<li class="p_tPrice">￥<%=otm.get(i).getCount()*otm.get(i).getItemprice()%></li>
					<!--<li class="remark">备注</li>-->
					<li></li>
				</ul>
				<br>
				<%} %>
				
			</div>

		</div>

	</div>
	<!-- 品质保障，私人定制等-->
	<jsp:include page="tail.jsp"></jsp:include>
	<script src="../js/jquery-3.1.1.min.js"></script>
	<script>
    //搜索下拉
    $('.seek').focus(function(){

        if($(this).hasClass('clickhover')){

            $(this).removeClass('clickhover');
            $(this).find('.seek_content').hide();
            $(this).find('img').attr('src','../img/header/header_normal.png');

        }else{
            $(this).addClass('clickhover');
            $(this).find('.seek_content').show();
            $(this).find('img').attr('src','../img/header/header_true.png');
        }
    })
    $('.seek_content>div').click(function(){
        $('.seek').removeClass('clickhover');
        var text=$(this).html();
        $('.seek span').html(text);
        $(this).parent().hide();
        $('.seek').find('img').attr('src','../img/header/header_normal.png');
        $('.seek').blur();

    })

    $('.seek').blur(function(){

        $('.seek').removeClass('clickhover');
        $('.seek_content').hide();

        $('.seek').find('img').attr('src','../img/header/header_normal.png');
        console.log(1);
    })
    //头部hover
    $(".care").hover(function(){
        $(this).attr('src',"../img/header/care1.png");
    },function(){
        $(this).attr('src',"../img/header/care.png");
    });
    $(".order").hover(function(){
        $(this).attr('src',"../img/header/order1.png");
    },function(){
        $(this).attr('src',"../img/header/order.png");
    });
    $(".shopcar").hover(function(){
        $(this).attr('src',"../img/header/shop_car1.png");
    },function(){
        $(this).attr('src',"../img/header/shop_car.png");
    });
</script>
</html>
