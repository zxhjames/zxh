package tools;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBUtil {
	private static String driver;
	private static String url;
	private static String user;
	private static String password;
	static{

		//从加载类的地方(classes子文件夹)去加载（读）文件，而不是
		//从src子文件夹中去读，理由是本地和发布项目到tomcat服务器
		//都会有classes子文件夹，而src子文件夹只在本地有而在tomcat
		//服务器没有。
		InputStream in=DBUtil.class.getClassLoader().getResourceAsStream("db.properties");
		//System.out.println(in);
		Properties props=new Properties(); //建立一个读属性文件的对象
		try {
			props.load(in);//加载属性文件（所谓属性文件，就是一个文本）文件，每一行都是由属性名=属性值构成
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		driver=props.getProperty("driver");
		url=props.getProperty("url");
		user=props.getProperty("user");
		password=props.getProperty("password");
//		System.out.println(driver);
//		System.out.println(url);
//		System.out.println(user);
//		System.out.println(password);
	}
	
	//返回一个数据库连接
	public static Connection getConnection() throws ClassNotFoundException, SQLException{
		Class.forName(driver);
		Connection conn=DriverManager.getConnection(url, user, password);
		return conn;
	}

	//关闭一个数据库连接
	public static void close(Connection conn){
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}