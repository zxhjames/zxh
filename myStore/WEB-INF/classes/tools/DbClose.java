package tools;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/*
 * 关闭资源
 */
public class DbClose {
	public static void close(Connection conn,PreparedStatement ps,ResultSet rs) throws SQLException {
		if(rs!=null) {
			rs.close();
			rs=null;
		}else if(ps!=null) {
			ps.close();
			ps=null;
		}else if(conn!=null) {
			conn.close();
			conn = null;
		}
	}
}
