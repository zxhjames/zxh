package tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/** 
 * java连接数据库的步骤
 */
public class DBUtil_1 {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		//1.加载数据库厂商提供的驱动类
		Class.forName("com.mysql.jdbc.Driver");
		//2.建立与数据库的连接
		String url="jdbc:mysql://localhost:3306/db_ecommerce?useUnicode=true&characterEncoding=utf-8&useSSL=false";
		String user="root";
		String password="123456";
		Connection conn= DriverManager.getConnection(url, user, password);
		System.out.println(conn);
		//3.使用数据库连接进一步对数据库进行增、删、改、查等操作
		
		//4.关闭连接
		conn.close();
	}

}