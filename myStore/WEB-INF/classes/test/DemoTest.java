package test;

import org.junit.Test;

/**
 * 将指定数组中的数字，每4个一行，并用一对()括起来，形成类似如下效果：
 * (11 12 13 14)
 * (21 22 23 24)
 * (31 32 33 34)
 * (41 42 43 44)
 * (98 99)
 */
public class DemoTest {
	@Test
	public void test1(){
		int[]a={11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44,98,99,97,96};
		for(int i=0 ; i<a.length; i++){
			if(i%4==0){
				System.out.print("\n(");
			}
			System.out.print(a[i]+" ");
			if(i%4==3 || i==a.length-1){
				System.out.print(")\n");
			}
		}
		
	}
}