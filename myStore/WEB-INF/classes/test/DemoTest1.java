package test;
/**
 * 字符串相关操作
 */
import org.junit.Test;

public class DemoTest1 {
	@Test
	public void test1(){
		//1. contains 包含测试
		String s1="hello bye",s2="by",s3="hi";
		System.out.println(s1.contains(s2));//true
		System.out.println(s1.contains(s3));//false
		
		//2.拆分字符串split
		String s="one,two,three";
		String[] t=s.split(",");
		System.out.println("拆分后的字符串个数为:"+t.length);//3
		for(int i=0; i<t.length; i++){
			System.out.println(t[i]);
		}
		
		
	}
}