package test;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Test;

import tools.DBUtil;
/**
 * 
 * 测试数据库工具类DBUtil是否工作正常
 *
 */
public class DBTest {
	@Test
	public void Test1() throws ClassNotFoundException, SQLException {
		Connection conn=DBUtil.getConnection();
		System.out.println(conn);
		DBUtil.close(conn);
	}
	
}