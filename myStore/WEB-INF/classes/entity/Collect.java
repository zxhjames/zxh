package entity;
/*
 * 用于用户用来收藏自己喜欢的书籍
 */
public class Collect {
	private String userid;//用户的id----手机号
	private String collectid;
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getCollectid() {
		return collectid;
	}
	public void setCollectid(String collectid) {
		this.collectid = collectid;
	}
	
}
