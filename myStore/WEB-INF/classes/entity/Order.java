package entity;
/*
 * 订单信息
 */

import java.sql.Timestamp;

public class Order{
	private String orderid;//订单编号，手机号后三位+yyyyMMddHHmm
	private String statue;//订单状态
	private Timestamp placed;//下单时间
	private Timestamp receipt;//接单时间
	private Timestamp deliver;//发货时间
	private Timestamp handover;//交付时间
	private String userid;//用户编号
	private int addressid;//地址id
	private double payment;//支付总价格
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getStatue() {
		return statue;
	}
	public void setStatue(String statue) {
		this.statue = statue;
	}
	public Timestamp getPlaced() {
		return placed;
	}
	public void setPlaced(Timestamp placed) {
		this.placed = placed;
	}
	public Timestamp getReceipt() {
		return receipt;
	}
	public void setReceipt(Timestamp receipt) {
		this.receipt = receipt;
	}
	public Timestamp getDeliver() {
		return deliver;
	}
	public void setDeliver(Timestamp deliver) {
		this.deliver = deliver;
	}
	public Timestamp getHandover() {
		return handover;
	}
	public void setHandover(Timestamp handover) {
		this.handover = handover;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public int getAddressid() {
		return addressid;
	}
	public void setAddressid(int addressid) {
		this.addressid = addressid;
	}
	public double getPayment() {
		return payment;
	}
	public void setPayment(double payment) {
		this.payment = payment;
	}
	
	
}
